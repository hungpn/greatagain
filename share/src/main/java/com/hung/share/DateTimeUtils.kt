package com.hung.share

import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtils {

    fun convertDateToString(date: Date?, format: String): String {
        if (date == null) {
            return EMPTY
        }

        val dateFormat = SimpleDateFormat(format, Locale.ENGLISH)
        return dateFormat.format(date)
    }
}
