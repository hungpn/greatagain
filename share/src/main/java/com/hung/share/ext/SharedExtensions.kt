package com.hung.share.ext

import com.hung.share.EMPTY

fun Boolean.negative() = !this

fun String?.safe(default: String = EMPTY) = this ?: default

fun Int?.safe(default: Int = 0) = this ?: default

fun Long?.safe(default: Long = 0) = this ?: default

fun Double?.safe(default: Double = 0.0) = this ?: default

fun Float?.safe(default: Float = 0f) = this ?: default

fun Boolean?.safe(default: Boolean = false) = this ?: default

fun CharSequence?.isNotNullOrEmpty() = !isNullOrEmpty()

fun String?.isNotNullOrEmpty() = !isNullOrEmpty()