package com.hung.data.database

import android.content.Context
import android.content.SharedPreferences
import com.hung.data.di.qualifier.ApplicationContext
import com.hung.share.EMPTY
import javax.inject.Inject


class SharePreferenceManager @Inject constructor(
        @ApplicationContext app: Context
) {

    companion object {
        const val SHARED_PREF_NAME = "hihihehe"
        const val USER_TOKEN = "user_token"
        const val USER_LOGIN_ID = "user_login_id"
    }

    private val sharedPreferences by lazy(LazyThreadSafetyMode.NONE) {
        app.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
    }

    var userToken: String?
        get() = sharedPreferences.getString(USER_TOKEN, EMPTY) ?: EMPTY
        set(value) = sharedPreferences.putOrRemoveString(USER_TOKEN, value)

    var userLoginId: String?
        get() = sharedPreferences.getString(USER_LOGIN_ID, EMPTY) ?: EMPTY
        set(value) = sharedPreferences.putOrRemoveString(USER_LOGIN_ID, value)

    fun clearData() {
        sharedPreferences.edit().remove(USER_TOKEN).apply()
    }

    private inline fun SharedPreferences.put(body: SharedPreferences.Editor.() -> Unit) {
        this.edit().apply {
            body()
            apply()
        }
    }

    private inline fun SharedPreferences.putOrRemoveString(key: String, value: String?) {
        if (value == null)
            this.edit().remove(key).apply()
        else
            this.edit().putString(key, value).apply()
    }
}