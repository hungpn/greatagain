package com.hung.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.hung.data.model.user.UserEntity
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface UserDao : BaseDao<UserEntity> {

    @Query(
            """
        SELECT * FROM User
        WHERE id = :id
        """
    )
    fun getUser(id: Long): Single<UserEntity>

    @Query(
            """
        SELECT * FROM User
        WHERE login = :loginId
        """
    )
    fun getUser(loginId: String): Single<UserEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveUser(userEntity: UserEntity)

/*    @Query(
        """
        SELECT * FROM User
        WHERE isCurrentUser = "true"
        """
    )
    fun getCurrentUser(): Flowable<UserEntity>*/
}