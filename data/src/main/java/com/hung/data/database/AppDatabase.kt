package com.hung.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.hung.data.database.converter.DateConverter
import com.hung.data.database.dao.UserDao
import com.hung.data.model.user.UserEntity

@Database(
    entities = [
        UserEntity::class
    ],
    version = 1,
    exportSchema = true
)
@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        const val APP_DATABASE_NAME = "slowgit"
    }

    abstract fun createUserDao(): UserDao
}