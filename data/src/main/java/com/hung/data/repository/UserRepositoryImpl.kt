package com.hung.data.repository

import com.hung.data.database.dao.UserDao
import com.hung.data.model.user.UserApiData
import com.hung.data.model.user.UserDomainToEntityMapper
import com.hung.data.model.user.UserEntityToDomainMapper
import com.hung.data.model.user.UserResponseToDomainMapper
import com.hung.data.network.service.UserService
import com.hung.data.database.SharePreferenceManager
import com.hung.domain.model.user.User
import com.hung.domain.repository.UserRepository
import com.hung.share.EMPTY
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.ResponseBody
import org.jsoup.Jsoup
import java.io.InputStream
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
        private val mSharedPreferences: SharePreferenceManager,
        private val mUserService: UserService,
        private val mUserDao: UserDao,
        private val mUserEntityToDomainMapper: UserEntityToDomainMapper,
        private val mUserDomainToEntityMapper: UserDomainToEntityMapper,
        private val mUserResponseToDomainMapper: UserResponseToDomainMapper
) : UserRepository {

    override fun getUserByLoginId(loginId: String, getFromLocalFirst: Boolean): Single<User> {
        if (getFromLocalFirst) {
            return mUserDao.getUser(loginId)
                    .map {
                        mUserEntityToDomainMapper.map(it)
                    }
                    .onErrorResumeNext {
                        mUserService.getUserProfile(loginId)
                                .map {
                                    mUserResponseToDomainMapper.map(it)
                                }
                                .doOnSuccess {
                                    saveUser(it)
                                }
                    }
        } else {
            return mUserService.getUserProfile(loginId)
                    .map {
                        mUserResponseToDomainMapper.map(it)
                    }
                    .doOnSuccess {
                        saveUser(it)
                    }
                    .onErrorResumeNext {
                        mUserDao.getUser(loginId)
                                .map {
                                    mUserEntityToDomainMapper.map(it)
                                }
                    }
        }
    }

    override fun getUserById(id: Long): Single<User> {
        //TODO
        return mUserDao.getUser(id)
                .map {
                    mUserEntityToDomainMapper.map(it)
                }
    }

    /*override fun getCurrentUser(getFromLocalFirst: Boolean): Single<User> {
        if (getFromLocalFirst) {
            return mUserDao.getCurrentUser()
                    .map {
                        mUserEntityToDomainMapper.map(it)
                    }.firstOrError()
        } else {
            val currentLoginId = mSharedPreferences.getString(USER_LOGIN_ID, "")
            return getUserByLoginId(currentLoginId,)
        }
    }*/

    override fun saveUser(user: User): Completable {
        return Single.just(
                mUserDao.saveUser(
                        mUserDomainToEntityMapper.map(user)
                )
        ).ignoreElement()
    }

    override fun saveToken(token: String?) {
        mSharedPreferences.userToken = token
    }

    override fun getToken() = mSharedPreferences.userToken ?: EMPTY

    override fun saveUserLoginId(loginId: String?) {
        mSharedPreferences.userLoginId = loginId
    }

    override fun getUserLoginId() = mSharedPreferences.userLoginId ?: EMPTY

    override fun getFollowers(loginId: String, page: Int): Single<List<User>> {
        return mUserService.getFollowers(loginId, page)
                .mapToDomainUser()
    }

    override fun getFollowing(loginId: String, page: Int): Single<List<User>> {
        return mUserService.getFollowing(loginId, page)
                .mapToDomainUser()
    }

    override fun getFollowingStatus(loginId: String): Single<Boolean> {
        return mUserService.getFollowStatus(loginId)
                .toSingle {
                    true
                }.onErrorReturnItem(false)
    }

    override fun getContributes(loginId: String): Single<InputStream> {
        val url = "https://github.com/users/$loginId/contributions"
        return mUserService.getContributions(url)
                .map {
                    val document = Jsoup.parse(it.string())
                    val value = document.select("svg")[0]
                    val content = value.toString()
                    content.byteInputStream()
                }
    }

    override fun followUser(username: String): Single<Boolean> {
        return mUserService.unFollowUser(username)
                .map {
                    true
                }.onErrorReturnItem(false)
    }

    override fun unFollowUser(username: String): Single<Boolean> {
        return mUserService.followUser(username)
                .map {
                    true
                }.onErrorReturnItem(false)
    }

    private fun Single<List<UserApiData>>.mapToDomainUser(): Single<List<User>> {
        return this.map {
            it.map { userApiData ->
                mUserResponseToDomainMapper.map(userApiData)
            }
        }
    }
}