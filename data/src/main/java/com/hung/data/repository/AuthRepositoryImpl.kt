package com.hung.data.repository

import com.hung.data.util.GITHUB_CLIENT_ID
import com.hung.data.util.GITHUB_SECRET
import com.hung.data.model.login.AuthRequest
import com.hung.data.model.login.AuthResponse
import com.hung.data.model.login.AuthResponseToDomainMapper
import com.hung.data.network.service.AuthService
import com.hung.data.database.SharePreferenceManager
import com.hung.domain.model.login.Auth
import com.hung.domain.repository.AuthRepository
import io.reactivex.Single
import okhttp3.Credentials
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
        private val mSharePreferenceManager: SharePreferenceManager,
        private val mAuthService: AuthService,
        private val mAuthResponseToDomainMapper: AuthResponseToDomainMapper
) : AuthRepository {

    override fun doBasicLogin(userName: String, password: String): Single<Auth> {

        // this just for testing
        if (userName == "AccountForTesting" && password == "123") {
            return Single.just(AuthResponse(0, "", "", "123", ""))
                    .map {
                        mSharePreferenceManager.userToken = it.hashedToken
                        mAuthResponseToDomainMapper.map(it)
                    }
        }

        val authToken = Credentials.basic(userName, password)
        val basicAuthRequest = AuthRequest(
                clientId = GITHUB_CLIENT_ID,
                clientSecret = GITHUB_SECRET,
                scopes = listOf("user", "repo", "gist", "notifications", "read:org")
        )
        return mAuthService
                .doBasicLogin(authToken, basicAuthRequest)
                .map {
                    mSharePreferenceManager.userToken = it.hashedToken
                    mAuthResponseToDomainMapper.map(it)
                }
    }
}