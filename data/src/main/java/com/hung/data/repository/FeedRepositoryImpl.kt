package com.hung.data.repository

import com.hung.data.model.feed.FeedResponseToDomainMapper
import com.hung.data.network.service.FeedService
import com.hung.domain.model.feed.Feed
import com.hung.domain.repository.FeedRepository
import io.reactivex.Single
import javax.inject.Inject

class FeedRepositoryImpl @Inject constructor(
        private val mFeedService: FeedService,
        private val mFeedResponseToDomainMapper: FeedResponseToDomainMapper
) : FeedRepository {

    override fun getFeedsByUserName(userName: String, page: Int): Single<List<Feed>> {
        return mFeedService.getFeeds(userName, page)
                .map { feedResponses ->
                    feedResponses.map { feedResponse ->
                        mFeedResponseToDomainMapper.map(feedResponse)
                    }
                }
    }
}