package com.hung.data.repository

import com.hung.data.model.issue.IssuesResponseToDomainMapper
import com.hung.data.network.service.IssueService
import com.hung.domain.model.issue.Issues
import com.hung.domain.repository.IssueRepository
import io.reactivex.Single
import javax.inject.Inject

class IssueRepositoryImpl @Inject constructor(
        private val mIssueService: IssueService,
        private val mIssuesResponseToDomainMapper: IssuesResponseToDomainMapper
) : IssueRepository {

    override fun getCreatedIssues(q: String, page: Int): Single<Issues> {
        return mIssueService.getIssues(q, page)
                .map {
                    mIssuesResponseToDomainMapper.map(it)
                }
    }
}