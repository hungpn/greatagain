package com.hung.data.model.feed

import com.hung.data.model.Mapper
import com.hung.data.model.shared.mapper.ActorResponseToDomainMapper
import com.hung.data.model.shared.mapper.FeedTypeResponseToDomainMapper
import com.hung.data.model.shared.mapper.PayloadResponseToDomainMapper
import com.hung.data.model.shared.mapper.RepoResponseToDomainMapper
import com.hung.domain.model.feed.Feed
import com.hung.domain.model.feed.FeedType
import javax.inject.Inject

class FeedResponseToDomainMapper @Inject constructor(
        private val mFeedTypeResponseToDomainMapper: FeedTypeResponseToDomainMapper,
        private val mPayloadResponseToDomainMapper: PayloadResponseToDomainMapper,
        private val mActorResponseToDomainMapper: ActorResponseToDomainMapper,
        private val mRepoResponseToDomainMapper: RepoResponseToDomainMapper
) : Mapper<FeedResponse, Feed> {

    override fun map(from: FeedResponse) = Feed(
            id = from.id,
            type = if (from.type != null) mFeedTypeResponseToDomainMapper.map(from.type) else FeedType.NO_DEFINE,
            isPublic = from.isPublic,
            repo = if (from.repo != null) mRepoResponseToDomainMapper.map(from.repo) else null,
            actor = if (from.actor != null) mActorResponseToDomainMapper.map(from.actor) else null,
            payload = if (from.payload != null) mPayloadResponseToDomainMapper.map(from.payload) else null,
            createAt = from.createdAt
    )
}