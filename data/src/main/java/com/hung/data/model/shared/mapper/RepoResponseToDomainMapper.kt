package com.hung.data.model.shared.mapper

import com.hung.data.model.Mapper
import com.hung.data.model.shared.PayloadResponse
import com.hung.data.model.shared.RepoResponse
import com.hung.domain.model.feed.Payload
import com.hung.domain.model.feed.Repo
import javax.inject.Inject

class RepoResponseToDomainMapper @Inject constructor() : Mapper<RepoResponse, Repo> {

    override fun map(from: RepoResponse) = Repo(
            id = from.id,
            name = from.name
    )
}