package com.hung.data.model.issue

import com.google.gson.annotations.SerializedName
import com.hung.data.model.base.BaseResponse

data class IssueResponse(

        @SerializedName("title")
        val title: String? = null,

        @SerializedName("comments")
        val comments: Int = 0,

        @SerializedName("html_url")
        val htmlUrl: String? = null,

        @SerializedName("body")
        val body: String? = null,

        @SerializedName("labels")
        val labels: List<LabelResponse>? = null
) : BaseResponse()