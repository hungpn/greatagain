package com.hung.data.model.feed

enum class FeedTypeResponse constructor(var key: String) {

    PUSH_EVENT("PushEvent"),
    FORK_EVENT("ForkEvent"),
    PUBLIC_EVENT("PublicEvent"),
    CREATE_EVENT("CreateEvent"),
    WATCH_EVENT("WatchEvent"),
    NO_DEFINE("NoDefine")
}
