package com.miuty.data.data.model


import com.google.gson.annotations.SerializedName
import com.hung.data.model.shared.AuthorResponse
import com.hung.data.model.shared.TreeResponse

class CommitResponse {

    @SerializedName("sha")
    val sha: String? = null

    @SerializedName("author")
    val author: AuthorResponse? = null

    @SerializedName("committer")
    val committer: AuthorResponse? = null

    @SerializedName("update")
    val update: String? = null

    @SerializedName("distinct")
    var distinct: Boolean = false

    @SerializedName("url")
    val url: String? = null

    @SerializedName("message")
    val message: String? = null

    @SerializedName("tree")
    val tree: TreeResponse? = null

    @SerializedName("commit_count")
    val commitCount: Int = 0
}
