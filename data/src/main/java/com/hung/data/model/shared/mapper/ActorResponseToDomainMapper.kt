package com.hung.data.model.shared.mapper

import com.hung.data.model.Mapper
import com.hung.data.model.shared.ActorResponse
import com.hung.data.model.shared.PayloadResponse
import com.hung.domain.model.feed.Actor
import com.hung.domain.model.feed.Payload
import javax.inject.Inject

class ActorResponseToDomainMapper @Inject constructor(
        private val mCommitResponseToDomainMapper: CommitResponseToDomainMapper
) : Mapper<ActorResponse, Actor> {

    override fun map(from: ActorResponse) = Actor(
            id = from.id,
            avatarUrl = from.avatarUrl,
            displayLogin = from.displayLogin
    )
}