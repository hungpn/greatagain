package com.hung.data.model.login

import com.hung.data.model.Mapper
import com.hung.domain.model.login.Auth
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthResponseToDomainMapper @Inject constructor() : Mapper<AuthResponse, Auth> {

    override fun map(from: AuthResponse) = Auth(
        id = from.id,
        token = from.token,
        url = from.url
    )
}