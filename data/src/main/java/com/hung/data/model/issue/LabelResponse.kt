package com.hung.data.model.issue

import com.google.gson.annotations.SerializedName

data class LabelResponse(

        @SerializedName("id")
        var id: Int = 0,

        @SerializedName("url")
        var url: String? = null,

        @SerializedName("name")
        var name: String? = null,

        @SerializedName("color")
        var color: String? = null
)