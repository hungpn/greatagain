package com.hung.data.model.shared

import com.google.gson.annotations.SerializedName

data class OrganizationReponse(

        @SerializedName("id")
        private val id: Long = 0,

        @SerializedName("loginId")
        private val login: String? = null,

        @SerializedName("gravatar_id")
        private val gravatarId: String? = null,

        @SerializedName("url")
        private val url: String? = null,

        @SerializedName("avatar_url")
        private val avatarUrl: String? = null
)
