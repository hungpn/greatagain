package com.hung.data.model.user

import com.hung.data.model.Mapper
import com.hung.domain.model.user.User
import com.hung.share.ext.safe
import javax.inject.Inject

class UserResponseToDomainMapper @Inject constructor() : Mapper<UserApiData, User> {

    override fun map(from: UserApiData) = User(
            id = from.id.safe(),
            name = from.name.safe(),
            loginId = from.login.safe(),
            avatarUrl = from.avatarUrl.safe(),
            htmlUrl = from.htmlUrl.safe(),
            followers = from.followers.safe(),
            following = from.following.safe(),
            company = from.company.safe(),
            location = from.location.safe(),
            email = from.email.safe(),
            blog = from.blog.safe(),
            bio = from.bio.safe(),
            joinedTime = from.createdAt
    )
}