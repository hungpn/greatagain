package com.hung.data.model.user

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.*

@Entity(
        tableName = "User",
        indices = [Index(
                value = ["login"],
                unique = true)
        ]
)
data class UserEntity(

        @PrimaryKey
        var id: Long = 0,

        var name: String = "",

        var login: String = "",

        var avatarUrl: String = "",

        var htmlUrl: String = "",

        val followers: Long = 0,

        val following: Long = 0,

        val company: String = "",

        val location: String = "",

        val email: String = "",

        val blog: String = "",

        val bio: String = "",

        val joinedDate : Date? = null
)