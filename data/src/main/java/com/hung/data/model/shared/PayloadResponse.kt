package com.hung.data.model.shared

import com.google.gson.annotations.SerializedName
import com.miuty.data.data.model.CommitResponse
import com.miuty.data.data.model.ForkeeResponse

class PayloadResponse(

        @SerializedName("push_id")
        val pushId: Long = 0,

        @SerializedName("size")
        val size: Int = 0,

        @SerializedName("distinct_size")
        val distinctSize: Int = 0,

        @SerializedName("ref")
        val ref: String? = null,

        @SerializedName("head")
        val head: String? = null,

        @SerializedName("before")
        val before: String? = null,

        @SerializedName("commits")
        val commits: List<CommitResponse>? = null,

        // forkee
        @SerializedName("forkee")
        val forkee: ForkeeResponse? = null,

        // action starred
        @SerializedName("action")
        val action: String? = null,

        // created event
        //    @SerializedName("ref")
        //     String ref;

        @SerializedName("ref_type")
        val refType: String? = null,

        @SerializedName("master_branch")
        val masterBranch: String? = null,

        @SerializedName("description")
        val description: String? = null,

        @SerializedName("pusher_type")
        val pusherType: String? = null
)
