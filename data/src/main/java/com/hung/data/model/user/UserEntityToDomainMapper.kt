package com.hung.data.model.user

import com.hung.data.model.Mapper
import com.hung.domain.model.user.User
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserEntityToDomainMapper @Inject constructor() : Mapper<UserEntity, User> {

    override fun map(from: UserEntity) = User(
            id = from.id,
            name = from.name,
            loginId = from.login,
            htmlUrl = from.htmlUrl,
            avatarUrl = from.avatarUrl,
            following = from.following,
            followers = from.followers,
            company = from.company,
            location = from.location,
            email = from.email,
            blog = from.blog,
            bio = from.bio,
            joinedTime = from.joinedDate
    )
}