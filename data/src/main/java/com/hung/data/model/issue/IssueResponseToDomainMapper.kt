package com.hung.data.model.issue

import com.hung.data.model.Mapper
import com.hung.domain.model.issue.Issue
import javax.inject.Inject

class IssueResponseToDomainMapper @Inject constructor(
        private val mLabelResponseToDomainMapper: LabelResponseToDomainMapper
) : Mapper<IssueResponse, Issue> {

    override fun map(from: IssueResponse) = Issue(
            title = from.title,
            comments = from.comments,
            htmlUrl = from.htmlUrl,
            body = from.body,
            labels = from.labels?.map {
                mLabelResponseToDomainMapper.map(it)
            }
    )
}