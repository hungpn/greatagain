package com.hung.data.model.shared

import com.google.gson.annotations.SerializedName

data class TreeResponse(

        @SerializedName("sha")
        var sha: String? = null,

        @SerializedName("url")
        var url: String? = null
)
