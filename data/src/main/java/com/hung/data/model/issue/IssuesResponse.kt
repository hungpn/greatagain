package com.hung.data.model.issue

import com.google.gson.annotations.SerializedName

data class IssuesResponse(

        @SerializedName("total_count")
        val totalCount: Int = 0,

        @SerializedName("incomplete_results")
        val incompleteResults: Boolean = false,

        @SerializedName("items")
        val items: List<IssueResponse> = listOf()
)