package com.hung.data.model.issue

import com.hung.data.model.Mapper
import com.hung.domain.model.issue.Label
import javax.inject.Inject

class LabelResponseToDomainMapper @Inject constructor() : Mapper<LabelResponse, Label> {

    override fun map(from: LabelResponse) = Label(
            id = from.id,
            url = from.url,
            name = from.name,
            color = from.color
    )
}