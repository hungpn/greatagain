package com.hung.data.model.shared.mapper

import com.hung.data.model.Mapper
import com.hung.data.model.shared.PayloadResponse
import com.hung.domain.model.feed.Payload
import javax.inject.Inject

class PayloadResponseToDomainMapper @Inject constructor(
        private val mCommitResponseToDomainMapper: CommitResponseToDomainMapper
) : Mapper<PayloadResponse, Payload> {

    override fun map(from: PayloadResponse) = Payload(
            pushId = from.pushId,
            ref = from.ref,
            commits = from.commits?.map {
                mCommitResponseToDomainMapper.map(it)
            }
    )
}