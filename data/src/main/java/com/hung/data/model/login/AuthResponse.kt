package com.hung.data.model.login

import com.google.gson.annotations.SerializedName
import com.hung.data.model.base.BaseResponse

data class AuthResponse(
    @SerializedName("id") val id: Long,
    @SerializedName("url") val url: String,
    @SerializedName("token") val token: String,
    @SerializedName("hashed_token") val hashedToken: String,
    @SerializedName("token_last_eight") val tokenLastEight: String
) : BaseResponse()