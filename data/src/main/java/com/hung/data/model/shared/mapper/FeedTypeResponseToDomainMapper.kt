package com.hung.data.model.shared.mapper

import com.hung.data.model.Mapper
import com.hung.data.model.feed.FeedTypeResponse
import com.hung.domain.model.feed.FeedType
import javax.inject.Inject

class FeedTypeResponseToDomainMapper @Inject constructor() : Mapper<FeedTypeResponse, FeedType> {

    override fun map(from: FeedTypeResponse): FeedType {
        return when (from) {
            FeedTypeResponse.PUSH_EVENT -> FeedType.PUSH_EVENT
            FeedTypeResponse.FORK_EVENT -> FeedType.FORK_EVENT
            FeedTypeResponse.PUBLIC_EVENT -> FeedType.PUBLIC_EVENT
            FeedTypeResponse.CREATE_EVENT -> FeedType.CREATE_EVENT
            FeedTypeResponse.WATCH_EVENT -> FeedType.WATCH_EVENT
            FeedTypeResponse.NO_DEFINE -> FeedType.NO_DEFINE
        }
    }
}