package com.hung.data.model.base

import com.google.gson.annotations.SerializedName
import java.util.*

open class BaseResponse {

    @SerializedName("created_at")
    val createdAt: Date? = null

    @SerializedName("updated_at")
    val updatedAt: Date? = null
}