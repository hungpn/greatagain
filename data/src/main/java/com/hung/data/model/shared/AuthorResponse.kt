package com.hung.data.model.shared

import com.google.gson.annotations.SerializedName

data class AuthorResponse(

        @SerializedName("email")
        val email: String? = null,

        @SerializedName("name")
        val name: String? = null
)
