package com.hung.data.model.shared.mapper

import com.hung.data.model.Mapper
import com.hung.domain.model.feed.Commit
import com.miuty.data.data.model.CommitResponse
import javax.inject.Inject

class CommitResponseToDomainMapper @Inject constructor() : Mapper<CommitResponse, Commit> {

    override fun map(from: CommitResponse) = Commit(
            sha = from.sha,
            message = from.message
    )
}