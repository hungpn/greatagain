package com.hung.data.model

interface Mapper<From, To> {

    fun map(from: From): To
}