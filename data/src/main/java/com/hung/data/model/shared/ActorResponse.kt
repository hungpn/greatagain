package com.hung.data.model.shared

import com.google.gson.annotations.SerializedName

data class ActorResponse(

        @SerializedName("id")
        val id: Long = 0,

        @SerializedName("loginId")
        val login: String? = null,

        @SerializedName("display_login")
        val displayLogin: String? = null,

        @SerializedName("gravatar_id")
        val gravatarId: String? = null,

        @SerializedName("url")
        val url: String? = null,

        @SerializedName("avatar_url")
        val avatarUrl: String? = null
)
