package com.hung.data.model.feed


import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import java.lang.reflect.Type

class FeedTypeDeserializer : JsonDeserializer<FeedTypeResponse> {

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): FeedTypeResponse {
        val feedsTypes = FeedTypeResponse.values()
        for (feedsType in feedsTypes) {
            if (feedsType.key == json.asString) {
                return feedsType
            }
        }
        return FeedTypeResponse.NO_DEFINE
    }
}
