package com.hung.data.model.login

import com.google.gson.annotations.SerializedName

data class AuthRequest(

    @SerializedName("client_id")
    var clientId: String? = null,

    @SerializedName("client_secret")
    var clientSecret: String? = null,

    var scopes: List<String>? = null
)