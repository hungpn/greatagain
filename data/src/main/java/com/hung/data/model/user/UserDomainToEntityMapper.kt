package com.hung.data.model.user

import com.hung.data.model.Mapper
import com.hung.domain.model.user.User
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserDomainToEntityMapper @Inject constructor() : Mapper<User, UserEntity> {

    override fun map(from: User): UserEntity {
        return UserEntity(
                id = from.id,
                avatarUrl = from.avatarUrl,
                htmlUrl = from.htmlUrl,
                login = from.loginId,
                name = from.name,
                followers = from.followers,
                following = from.following
        )
    }
}