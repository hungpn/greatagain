package com.hung.data.model.feed

import com.google.gson.annotations.SerializedName
import com.hung.data.model.shared.ActorResponse
import com.hung.data.model.shared.OrganizationReponse
import com.hung.data.model.shared.PayloadResponse
import com.hung.data.model.shared.RepoResponse
import com.hung.data.model.base.BaseResponse

data class FeedResponse(

        @SerializedName("id")
        val id: String? = null,

        @SerializedName("type")
        val type: FeedTypeResponse? = FeedTypeResponse.NO_DEFINE,

        @SerializedName("actor")
        val actor: ActorResponse? = null,

        @SerializedName("repo")
        val repo: RepoResponse? = null,

        @SerializedName("payload")
        val payload: PayloadResponse? = null,

        @SerializedName("public")
        val isPublic: Boolean = false,

        @SerializedName("org")
        val organization: OrganizationReponse? = null
) : BaseResponse()