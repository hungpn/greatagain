package com.hung.data.model.issue

import com.hung.data.model.Mapper
import com.hung.domain.model.issue.Issues
import javax.inject.Inject

class IssuesResponseToDomainMapper @Inject constructor(
        private val mIssueResponseToDomainMapper: IssueResponseToDomainMapper
) : Mapper<IssuesResponse, Issues> {

    override fun map(from: IssuesResponse) = Issues(
            totalCount = from.totalCount,
            incompleteResults = from.incompleteResults,
            items = from.items.map {
                mIssueResponseToDomainMapper.map(it)
            }
    )
}