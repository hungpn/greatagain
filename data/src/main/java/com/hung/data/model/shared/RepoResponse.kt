package com.hung.data.model.shared

import com.google.gson.annotations.SerializedName
import java.util.*

data class RepoResponse(

        @SerializedName("id")
        val id: Long = 0,

        @SerializedName("name")
        val name: String? = null,

        @SerializedName("url")
        val url: String? = null,

        @SerializedName("fork")
        val isFork: Boolean = false,

        @SerializedName("size")
        val size: Long = 0,

        @SerializedName("stargazers_count")
        val stargazersCount: Int = 0,

        @SerializedName("watchers_count")
        val watchersCount: Int = 0,

        @SerializedName("language")
        val language: String? = null,

        @SerializedName("forks_count")
        val forksCount: Int = 0,

        @SerializedName("forks")
        val forks: Int = 0,

        @SerializedName("created_at")
        val createdAt: Date? = null,

        @SerializedName("updated_at")
        val updatedAt: Date? = null,

        @SerializedName("pushed_at")
        val pushedAt: Date? = null
)
