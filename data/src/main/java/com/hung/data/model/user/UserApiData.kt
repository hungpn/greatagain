package com.hung.data.model.user

import com.google.gson.annotations.SerializedName
import com.hung.data.model.base.BaseResponse
import com.hung.share.EMPTY


//https://proandroiddev.com/safe-parsing-kotlin-data-classes-with-gson-4d560fe3cdd2
data class UserApiData(

        @SerializedName("id")
        val id: Long? = 0,

        @SerializedName("name")
        val name: String? = EMPTY,

        @SerializedName("login")
        val login: String? = EMPTY,

        @SerializedName("avatar_url")
        val avatarUrl: String? = EMPTY,

        @SerializedName("html_url")
        val htmlUrl: String? = EMPTY,

        @SerializedName("followers_url")
        val followersUrl: String? = EMPTY,

        @SerializedName("following_url")
        val followingUrl: String? = EMPTY,

        @SerializedName("gists_url")
        val gistsUrl: String? = EMPTY,

        @SerializedName("starred_url")
        val starredUrl: String? = EMPTY,

        @SerializedName("subscriptions_url")
        val subscriptionsUrl: String? = EMPTY,

        @SerializedName("organizations_url")
        val organizationsUrl: String? = EMPTY,

        @SerializedName("repos_url")
        val reposUrl: String? = EMPTY,

        @SerializedName("events_url")
        val eventsUrl: String? = EMPTY,

        @SerializedName("received_events_url")
        val receivedEventsUrl: String? = EMPTY,

        @SerializedName("location")
        val location: String? = EMPTY,

        @SerializedName("company")
        val company: String? = EMPTY,

        @SerializedName("email")
        val email: String? = EMPTY,

        @SerializedName("public_repos")
        val publicRepos: Long = 0,

        @SerializedName("public_gists")
        val publicGists: Long = 0,

        @SerializedName("followers")
        val followers: Long = 0,

        @SerializedName("following")
        val following: Long = 0,

        @SerializedName("blog")
        val blog: String? = EMPTY,

        @SerializedName("bio")
        val bio: String? = EMPTY

) : BaseResponse()