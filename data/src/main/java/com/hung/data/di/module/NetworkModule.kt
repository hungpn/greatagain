package com.hung.data.di.module

import android.content.Context
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.hung.data.util.BASE_URL
import com.hung.data.util.DEFAULT_TIME_OUT_IN_SECONDS
import com.hung.data.di.qualifier.*
import com.hung.data.network.service.AuthService
import com.hung.data.network.service.FeedService
import com.hung.data.network.service.IssueService
import com.hung.data.network.service.UserService
import com.hung.data.network.interceptor.ConnectivityInterceptor
import com.hung.data.database.SharePreferenceManager
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    companion object {
        private const val KEY_CONTENT_TYPE = "Content-Type"
        const val KEY_AUTHORIZATION = "Authorization"
        const val VALUE_CONTENT_TYPE = "application/json"
    }

    @DefaultOkHttpClient
    @Provides
    fun provideDefaultOkHttpClient(
            @ApplicationContext context: Context,
            sharePreferenceManager: SharePreferenceManager
    ) = getDefaultOkHttpClient(context, sharePreferenceManager, true)

    @Provides
    @DefaultOkHttpClientWithoutToken
    fun provideDefaultOkHttpClientWithoutToken(
            @ApplicationContext context: Context,
            sharePreferenceManager: SharePreferenceManager
    ) = getDefaultOkHttpClient(context, sharePreferenceManager, false)

    private fun getDefaultOkHttpClient(
            context: Context,
            sharePreferenceManager: SharePreferenceManager,
            hasAddTokenToHeader: Boolean = true
    ): OkHttpClient {
        val okBuilder = OkHttpClient.Builder()

        //Enable log
        val logInterceptor = getHttpLoggingInterceptor()
        okBuilder
                .addInterceptor(logInterceptor) // if addInterceptor(logInterceptor) need to put to the end of chain
                .addNetworkInterceptor(ConnectivityInterceptor(context))
                .addNetworkInterceptor(StethoInterceptor())
                .addInterceptor { chain ->
                    if (hasAddTokenToHeader) {
                        val request = chain.request()
                        val builder = request.newBuilder()

                        val headers = HashMap<String, String>()
                        headers[KEY_CONTENT_TYPE] =
                                VALUE_CONTENT_TYPE
                        if (sharePreferenceManager.userToken?.isNotEmpty() == true) {
                            headers[KEY_AUTHORIZATION] = "token ${sharePreferenceManager.userToken}"
                        }
                        for ((key, value) in headers) {
                            builder.addHeader(key, value)
                        }
                        chain.proceed(builder.build())
                    } else {
                        chain.proceed(chain.request())
                    }

                }
                .connectTimeout(DEFAULT_TIME_OUT_IN_SECONDS.toLong(), TimeUnit.SECONDS)
                .readTimeout(DEFAULT_TIME_OUT_IN_SECONDS.toLong(), TimeUnit.SECONDS)
                .writeTimeout(DEFAULT_TIME_OUT_IN_SECONDS.toLong(), TimeUnit.SECONDS)
        return okBuilder.build()
    }

    private fun getHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    @RetrofitQualifier
    @Provides
    internal fun provideRetrofit(
            @DefaultOkHttpClient okHttpClient: OkHttpClient,
            gson: Gson
    ): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }


    @RetrofitWithoutTokenQualifier
    @Provides
    internal fun provideRetrofitWithoutToken(
            @DefaultOkHttpClientWithoutToken okHttpClient: OkHttpClient,
            gson: Gson
    ): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Singleton
    @Provides
    internal fun provideUserService(@RetrofitQualifier retrofit: Retrofit) =
            retrofit.create(UserService::class.java)!!

    @Singleton
    @Provides
    internal fun provideAuthService(@RetrofitWithoutTokenQualifier retrofit: Retrofit) =
            retrofit.create(AuthService::class.java)!!

    @Singleton
    @Provides
    internal fun provideFeedService(@RetrofitWithoutTokenQualifier retrofit: Retrofit) =
            retrofit.create(FeedService::class.java)!!

    @Singleton
    @Provides
    internal fun provideIssueService(@RetrofitWithoutTokenQualifier retrofit: Retrofit) =
            retrofit.create(IssueService::class.java)!!
}