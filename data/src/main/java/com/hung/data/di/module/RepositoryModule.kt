package com.hung.data.di.module

import com.hung.data.repository.AuthRepositoryImpl
import com.hung.data.repository.FeedRepositoryImpl
import com.hung.data.repository.IssueRepositoryImpl
import com.hung.data.repository.UserRepositoryImpl
import com.hung.domain.repository.AuthRepository
import com.hung.domain.repository.FeedRepository
import com.hung.domain.repository.IssueRepository
import com.hung.domain.repository.UserRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class RepositoryModule {

    @Singleton
    @Binds
    abstract fun provideUserRepository(userRepositoryImpl: UserRepositoryImpl): UserRepository

    @Singleton
    @Binds
    abstract fun provideAuthRepository(authRepositoryImpl: AuthRepositoryImpl): AuthRepository

    @Singleton
    @Binds
    abstract fun provideFeedRepository(feedRepositoryImpl: FeedRepositoryImpl): FeedRepository

    @Singleton
    @Binds
    abstract fun provideIssueRepository(issueRepository: IssueRepositoryImpl): IssueRepository
}