package com.hung.data.di.qualifier

import javax.inject.Qualifier
import kotlin.annotation.Retention

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class RetrofitWithoutTokenQualifier