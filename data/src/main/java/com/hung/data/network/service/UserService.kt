package com.hung.data.network.service

import com.hung.data.model.user.UserApiData
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface UserService {

    @GET("users/{loginId}")
    fun getUserProfile(@Path("loginId") loginId: String): Single<UserApiData>

    @GET("users/{loginId}/followers")
    fun getFollowers(@Path("loginId") loginId: String, @Query("page") page: Int): Single<List<UserApiData>>

    @GET("users/{loginId}/following")
    fun getFollowing(@Path("loginId") loginId: String, @Query("page") page: Int): Single<List<UserApiData>>

    @PUT("user/following/{username}")
    fun followUser(@Path("username") username: String): Single<Response<Boolean>>

    @DELETE("user/following/{username}")
    fun unFollowUser(@Path("username") username: String): Single<Response<Boolean>>

    @GET("user/following/{username}")
    fun getFollowStatus(@Path("username") username: String): Completable

    @GET
    fun getContributions(@Url url: String): Single<ResponseBody>
}