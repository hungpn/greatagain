package com.hung.data.network.service


import com.hung.data.model.feed.FeedResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface FeedService {

    @GET("users/{username}/received_events?client_id=4fa5fcacb0ec316a2758&client_secret=1be5777b6f9a6a4a09fa6cc3cdec62f3c579dbe5")
    fun getFeeds(@Path("username") username: String, @Query("page") page: Int): Single<List<FeedResponse>>
}
