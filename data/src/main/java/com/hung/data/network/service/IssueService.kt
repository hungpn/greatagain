package com.hung.data.network.service

import com.hung.data.model.issue.IssuesResponse
import com.hung.data.util.GITHUB_CLIENT_ID
import com.hung.data.util.GITHUB_SECRET
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface IssueService {

    // https://api.github.com/search/issues?q=type:issue+involves:hungpn+is:open&page=1
    @GET("search/issues")
    fun getIssues(
            @Query(value = "q", encoded = true) q: String,
            @Query("page") page: Int,
            @Query("client_id") clientId: String = GITHUB_CLIENT_ID,
            @Query("client_secret") clientSecret: String = GITHUB_SECRET
    ): Single<IssuesResponse>
}