package com.hung.data.network.service

import com.hung.data.model.login.AuthRequest
import com.hung.data.model.login.AuthResponse
import com.hung.data.model.user.UserEntity
import io.reactivex.Maybe
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface AuthService {

    @POST("authorizations")
    fun doBasicLogin(@Header("Authorization") basicToken: String, @Body request: AuthRequest): Single<AuthResponse>
}
