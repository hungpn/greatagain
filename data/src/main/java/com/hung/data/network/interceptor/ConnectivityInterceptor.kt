package com.hung.data.network.interceptor

import android.content.Context
import com.hung.data.exception.NoConnectionException
import com.hung.data.util.NetworkUtil
import okhttp3.Interceptor
import okhttp3.Response


class ConnectivityInterceptor(private val mContext: Context) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        if (NetworkUtil.isNotConnected(mContext)) {
            throw NoConnectionException("No internet connection. Please check your connection and try again.")
        }

        val builder = chain.request().newBuilder()
        return chain.proceed(builder.build())
    }
}