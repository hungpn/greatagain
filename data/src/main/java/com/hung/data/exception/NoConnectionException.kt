package com.hung.data.exception

class NoConnectionException(msg: String) : RuntimeException(msg)