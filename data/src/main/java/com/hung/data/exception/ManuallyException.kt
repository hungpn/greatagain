package com.hung.data.exception

class ManuallyException(msg: String) : RuntimeException(msg)