package com.hung.data.repo

import com.hung.data.model.login.AuthResponse
import com.hung.data.model.login.AuthResponseToDomainMapper
import com.hung.data.network.service.AuthService
import com.hung.data.repository.AuthRepositoryImpl
import com.hung.data.database.SharePreferenceManager
import com.hung.domain.model.login.Auth
import com.hung.domain.repository.AuthRepository
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mock

class AuthRepositoryImplTest : BaseRepoTest<AuthRepository>() {

    @Mock
    private lateinit var mSharedPreferences: SharePreferenceManager

    @Mock
    private lateinit var mAuthService: AuthService

    @Mock
    private lateinit var mAuthResponseToDomainMapper: AuthResponseToDomainMapper

    // dummy data
    private val authResponse = AuthResponse(
        id = 0,
        url = "url",
        token = "token",
        hashedToken = "hashedToken",
        tokenLastEight = "tokenLastEight"
    )

    private val auth = Auth(
        id = 0,
        url = "url",
        token = "token"
    )

    override fun createRepo() =
        AuthRepositoryImpl(
            mSharedPreferences,
            mAuthService,
            mAuthResponseToDomainMapper
        )

    @Test
    fun `do login from server successfully complete`() {
        `stub login from server`(Single.just(authResponse))

        val testObservable = mRepository.doBasicLogin("userName", "password")
            .test()

        // test case
        testObservable.assertComplete()
    }

    @Test
    fun `do login from server successfully emits auth object`() {
        `stub login from server`(Single.just(authResponse))

        val testObservable = mRepository.doBasicLogin("userName", "password")
            .test()

        // test case
        testObservable.assertValue {
            it == auth
        }
        verify(mSharedPreferences, times(1)).userToken = authResponse.hashedToken
    }


    @Test
    fun `do login from server successfully error`() {
        val errorMsg = "loginId error"
        `stub login from server`(Single.error(Throwable(errorMsg)))

        val testObservable = mRepository.doBasicLogin("userName", "password")
            .test()

        // test case
        testObservable.assertError {
            it.message == errorMsg
        }
    }

    private fun `stub login from server`(singleObservable: Single<AuthResponse>) {
        whenever(mAuthService.doBasicLogin(any(), any())).thenReturn(singleObservable)
        whenever(mAuthResponseToDomainMapper.map(authResponse)).thenReturn(auth)
    }
}