package com.hung.data.repo

import androidx.annotation.CallSuper
import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
abstract class BaseRepoTest<R : Any> {

    protected lateinit var mRepository : R

    @Before
    @CallSuper
    open fun setup() {
        mRepository = createRepo()
    }

    abstract fun createRepo() : R
}