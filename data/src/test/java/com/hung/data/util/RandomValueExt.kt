package com.hung.data.util

import java.util.*
import java.util.concurrent.ThreadLocalRandom

class RandomValueExt {

    companion object {

        @JvmStatic
        fun randomUUID() = UUID.randomUUID()

        @JvmStatic
        fun randomInt() = ThreadLocalRandom.current().nextInt(0, 1000 + 1)

        @JvmStatic
        fun randomLong() = randomInt().toLong()

        @JvmStatic
        fun randomBoolean() = Math.random() < 0.5
    }
}