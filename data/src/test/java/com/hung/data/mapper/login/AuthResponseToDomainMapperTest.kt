package com.hung.data.mapper.login

import com.hung.data.mapper.BaseMapperTest
import com.hung.data.model.login.AuthResponse
import com.hung.data.model.login.AuthResponseToDomainMapper
import com.hung.domain.model.login.Auth
import org.junit.Test

class AuthResponseToDomainMapperTest : BaseMapperTest<AuthResponseToDomainMapper>() {

    override fun createMapper() = AuthResponseToDomainMapper()

    @Test
    fun responseToDomain() {
        val authResponse = AuthResponse(
            id = 10,
            url = "url",
            token = "123",
            hashedToken = "321",
            tokenLastEight = "abc"
        )

        val auth = Auth(
            id = 10,
            url = "url",
            token = "123"
        )

        val result = mMapper.map(authResponse)

        // test case
        assert(auth == result)
    }
}