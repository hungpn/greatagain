package com.hung.data.mapper.user

import com.hung.data.mapper.BaseMapperTest
import com.hung.data.model.user.UserEntity
import com.hung.data.model.user.UserEntityToDomainMapper
import com.hung.domain.model.user.User
import org.junit.Test

class UserEntityToDomainMapperTest : BaseMapperTest<UserEntityToDomainMapper>() {

    // dummy data
    private lateinit var user: User
    private lateinit var userEntity: UserEntity

    override fun createMapper() = UserEntityToDomainMapper()

    override fun setup() {
        super.setup()

        user = DummyUserFactory.createDummyDomainUser()
        userEntity = DummyUserFactory.createDummyEntityUser()
    }

    @Test
    fun `user entity to domain`() {
        val result = mMapper.map(userEntity)

        // test case
        assert(result == user)
    }
}