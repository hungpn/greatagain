package com.hung.data.mapper

import com.hung.data.model.Mapper
import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
abstract class BaseMapperTest<M : Mapper<*, *>> {

    protected lateinit var mMapper: M

    @Before
    open fun setup() {
        mMapper = createMapper()
    }

    abstract fun createMapper(): M
}