package com.hung.data.mapper.user

import com.hung.data.model.user.UserEntity
import com.hung.domain.model.user.User
import java.util.*

class DummyUserFactory {

    companion object {
        fun createDummyDomainUser() = User(
            id = 10,
            name = "vu",
            loginId = "vudk",
            avatarUrl = "http://github.com/vudk.png",
            htmlUrl = "http://github.com/vudk",
            followers = 10,
            following = 10
           /* company = "Github",
            location = "HCM",
            email = "abc@gmail.com",
            blog = "hung.com",
            bio = "test",
            joinedTime = Date()*/
        )

        fun createDummyEntityUser() = UserEntity(
            id = 10,
            name = "vu",
            login = "vudk",
            avatarUrl = "http://github.com/vudk.png",
            htmlUrl = "http://github.com/vudk",
            followers = 10,
            following = 10
            /*company = "Github",
            location = "HCM",
            email = "abc@gmail.com",
            blog = "hung.com",
            bio = "test",
            joinedDate = Date()*/
        )
    }
}