package com.hung.data.mapper.user

import com.hung.data.mapper.BaseMapperTest
import com.hung.data.model.user.UserDomainToEntityMapper
import com.hung.data.model.user.UserEntity
import com.hung.domain.model.user.User
import org.junit.Test

class UserDomainToEntityMapperTest : BaseMapperTest<UserDomainToEntityMapper>() {

    // dummy data
    private lateinit var user: User
    private lateinit var userEntity: UserEntity

    override fun createMapper() = UserDomainToEntityMapper()

    override fun setup() {
        super.setup()

        user = DummyUserFactory.createDummyDomainUser()
        userEntity = DummyUserFactory.createDummyEntityUser()
    }

    @Test
    fun `user domain to entity`() {
        val result = mMapper.map(user)

        // test case
        assert(result == userEntity)
    }

/*    @Test
    fun `user domain to entity, set as current user`() {
        val result = mMapper.map(user).apply {
            isCurrentUser = true
        }

        userEntity.isCurrentUser = true

        // test case
        assert(result == userEntity)
    }*/
}