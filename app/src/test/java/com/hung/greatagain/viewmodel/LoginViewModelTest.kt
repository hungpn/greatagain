package com.hung.greatagain.viewmodel

import android.util.Log
import androidx.lifecycle.Observer
import com.hung.data.exception.NoConnectionException
import com.hung.domain.model.login.Auth
import com.hung.domain.model.user.User
import com.hung.domain.usecase.DoBasicLoginUseCase
import com.hung.domain.usecase.GetUserByLoginIdUseCase
import com.hung.greatagain.base.model.DataState
import com.hung.greatagain.base.model.DataState.*
import com.hung.greatagain.base.model.NetworkState
import com.hung.greatagain.ui.feature.login.AuthUserWrapper
import com.hung.greatagain.ui.feature.login.LoginViewModel
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.internal.verification.Times
import org.powermock.api.mockito.PowerMockito

class LoginViewModelTest : BaseViewModelTest<LoginViewModel>() {

    @Mock
    private lateinit var mDoBasicLoginUseCase: DoBasicLoginUseCase

    @Mock
    private lateinit var mGetUserByLoginIdUseCase: GetUserByLoginIdUseCase

    private val mLoginDataStateObserver: Observer<DataState<AuthUserWrapper>> = mock()
    private val mNetworkStateObserver: Observer<NetworkState> = mock()

    // dummy data
    private lateinit var mUserName: String
    private lateinit var mPassword: String
    private lateinit var mParam: DoBasicLoginUseCase.Param
    private lateinit var mAuth: Auth
    private lateinit var mAuthUserWrapper: AuthUserWrapper
    private lateinit var mUser: User

    /*private val mAnyNonNullUser = User()
    private val mAnyNonNullUserSingle = Single.just(mAnyNonNullUser)*/

    @Mock
    private lateinit var mUserObservable: Single<User>

    @Before
    override fun setUp() {
        super.setUp()

        PowerMockito.mockStatic(Log::class.java)
        Mockito.`when`(Log.i(any(), any())).then {
            println(it.arguments[1] as String)
            1
        }

        mUserName = "userName"
        mPassword = "password"
        mParam = DoBasicLoginUseCase.Param(
            userName = mUserName,
            password = mPassword
        )

        mAuth = Auth(id = 0, url = "url", token = "token")
        mUser = User(id = 0, name = "hung", loginId = "hungpn")
        mAuthUserWrapper = AuthUserWrapper(auth = mAuth, user = mUser)
    }

    override fun createViewModel() = LoginViewModel(
        mDoBasicLoginUseCase,
        mGetUserByLoginIdUseCase
    )

    @Test
    fun `do basic login success, test DataState Loading`() {
        // arrange
        whenever(mDoBasicLoginUseCase.execute(any()))
            .thenReturn(Single.just(mAuth))
        whenever(mGetUserByLoginIdUseCase.execute(any()))
            .thenReturn(Single.just(mUser))

        mViewModel.authUserWrapper.observeForever(mLoginDataStateObserver)

        // action
        mViewModel.doBasicLogin(mUserName, mPassword)

        // assert
        verify(mLoginDataStateObserver, Times(1)).onChanged(Loading(true))
        verify(mLoginDataStateObserver, Times(1)).onChanged(Loading(false))
    }

    @Test
    fun `do basic login fail, test DataState Loading`() {
        // arrange
        whenever(mDoBasicLoginUseCase.execute(any()))
            .thenReturn(Single.error(Throwable("loginId error")))
        whenever(mGetUserByLoginIdUseCase.execute(any()))
            .thenReturn(Single.just(mUser))

        mViewModel.authUserWrapper.observeForever(mLoginDataStateObserver)

        // action
        mViewModel.doBasicLogin(mUserName, mPassword)

        // assert
        verify(mLoginDataStateObserver, Times(1)).onChanged(Loading(true))
        verify(mLoginDataStateObserver, Times(1)).onChanged(Loading(false))
    }

    @Test
    fun `do basic login success, test DataState Success`() {
        // arrange
        whenever(mDoBasicLoginUseCase.execute(any()))
            .thenReturn(Single.just(mAuth))
        whenever(mGetUserByLoginIdUseCase.execute(any()))
            .thenReturn(Single.just(mUser))

        mViewModel.authUserWrapper.observeForever(mLoginDataStateObserver)

        // action
        mViewModel.doBasicLogin(mUserName, mPassword)

        // assert
        verify(mLoginDataStateObserver, Times(1))
            .onChanged(Success(mAuthUserWrapper))
    }

    @Test
    fun `do basic login fail, test DataState Failure`() {
        // arrange
        whenever(mDoBasicLoginUseCase.execute(any()))
            .thenReturn(Single.error(Throwable("loginId error")))
        whenever(mGetUserByLoginIdUseCase.execute(any()))
            .thenReturn(Single.just(mUser))

        mViewModel.authUserWrapper.observeForever(mLoginDataStateObserver)

        // action
        mViewModel.doBasicLogin(mUserName, mPassword)

        // assert
        verify(mLoginDataStateObserver, Times(1)).onChanged(any<Failure<AuthUserWrapper>>())
    }

    @Test
    fun `do basic login fail with NoConnection exception, test DataState Failure`() {
        // arrange
        val errorMsg = "no internet"
        whenever(mDoBasicLoginUseCase.execute(any()))
            .thenReturn(Single.error(NoConnectionException(errorMsg)))
        whenever(mGetUserByLoginIdUseCase.execute(any()))
            .thenReturn(Single.just(mUser))

        mViewModel.authUserWrapper.observeForever(mLoginDataStateObserver)
        mViewModel.mNetworkStateLiveData.observeForever(mNetworkStateObserver)

        // action
        mViewModel.doBasicLogin(mUserName, mPassword)

        // assert
        verify(mLoginDataStateObserver, never()).onChanged(any<Failure<AuthUserWrapper>>())
        verify(mNetworkStateObserver, Times(1)).onChanged(NetworkState.NoConnection(errorMsg))
    }
}