package com.hung.greatagain.viewmodel

import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.ViewModel
import com.hung.domain.usecase.DoBasicLoginUseCase
import com.hung.domain.usecase.GetUserByLoginIdUseCase
import com.hung.greatagain.rule.TrampolineSchedulerRule
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.powermock.core.classloader.annotations.PowerMockIgnore
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner

/*
    https://stackoverflow.com/questions/16520699/mockito-powermock-linkageerror-while-mocking-system-class/21268013#21268013
 */
@RunWith(PowerMockRunner::class)
@PrepareForTest(
    value = [
        DoBasicLoginUseCase::class,
        GetUserByLoginIdUseCase::class,
        Log::class
    ]
)
@PowerMockIgnore("javax.management.*")
abstract class BaseViewModelTest<V : ViewModel> {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val testSchedulerRule = TrampolineSchedulerRule()

    lateinit var mViewModel: V

    @Before
    open fun setUp() {
        mViewModel = createViewModel()
    }

    abstract fun createViewModel(): V
}