package com.hung.greatagain.viewmodel

import org.junit.Test


class Test {

    @Test
    fun testReplace() {
        val input = """<p class="description">Hello</p>"""
        val result = """<a class="description">Hello</a>"""

        val output = input
                .replace("<p", "<a")
                .replace("</p>", "</a>")

        assert(output == result)
    }
}