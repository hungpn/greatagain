package com.hung.greatagain.ui.activity

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.hung.greatagain.R
import com.hung.greatagain.ui.feature.login.LoginActivity
import com.hung.greatagain.waitId
import com.hung.share.tuple.Tuple
import org.junit.Rule
import org.junit.Test
import java.util.concurrent.TimeUnit

@LargeTest
class LoginActivityTest : BaseLoginUITest() {

    @Rule
    @JvmField
    var rule = ActivityTestRule(LoginActivity::class.java)

    private val incorrectUserNamePassword = Tuple("username", "password")
    private val correctUserNamePassword = Tuple("username", "password")

    fun setUp() {

    }

    @Test
    fun test_login_success_not_show_error_view() {
        // arrange
        onView(withId(R.id.edtUsername))
                .perform(clearText())
                .perform(typeText(incorrectUserNamePassword._1))

        onView(withId(R.id.edtPassword))
                .perform(clearText())
                .perform(typeText(incorrectUserNamePassword._2))

        onView(withId(R.id.edtPassword))
                .perform(closeSoftKeyboard())

        // action
        onView(withId(R.id.btnLogin))
                .perform(click())


        // assert
        onView(isRoot())
                .perform(waitId(R.id.rlIncorrectAuth, TimeUnit.SECONDS.toMillis(5)))
                .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
    }
}