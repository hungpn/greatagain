package com.hung.greatagain.widget.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.hung.greatagain.R
import com.hung.greatagain.databinding.FragmentSimpleLoadingBinding

class SimpleLoadingDialogFragment : DialogFragment() {

    companion object {
        var TAG = SimpleLoadingDialogFragment::class.java.canonicalName
    }

    private lateinit var mViewBinding: FragmentSimpleLoadingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mViewBinding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_simple_loading,
                container,
                false
            )
        return mViewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    fun show(fragmentManager: FragmentManager) {
        val transaction = fragmentManager.beginTransaction()
        val fragment = fragmentManager.findFragmentByTag(TAG)
        if (fragment != null) {
            transaction.remove(fragment)
        }
        transaction.addToBackStack(null)
        show(transaction, TAG)
    }
}