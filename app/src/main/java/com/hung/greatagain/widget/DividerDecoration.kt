package com.hung.greatagain.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.RecyclerView

class DividerDecoration : RecyclerView.ItemDecoration {

    private var hasLoadMore: Boolean = false
    private var drawableDivider: Drawable? = null

    constructor(context: Context, hasLoadMore: Boolean) {
        this.hasLoadMore = hasLoadMore
        val styledAttributes = context.obtainStyledAttributes(ATTRS)
        drawableDivider = styledAttributes.getDrawable(0)
        styledAttributes.recycle()
    }

    constructor(context: Context, hasLoadMore: Boolean, @DrawableRes resId: Int) {
        this.hasLoadMore = hasLoadMore
        this.drawableDivider = context.getDrawable(resId)
    }

    override fun onDraw(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val left = parent.paddingLeft
        val right = parent.width - parent.paddingRight

        val childCount = parent.childCount
        var delta = 0
        if (hasLoadMore) {
            delta = 1
        }
        for (i in 0 until childCount - delta) {
            val child = parent.getChildAt(i)

            val params = child.layoutParams as RecyclerView.LayoutParams

            val top = child.bottom + params.bottomMargin

            val bottom = top + (drawableDivider?.intrinsicHeight ?: 0)

            drawableDivider?.setBounds(left, top, right, bottom)
            drawableDivider?.draw(canvas)
        }
    }

    companion object {

        private val ATTRS = intArrayOf(android.R.attr.listDivider)
    }
}
