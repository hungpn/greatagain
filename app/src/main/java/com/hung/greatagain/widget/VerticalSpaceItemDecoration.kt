package com.hung.greatagain.widget

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class VerticalSpaceItemDecoration(private var verticalSpaceHeight: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                state: RecyclerView.State) {
        val itemPosition = (view.layoutParams as RecyclerView.LayoutParams).viewAdapterPosition
        if (itemPosition == 0) {
            outRect.top = verticalSpaceHeight
        }
        outRect.bottom = verticalSpaceHeight
        if (itemPosition == parent.adapter!!.itemCount - 1) {
            outRect.bottom = verticalSpaceHeight * 2
        }
    }
}