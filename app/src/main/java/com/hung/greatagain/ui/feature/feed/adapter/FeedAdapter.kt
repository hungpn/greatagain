package com.hung.greatagain.ui.feature.feed.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.hung.greatagain.R
import com.hung.greatagain.databinding.ItemNewFeedActionBinding
import com.hung.greatagain.ui.feature.feed.adapter.holder.*
import com.hung.greatagain.ui.feature.feed.adapter.item.*
import java.util.concurrent.Executor

class FeedAdapter(
        var executor: Executor,
        diffCallback: DiffUtil.ItemCallback<BaseFeedItem> = object : DiffUtil.ItemCallback<BaseFeedItem>() {
            override fun areItemsTheSame(oldItem: BaseFeedItem, newItem: BaseFeedItem): Boolean {
                return oldItem.feed.id == newItem.feed.id
            }

            override fun areContentsTheSame(oldItem: BaseFeedItem, newItem: BaseFeedItem): Boolean {
                return oldItem.feed == newItem.feed
            }

        }
) : ListAdapter<BaseFeedItem, BaseFeedViewHolder<*>>(
        AsyncDifferConfig.Builder(diffCallback)
                .setBackgroundThreadExecutor(executor)
                .build()
) {


    override fun getItemViewType(position: Int): Int {
        return getItem(position).viewType
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseFeedViewHolder<*> {
        val binding = DataBindingUtil.inflate<ItemNewFeedActionBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_new_feed_action,
                parent,
                false
        )

        return when (viewType) {
            BaseFeedItem.TYPE_ITEM_FEEDS_CREATED -> {
                CreatedViewHolder(binding)
            }
            BaseFeedItem.TYPE_ITEM_FEEDS_FORKED -> {
                ForkedViewHolder(binding)
            }
            BaseFeedItem.TYPE_ITEM_FEEDS_PUBLIC -> {
                PublicViewHolder(binding)
            }
            BaseFeedItem.TYPE_ITEM_FEEDS_PUSHED -> {
                PushedViewHolder(binding)
            }
            BaseFeedItem.TYPE_ITEM_FEEDS_WATCH -> {
                StartedViewHolder(binding)
            }
            else -> {
                NoDefinedViewHolder(binding)
            }
        }
    }

    override fun onBindViewHolder(holder: BaseFeedViewHolder<*>, position: Int) {
        when (holder) {
            is CreatedViewHolder -> {
                val item = getItem(position) as? CreatedItem
                        ?: return
                holder.onBinding(item)
            }

            is ForkedViewHolder -> {
                val item = getItem(position) as? ForkedItem
                        ?: return
                holder.onBinding(item)
            }

            is PublicViewHolder -> {
                val item = getItem(position) as? PublicItem
                        ?: return
                holder.onBinding(item)
            }

            is StartedViewHolder -> {
                val item = getItem(position) as? StarredItem
                        ?: return
                holder.onBinding(item)
            }

            is PushedViewHolder -> {
                val item = getItem(position) as? PushedItem
                        ?: return
                holder.onBinding(item)
            }

            else -> {
                (holder as? NoDefinedViewHolder)?.onBinding(NoDefinedItem())
            }
        }
    }
}