package com.hung.greatagain.ui.feature.profile.tab.overview.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.hung.greatagain.R
import com.hung.greatagain.base.adapter.DataBoundViewHolder
import com.hung.greatagain.databinding.ItemProfileHeaderInfoBinding
import com.hung.greatagain.util.loadImg

class ProfileOverviewHeaderInfoViewHolder(
        parent: ViewGroup
) : DataBoundViewHolder<ProfileOverviewHeaderInfoItem, ItemProfileHeaderInfoBinding>(
        DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_profile_header_info,
                parent,
                false
        )
) {

    override fun onBind(item: ProfileOverviewHeaderInfoItem) {
        binding.apply {
            url = item.avatarUrl
            fullName = item.fullName
            loginId = item.userName
            description = item.tvDescription
        }
    }
}