package com.hung.greatagain.ui.feature.issue.pager.adapter.holder

import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.hung.domain.model.issue.Issue
import com.hung.greatagain.R
import com.hung.greatagain.base.adapter.BaseAdapter
import com.hung.greatagain.databinding.ItemIssueBinding
import com.hung.greatagain.util.SpannableBuilder
import com.hung.share.DateTimeUtils
import com.hung.share.EMPTY

class IssueItemViewHolder(
        binding: ItemIssueBinding
) : BaseAdapter.BaseHolder<ItemIssueBinding, Issue>(binding) {

    override fun bindData(data: Issue) {
        mViewDataBinding.apply {
            tvTitle.text = data.title
            tvDescription.text = buildDescription(data)
            initTagsFlow(data)
        }
    }

    fun buildDescription(issue: Issue): SpannableBuilder {
        val spannableBuilder = SpannableBuilder.builder()

        spannableBuilder
                .bold(parseRepo(issue.htmlUrl))
                .append(" ")
                .append(DateTimeUtils.convertDateToString(issue.createdAt, "dd-MM-yyyy HH:mm:ss"))

        return spannableBuilder
    }

    private fun initTagsFlow(issue: Issue) {
        mViewDataBinding.flTags.removeAllViews()
        val labels = issue.labels
        if (labels != null && labels.isNotEmpty()) {
            val inflater = LayoutInflater.from(mViewDataBinding.root.context)
            for (label in labels) {
                val view = inflater.inflate(R.layout.item_tag, null, false) as ViewGroup
                val tvTag = view.findViewById<TextView>(R.id.tvTag)
                tvTag.text = label.name
                tvTag.setBackgroundColor(Color.parseColor("#" + label.color))
                mViewDataBinding.flTags.addView(view)
            }
        }
    }

    private fun parseRepo(url: String?): String {
        if (url.isNullOrEmpty()) {
            return EMPTY
        }

        val uri = Uri.parse(url)
        val segments = uri.pathSegments
        return if (segments == null || segments.size < 3) {
            EMPTY
        } else {
            val name = segments[0]
            val repoName = segments[1]
            val number = segments[3]
            "$name/$repoName#$number"
        }
    }
}