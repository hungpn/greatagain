package com.hung.greatagain.ui.feature.feed.adapter.item

import android.graphics.Color
import com.hung.domain.model.feed.Feed
import com.hung.greatagain.R
import com.hung.greatagain.util.SpannableBuilder

class PushedItem(feed: Feed) : BaseFeedItem(feed) {

    override fun buildTitle(): SpannableBuilder {
        val spannableBuilder = SpannableBuilder.builder()

        spannableBuilder
                .append(actorName)
                .bold(" pushed to ")
                .append(branchPushedTo)
                .bold(" at ")
                .append(repoNamePushTo)

        return spannableBuilder
    }

    override fun buildDescription(): SpannableBuilder {
        val spannableBuilder = SpannableBuilder.builder()

        feed.payload?.commits?.let { safeCommits ->
            spannableBuilder
                    .append(safeCommits.size.toString())
                    .append(" new commits")
                    .beginANewLine()
            for (i in safeCommits.indices) {
                val shortSHA = safeCommits[i].sha?.substring(0, 7)

                val message = safeCommits[i].message
                spannableBuilder
                        .foreground(shortSHA, Color.RED)
                        .append(" ")
                        .append(message)

                // drop a line if this is not end of list, otherwise does not
                if (i < safeCommits.size - 1) {
                    spannableBuilder.beginANewLine()
                }
            }
        }
        return spannableBuilder
    }

    override var timeIconRes = R.drawable.ic_commit;

    override var viewType = TYPE_ITEM_FEEDS_PUSHED
}
