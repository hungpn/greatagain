package com.hung.greatagain.ui.feature.profile.tab.overview

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.hung.domain.model.user.FollowStatusType
import com.hung.greatagain.R
import com.hung.greatagain.base.ViewModelFragment
import com.hung.greatagain.base.dialog.LoadingDialog
import com.hung.greatagain.base.model.DataState.*
import com.hung.greatagain.databinding.FragmentProfileOverviewBinding
import com.hung.greatagain.ui.feature.profile.ProfileViewModel
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.ProfileOverviewAdapter
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.ProfileOverviewFollowItem
import com.hung.greatagain.util.BundleKeyConst
import com.hung.share.EMPTY

class ProfileOverviewFragment :
        ViewModelFragment<ProfileOverviewViewModel, FragmentProfileOverviewBinding>() {

    companion object {

        fun newInstance(loginId: String) = ProfileOverviewFragment().apply {
            arguments = Bundle().apply {
                putString(BundleKeyConst.EXTRA_1, loginId)
            }
        }
    }

    private lateinit var mLoadingDialog: LoadingDialog

    private var mAdapter = ProfileOverviewAdapter(
            mOnFollowClick = {
                val targetStatus = when (it) {
                    ProfileOverviewFollowItem.FollowStatusButton.HIDE -> {
                        return@ProfileOverviewAdapter
                    }
                    ProfileOverviewFollowItem.FollowStatusButton.FOLLOW -> {
                        FollowStatusType.UN_FOLLOW
                    }
                    ProfileOverviewFollowItem.FollowStatusButton.UN_FOLLOW -> {
                        FollowStatusType.FOLLOW
                    }
                }
                mSharedProfileViewModel.doFollowChangeStatus(targetStatus)
            })

    private lateinit var mSharedProfileViewModel: ProfileViewModel

    override val mLayoutId = R.layout.fragment_profile_overview

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mSharedProfileViewModel = activity?.run {
            ViewModelProviders.of(this).get(ProfileViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initViewModel()
        initSharedViewModel()
    }

    private fun initView() {
        mLoadingDialog = LoadingDialog.newInstance(EMPTY).apply {
            isCancelable = true
        }

        mViewBinding.apply {
            rvProfileOverview.apply {
                //setHasFixedSize(true)
                layoutManager = LinearLayoutManager(
                        requireContext(),
                        LinearLayoutManager.VERTICAL,
                        false
                )

                adapter = mAdapter
            }
        }
    }

    private fun initViewModel() {
        mViewModel.apply {

            loginId = arguments?.getString(BundleKeyConst.EXTRA_1, EMPTY) ?: EMPTY

            profileOverviewDataStateItems.observe(this@ProfileOverviewFragment.viewLifecycleOwner,
                    Observer { profileOverviewItem ->
                        when (profileOverviewItem) {
                            is Loading -> {
                                if (profileOverviewItem.isLoading) {
                                    mLoadingDialog.show(fragmentManager, LoadingDialog::javaClass.name)
                                } else {
                                    mLoadingDialog.dismiss()
                                }
                            }
                            is Success -> {
                                mAdapter.submitList(null)
                                mAdapter.submitList(profileOverviewItem.data)
                            }
                            is Failure -> {

                            }
                        }
                    })
        }

        mSharedProfileViewModel.targetFollowStatusLiveData.observe(this@ProfileOverviewFragment,
                Observer {
                    mViewModel.changeTargetFollowStatusOnDone(it)
                })
    }

    private fun initSharedViewModel() {
        mSharedProfileViewModel.apply {
            userLiveData.observe(
                    this@ProfileOverviewFragment.viewLifecycleOwner,
                    Observer { user ->
                        mViewModel.handleGetUserInfoOnDone(user)
                    })
        }
    }
}