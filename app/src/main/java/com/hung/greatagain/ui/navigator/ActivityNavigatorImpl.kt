package com.hung.greatagain.ui.navigator

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

open class ActivityNavigatorImpl(
        private val activity: AppCompatActivity
) : ActivityNavigator {

    init {
        //Log.d("hung123", "ActivityNavigatorImpl=${this.hashCode()}")
    }

    override fun finish() {
        activity.finish()
    }

    override fun startActivity(intent: Intent) {
        activity.startActivity(intent)
    }

    override fun startActivity(clazz: Class<out Activity>) {
        val intent = Intent(activity, clazz)
        activity.startActivity(intent)
    }

    override fun startActivity(clazz: Class<out Activity>, bundle: Bundle) {
        //TODO
    }

    override fun startActivityForResult(clazz: Class<out Activity>, requestCode: Int) {
        //TODO
    }

    override fun replaceFragment(@IdRes idResource: Int, fragment: Fragment) {
        replaceFragment(
                activity.supportFragmentManager,
                idResource,
                fragment,
                null,
                null,
                false,
                null
        )
    }

    override fun replaceFragment(@IdRes idResource: Int, fragment: Fragment, bundle: Bundle) {
        replaceFragment(
                activity.supportFragmentManager,
                idResource,
                fragment,
                null,
                bundle,
                false,
                null
        )
    }

    private fun replaceFragment(
            fragmentManager: FragmentManager,
            @IdRes idResource: Int,
            fragment: Fragment,
            tag: String?,
            args: Bundle?,
            hasAddToBackStack: Boolean,
            backStackTag: String?,
            vararg transitionView: View
    ) {
        if (args != null) {
            fragment.arguments = args
        }

        val ft = fragmentManager.beginTransaction()
        ft.replace(idResource, fragment, tag)
        if (hasAddToBackStack) {
            ft.addToBackStack(tag).commit()
            fragmentManager.executePendingTransactions()
        } else {
            ft.commitNow()
        }
    }

    override fun <T : Fragment?> findFragmentByTag(tag: String): T? {
        //TODO
        return null
    }

    override fun <T : Fragment?> findFragmentById(idResource: Int): T? {
        //TODO
        return null
    }

    override fun getCurrentFragment(fragmentManager: FragmentManager): Fragment? {
        val fragments = fragmentManager.fragments
        if (fragments.isNotEmpty()) {
            for (f in fragments) {
                if (f != null && f.isVisible) {
                    return f
                }
            }
        }
        return null
    }
}
