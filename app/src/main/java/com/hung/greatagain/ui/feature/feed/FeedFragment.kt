package com.hung.greatagain.ui.feature.feed

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.hung.greatagain.R
import com.hung.greatagain.base.ViewModelFragment
import com.hung.greatagain.base.adapter.VerticalSpacingDecoration
import com.hung.greatagain.base.model.DataState.*
import com.hung.greatagain.base.model.LoadMoreListState
import com.hung.greatagain.databinding.FragmentFeedBinding
import com.hung.greatagain.ui.feature.feed.adapter.loadmore.LoadMoreFeedAdapter
import com.hung.greatagain.util.BundleKeyConst
import com.hung.share.EMPTY
import com.hung.share.ext.negative

class FeedFragment : ViewModelFragment<FeedViewModel, FragmentFeedBinding>() {

    companion object {

        fun newInstance(loginId: String)=  FeedFragment().apply {
            arguments = Bundle().apply {
                putString(BundleKeyConst.EXTRA_1, loginId)
            }
        }
    }

    override val mLayoutId = R.layout.fragment_feed

    private lateinit var mAdapter: LoadMoreFeedAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initViewModel()
    }

    private fun initView() {
        mAdapter =
                LoadMoreFeedAdapter(
                        onLoadMore = {
                            mViewModel.getFeeds()
                        },
                        onRetry = {
                            mViewModel.getFeeds()
                        }
                )
        mAdapter.setLoadMoreCondition {
            mViewModel.isEnd.negative()
        }

        mViewBinding.rvFeeds.apply {
            setLoadMoreAdapter(adapter = mAdapter)
            addItemDecoration(VerticalSpacingDecoration(50, false))
        }

        mViewBinding.srlFeeds.setOnRefreshListener {
            mAdapter.reset()
            mViewModel.getFeeds(true)
        }

    }

    private fun initViewModel() {
        mViewModel.apply {
            loginId = arguments?.getString(BundleKeyConst.EXTRA_1, EMPTY) ?: EMPTY
            getFeeds(true)
            feedsState.observe(this@FeedFragment.viewLifecycleOwner, Observer { feedsState ->
                when (feedsState) {
                    is Loading -> {
                        if (feedsState.isRefresh) {
                            mViewBinding.srlFeeds.isRefreshing = feedsState.isLoading
                        }
                        mAdapter.setLoading(feedsState.isLoading)
                    }
                    is Success -> {
                        feedsState.apply {
                            when {
                                data.isNotEmpty() && !isComplete -> mAdapter.update(data)
                                isComplete -> mAdapter.setLoadMoreState(LoadMoreListState.COMPLETE)
                            }
                        }
                    }
                    is Failure -> {
                        if (mViewBinding.srlFeeds.isRefreshing) {
                            mViewBinding.srlFeeds.isRefreshing = false
                        }
                        mAdapter.setLoadMoreState(LoadMoreListState.ERROR)
                    }
                }
            })
        }
    }
}
