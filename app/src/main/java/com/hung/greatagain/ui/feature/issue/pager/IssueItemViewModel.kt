package com.hung.greatagain.ui.feature.issue.pager

import androidx.lifecycle.MutableLiveData
import com.hung.domain.model.issue.Issue
import com.hung.domain.usecase.GetIssueUseCase
import com.hung.greatagain.base.BaseViewModel
import com.hung.greatagain.base.model.DataState
import com.hung.greatagain.ui.feature.issue.model.IssueType
import com.hung.share.EMPTY
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class IssueItemViewModel @Inject constructor(
        private val mGetIssueUseCase: GetIssueUseCase
) : BaseViewModel() {

    companion object {
        const val STARTING_PAGE = 1
    }

    val mIssuesState = MutableLiveData<DataState<List<Issue>>>()
    private var mIssues = listOf<Issue>()

    //Data object
    var mIssuesType = IssueType.UNKNOW
    var mLoginId: String = EMPTY

    val mStatus = "open" //TODO
    var mCurrentPage = STARTING_PAGE
    var isEnd = false

    fun getIssues(isRefresh: Boolean = false) {
        if (mIssuesType == IssueType.UNKNOW) {
            return
        }

        if (isRefresh) {
            mCurrentPage = STARTING_PAGE
            mIssues = emptyList()
            isEnd = false
        }

        // patter:https://api.github.com/search/issues?q=type:issue+author:hungpn+is:open&page=1
        val issuesQuery = StringBuilder()
        issuesQuery
                .append("type:issue+")
                .append("is:$mStatus+")

        when (mIssuesType) {
            IssueType.CREATED -> {
                issuesQuery.append("author:").append(mLoginId)
            }
            IssueType.ASSIGNED -> {
                issuesQuery.append("assignee:").append(mLoginId)
            }
            IssueType.MENTIONED -> {
                issuesQuery.append("mentions:").append(mLoginId)
            }
            else -> {

            }
        }

        mGetIssueUseCase.execute(
                GetIssueUseCase.Param(
                        q = issuesQuery.toString(),
                        page = mCurrentPage
                ))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    mIssuesState.value = DataState.Loading(true, isRefresh)
                }
                .doFinally {
                    mIssuesState.value = DataState.Loading(false, isRefresh)
                }
                .subscribe({
                    val issues = it.items
                    if (issues.isNotEmpty() && mIssues.size < it.totalCount) {
                        mCurrentPage++
                        mIssues = mIssues + issues
                        mIssuesState.value = DataState.Success(issues)
                    } else {
                        mIssuesState.value = DataState.Success(issues, true)
                    }
                }, {
                    handleCommonError(it) {
                        mIssuesState.value = DataState.Failure(it)
                    }
                }).addToDisposables()
    }
}