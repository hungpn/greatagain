package com.hung.greatagain.ui.feature.profile.tab.overview.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.hung.greatagain.R
import com.hung.greatagain.base.adapter.DataBoundViewHolder
import com.hung.greatagain.databinding.ItemProfileOverviewDividerBinding

class ProfileOverviewDividerViewHolder(
        parent: ViewGroup
) : DataBoundViewHolder<ProfileOverviewDividerItem, ItemProfileOverviewDividerBinding>(
        DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_profile_overview_divider,
                parent,
                false
        )
) {

    override fun onBind(item: ProfileOverviewDividerItem) {
        binding.apply {

        }
    }
}