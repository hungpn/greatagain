package com.hung.greatagain.ui.feature.feed.adapter.loadmore.item

import com.hung.domain.model.feed.Feed
import com.hung.greatagain.R
import com.hung.greatagain.util.SpannableBuilder

class ForkedItem(feed: Feed) : BaseFeedItem(feed) {

    override fun buildTitle(): SpannableBuilder {
        val spannableBuilder = SpannableBuilder.builder()
        spannableBuilder.append(actorName)
                .bold(" forked ")
                .append(repoName)
        return spannableBuilder
    }

    override fun buildDescription(): SpannableBuilder {
        //TODO
        return SpannableBuilder.builder()
    }

    override var timeIconRes = R.drawable.ic_fork

    override var viewType = TYPE_ITEM_FEEDS_FORKED

    val payloadRef: String?
        get() {
            return feed.payload?.ref?.let { safeRef ->
                val prefix = "refs/heads/"
                if (safeRef.startsWith(prefix)) {
                    return safeRef.substring(prefix.length)
                }
                safeRef
            }
        }

    val repoName: String?
        get() = feed.repo?.name
}
