package com.hung.greatagain.ui.feature.feed.adapter.holder

import com.hung.greatagain.databinding.ItemNewFeedActionBinding
import com.hung.greatagain.ui.feature.feed.adapter.item.StarredItem

class StartedViewHolder(
    binding: ItemNewFeedActionBinding
) : BaseFeedViewHolder<StarredItem>(binding) {

    override fun onBinding(item: StarredItem) {
        super.onBinding(item)

    }
}