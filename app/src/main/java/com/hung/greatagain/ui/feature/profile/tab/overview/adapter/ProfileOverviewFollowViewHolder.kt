package com.hung.greatagain.ui.feature.profile.tab.overview.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.hung.greatagain.R
import com.hung.greatagain.base.adapter.DataBoundViewHolder
import com.hung.greatagain.databinding.ItemProfileFollowActionBinding
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.ProfileOverviewFollowItem.FollowStatusButton
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.ProfileOverviewFollowItem.FollowStatusButton.FOLLOW
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.ProfileOverviewFollowItem.FollowStatusButton.HIDE

class ProfileOverviewFollowViewHolder(
        parent: ViewGroup,
        private val mOnFollowClick: (currentStatusButton: FollowStatusButton) -> Unit
) : DataBoundViewHolder<ProfileOverviewFollowItem, ItemProfileFollowActionBinding>(
        DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_profile_follow_action,
                parent,
                false
        )
) {

    override fun onBind(item: ProfileOverviewFollowItem) {
        binding.apply {
            txtFollowers.text = context.getString(R.string.followers_number, item.followers)
            txtFollowing.text = context.getString(R.string.following_number, item.following)

            if (item.followStatusButton == HIDE) {
                btnFollow.visibility = View.GONE
            } else {
                btnFollow.visibility = View.VISIBLE
                if (item.followStatusButton == FOLLOW) {
                    btnFollow.text = context.getString(R.string.follow)
                } else {
                    btnFollow.text = context.getString(R.string.un_follow)
                }
            }

            btnFollow.setOnClickListener {
                mOnFollowClick(item.followStatusButton)
            }
        }
    }
}