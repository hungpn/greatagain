package com.hung.greatagain.ui.feature.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.hung.domain.model.login.Auth
import com.hung.domain.model.user.User
import com.hung.domain.usecase.DoBasicLoginUseCase
import com.hung.domain.usecase.GetUserByLoginIdUseCase
import com.hung.greatagain.base.BaseViewModel
import com.hung.greatagain.base.model.DataState
import com.hung.greatagain.base.model.DataState.*
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class LoginViewModel @Inject constructor(
        private val mDoBasicLoginUseCase: DoBasicLoginUseCase,
        private val mGetUserByLoginIdUseCase: GetUserByLoginIdUseCase
) : BaseViewModel() {

    private val _authUserWrapper = MutableLiveData<DataState<AuthUserWrapper>>()
    val authUserWrapper: LiveData<DataState<AuthUserWrapper>>
        get() = _authUserWrapper

    fun doBasicLogin(userName: String, password: String) {
        val authSource = mDoBasicLoginUseCase
                .execute(
                        DoBasicLoginUseCase.Param(
                                userName = userName,
                                password = password
                        )
                )

        val userSource = mGetUserByLoginIdUseCase
                .execute(
                        GetUserByLoginIdUseCase.Param(
                                loginId = userName
                        )
                )
                .onErrorReturnItem(User(id = -1))

        Single.zip(
                authSource,
                userSource,
                BiFunction<Auth, User, AuthUserWrapper> { auth, user ->
                    AuthUserWrapper(
                            auth = auth,
                            user = user
                    )
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    _authUserWrapper.value = Loading(true)
                }
                .doFinally {
                    _authUserWrapper.value = Loading(false)
                }
                .subscribe({
                    _authUserWrapper.value = Success(it)
                }, {
                    handleCommonError(it) {
                        _authUserWrapper.value = Failure(it)
                    }
                }).addToDisposables()
    }
}

data class AuthUserWrapper(
        val auth: Auth,
        val user: User
)