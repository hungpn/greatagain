package com.hung.greatagain.ui.feature.login

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.hung.domain.usecase.DoBasicLoginUseCase
import com.hung.greatagain.base.BaseStateViewModel
import com.hung.greatagain.base.model.Event
import com.hung.greatagain.util.event
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class LoginStateViewModel @Inject constructor(private val mDoBasicLoginUseCase: DoBasicLoginUseCase) :
        BaseStateViewModel<LoginViewState, LoginIntent>() {

    val isLoading = ObservableBoolean(false)
    val showIncorrectPassword = ObservableBoolean(false)
    val textUsername = ObservableField("")
    val textPassword = ObservableField("")
    val textSignIn = ObservableField("")

    override fun initState() = LoginViewState()

    override fun getIntentClass(): Class<LoginIntent> {
        return LoginIntent::class.java
    }

    override fun mapIntentToState(intent: LoginIntent): Observable<LoginViewState> {
        return Observable.create { emitter ->
            event(intent.loginRequest) {
                addDisposable {
                    mDoBasicLoginUseCase
                            .execute(
                                    DoBasicLoginUseCase.Param(
                                            userName = this.username,
                                            password = this.password
                                    )
                            )
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .doOnSubscribe {
                                emitter.onNext(
                                        currentState.copy(
                                                isLoading = Event(true)
                                        )
                                )
                            }
                            .doFinally {
                                emitter.onNext(
                                        currentState.copy(
                                                isLoading = Event(false)
                                        )
                                )
                            }
                            .subscribe({
                                emitter.onNext(
                                        currentState.copy(
                                                loginSuccess = Event(Unit)
                                        )
                                )
                            }, {
                                emitter.onNext(
                                        currentState.copy(
                                                isError = Event(true),
                                                isLoading = Event(false)
                                        )
                                )
                            })
                }
            }
        }
    }

    fun login() {
        disPatch {
            copy(
                    loginRequest = Event(
                            LoginIntent.LoginRequest(
                                    textUsername.get()!!,
                                    textPassword.get()!!
                            )
                    )
            )
        }
    }

    fun closeErrorMessage() {
        setState {
            copy(isError = Event(false))
        }
    }
}

data class LoginViewState(
        val isLoading: Event<Boolean> = Event(false),
//    val errorMessage: Event<String>? = null,
        val isError: Event<Boolean> = Event(false),
        val loginSuccess: Event<Unit>? = null
)

data class LoginIntent(
        var loginRequest: Event<LoginRequest>? = null
) {

    data class LoginRequest(val username: String, val password: String)
}