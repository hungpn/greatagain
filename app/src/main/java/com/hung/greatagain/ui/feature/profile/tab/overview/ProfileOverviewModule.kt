package com.hung.greatagain.ui.feature.profile.tab.overview

import androidx.lifecycle.ViewModel
import com.hung.greatagain.di.scope.PerFragment
import com.hung.greatagain.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class ProfileOverviewModule {

    @Binds
    @IntoMap
    @ViewModelKey(ProfileOverviewViewModel::class)
    abstract fun provideProfileOverviewViewModel(profileOverviewViewModel: ProfileOverviewViewModel): ViewModel

    @PerFragment
    @ContributesAndroidInjector
    internal abstract fun contributeProfileOverviewFragment(): ProfileOverviewFragment
}