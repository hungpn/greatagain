package com.hung.greatagain.ui.feature.issue.pager

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hung.domain.executor.UseCaseExecutor
import com.hung.greatagain.R
import com.hung.greatagain.base.ViewModelFragment
import com.hung.greatagain.base.model.DataState
import com.hung.greatagain.base.model.LoadMoreListState
import com.hung.greatagain.databinding.FragmentIssueItemBinding
import com.hung.greatagain.ui.feature.issue.model.IssueType
import com.hung.greatagain.ui.feature.issue.pager.adapter.IssueItemAdapter
import com.hung.greatagain.util.BundleKeyConst
import com.hung.greatagain.widget.DividerDecoration
import com.hung.share.EMPTY
import com.hung.share.ext.negative
import javax.inject.Inject

class IssueItemFragment : ViewModelFragment<IssueItemViewModel, FragmentIssueItemBinding>() {

    companion object {
        fun newInstance(issueType: IssueType, loginId: String) = IssueItemFragment()
                .apply {
                    arguments = Bundle().apply {
                        mTitle = getTitle(issueType)
                        putSerializable(BundleKeyConst.EXTRA_1, issueType)
                        putString(BundleKeyConst.EXTRA_2, loginId)
                    }
                }

        private fun getTitle(issuesType: IssueType): String {
            return when (issuesType) {
                IssueType.CREATED -> {
                    IssueType.CREATED.displayName
                }
                IssueType.ASSIGNED -> {
                    IssueType.ASSIGNED.displayName
                }
                IssueType.MENTIONED -> {
                    IssueType.MENTIONED.displayName
                }
                else -> {
                    IssueType.UNKNOW.displayName
                }
            }
        }
    }

    private lateinit var mAdapter: IssueItemAdapter

    var mTitle = IssueType.UNKNOW.displayName

    override val mLayoutId = R.layout.fragment_issue_item

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initViewModel()
    }

    private fun initView() {
        mAdapter = IssueItemAdapter(
                onLoadMore = {
                    mViewModel.getIssues()
                },
                onRetry = {
                    mViewModel.getIssues()
                })

        mAdapter.setLoadMoreCondition {
            mViewModel.isEnd.negative()
        }

        mViewBinding.apply {
            rvIssue.apply {
                setLoadMoreAdapter(mAdapter)
                //addItemDecoration(VerticalSpacingDecoration(50, false))
                layoutManager = LinearLayoutManager(
                        requireContext(),
                        RecyclerView.VERTICAL,
                        false
                )
                addItemDecoration(DividerDecoration(context, true))
            }

            srlIssue.setOnRefreshListener {
                mAdapter.reset()
                mViewModel.getIssues(isRefresh = true)
            }
        }
    }

    private fun initViewModel() {
        mViewModel.apply {

            arguments?.let { safeArguments ->
                mIssuesType = safeArguments.getSerializable(BundleKeyConst.EXTRA_1) as IssueType
                mLoginId = safeArguments.getString(BundleKeyConst.EXTRA_2, EMPTY)
            }

            getIssues()

            mIssuesState.observe(this@IssueItemFragment.viewLifecycleOwner,
                    Observer { issuesState ->
                        when (issuesState) {
                            is DataState.Loading -> {
                                if (issuesState.isRefresh) {
                                    mViewBinding.srlIssue.isRefreshing = issuesState.isLoading
                                }
                                mAdapter.setLoading(issuesState.isLoading)
                            }

                            is DataState.Success -> {
                                issuesState.apply {
                                    when {
                                        data.isNotEmpty() && !isComplete -> mAdapter.update(data)
                                        isComplete -> mAdapter.setLoadMoreState(LoadMoreListState.COMPLETE)
                                    }
                                }
                            }

                            is DataState.Failure -> {
                                if (mViewBinding.srlIssue.isRefreshing) {
                                    mViewBinding.srlIssue.isRefreshing = false
                                }
                                mAdapter.setLoadMoreState(LoadMoreListState.ERROR)
                            }
                        }
                    })
        }
    }
}