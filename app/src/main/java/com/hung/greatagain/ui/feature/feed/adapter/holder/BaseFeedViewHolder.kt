package com.hung.greatagain.ui.feature.feed.adapter.holder

import android.view.View
import androidx.annotation.CallSuper
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hung.greatagain.databinding.ItemNewFeedActionBinding
import com.hung.greatagain.ui.feature.feed.adapter.item.BaseFeedItem

open class BaseFeedViewHolder<in T : BaseFeedItem>(
        protected val mBinding: ViewDataBinding
) : RecyclerView.ViewHolder(mBinding.root) {

    @CallSuper
    open fun onBinding(item: T) {
        if (mBinding is ItemNewFeedActionBinding) {
            mBinding.apply {
                Glide.with(mBinding.root.context)
                        .asDrawable()
                        .load(item.actorAvatar)
                        .into(imvAvatar)

                tvTitle.text = item.buildTitle()
                tvDescription.apply {
                    val a = item.buildDescription()
                    if (a.toString().isEmpty()) {
                        visibility = View.GONE
                    } else {
                        visibility = View.VISIBLE
                        text = item.buildDescription()
                    }
                }

                time = item.buildTime.toString()
                timeIconRes = item.timeIconRes
            }
        }
    }
}