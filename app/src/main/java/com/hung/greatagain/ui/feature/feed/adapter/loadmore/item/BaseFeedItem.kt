package com.hung.greatagain.ui.feature.feed.adapter.loadmore.item

import androidx.annotation.DrawableRes
import com.hung.domain.model.feed.Feed
import com.hung.greatagain.util.AndroidDateTimeUtils
import com.hung.greatagain.util.SpannableBuilder

abstract class BaseFeedItem constructor(
        val feed: Feed = Feed()
) {

    companion object {
        const val TYPE_ITEM_FEEDS_NO_DEFINED = -1
        const val TYPE_ITEM_FEEDS_CREATED = 1
        const val TYPE_ITEM_FEEDS_FORKED = 2
        const val TYPE_ITEM_FEEDS_PUSHED = 3
        const val TYPE_ITEM_FEEDS_WATCH = 6 // starred
        const val TYPE_ITEM_FEEDS_PUBLIC = 5
    }

    abstract var viewType: Int

    abstract fun buildTitle(): SpannableBuilder

    abstract fun buildDescription(): SpannableBuilder

    var buildTime = AndroidDateTimeUtils.getTimeAgo(feed.createAt)

    @get:DrawableRes
    abstract var timeIconRes: Int

    val repoId: String?
        get() = feed.repo?.name

    // use for common
    val actorName: String?
        get() = feed.actor?.displayLogin

    // use for showing avatar
    val actorAvatar: String?
        get() = feed.actor?.avatarUrl

    // use for PUSH_EVENT
    val branchPushedTo: String?
        get() {
            return feed.payload?.ref?.let {
                val prefix = "refs/heads/"
                if (it.startsWith(prefix)) {
                    it.substring(prefix.length)
                }
                it
            }
        }

    // use for PUSH_EVENT
    val repoNamePushTo: String?
        get() = feed.repo?.name
}
