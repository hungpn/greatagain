package com.hung.greatagain.ui.feature.feed.adapter.holder

import com.hung.greatagain.databinding.ItemNewFeedActionBinding
import com.hung.greatagain.ui.feature.feed.adapter.item.ForkedItem

class ForkedViewHolder(
    binding: ItemNewFeedActionBinding
) : BaseFeedViewHolder<ForkedItem>(binding) {

    override fun onBinding(item: ForkedItem) {
        super.onBinding(item)

    }
}