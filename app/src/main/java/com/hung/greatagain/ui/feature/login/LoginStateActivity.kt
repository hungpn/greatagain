package com.hung.greatagain.ui.feature.login

import android.content.Intent
import android.os.Bundle
import com.hung.greatagain.R
import com.hung.greatagain.base.BaseStateActivity
import com.hung.greatagain.databinding.ActivityStateLoginBinding
import com.hung.greatagain.ui.feature.main.MainActivity
import com.hung.greatagain.util.event

class LoginStateActivity : BaseStateActivity<LoginViewState, LoginIntent, LoginStateViewModel, ActivityStateLoginBinding>() {

    override val mLayoutId = R.layout.activity_state_login

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewBinding.viewModel = mViewModel

        withState {
            event(isLoading) {
                mViewModel.isLoading.set(this)
            }

            event(isError) {
                mViewModel.showIncorrectPassword.set(this)
            }

            event(loginSuccess) {
                startActivity(Intent(this@LoginStateActivity, MainActivity::class.java))
            }
        }
    }
}