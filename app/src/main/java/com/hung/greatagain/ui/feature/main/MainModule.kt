package com.hung.greatagain.ui.feature.main

import androidx.lifecycle.ViewModel
import com.hung.greatagain.di.module.ActivityModule
import com.hung.greatagain.ui.navigator.ActivityNavigator
import com.hung.greatagain.ui.navigator.ActivityNavigatorImpl
import com.hung.greatagain.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Inject

@Module
class MainModule @Inject constructor(private val mActivity: MainActivity) {

}

@Module
internal abstract class MainViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel
}

@Module
class MainActivityModule : ActivityModule<MainActivity>() {

    @Provides
    fun provideNavigator(activity: MainActivity): ActivityNavigator = ActivityNavigatorImpl(activity)
}