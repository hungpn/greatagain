package com.hung.greatagain.ui.feature.issue.pager.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.hung.greatagain.ui.feature.issue.pager.IssueItemFragment

class IssuePagerAdapter(fm: FragmentManager, private val baseFragments: List<IssueItemFragment>) : FragmentStatePagerAdapter(fm) {

    override fun getPageTitle(position: Int): CharSequence? {
        return baseFragments[position].mTitle
    }

    override fun getItem(position: Int): Fragment {
        return baseFragments[position]
    }

    override fun getCount(): Int {
        return baseFragments.size
    }
}
