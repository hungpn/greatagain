package com.hung.greatagain.ui.feature.feed.adapter.item

import com.hung.domain.model.feed.Feed
import com.hung.greatagain.R
import com.hung.greatagain.util.SpannableBuilder

class StarredItem(feed: Feed) : BaseFeedItem(feed) {

    override fun buildTitle(): SpannableBuilder {
        val spannableBuilder = SpannableBuilder.builder()

        spannableBuilder
                .append(actorName)
                .bold(" starred ")
                .append(repoNamePushTo)

        return spannableBuilder
    }

    override fun buildDescription(): SpannableBuilder {
        //TODO
        return SpannableBuilder.builder()
    }

    override var timeIconRes = R.drawable.ic_star

    override var viewType = TYPE_ITEM_FEEDS_WATCH
}
