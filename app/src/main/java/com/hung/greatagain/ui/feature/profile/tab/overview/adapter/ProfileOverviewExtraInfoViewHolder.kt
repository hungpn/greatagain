package com.hung.greatagain.ui.feature.profile.tab.overview.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.hung.greatagain.R
import com.hung.greatagain.base.adapter.DataBoundViewHolder
import com.hung.greatagain.databinding.ItemProfileExtraInfoBinding

class ProfileOverviewExtraInfoViewHolder(
        parent: ViewGroup
) : DataBoundViewHolder<ProfileOverviewExtraInfoItem, ItemProfileExtraInfoBinding>(
        DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_profile_extra_info,
                parent,
                false
        )
) {

    override fun onBind(item: ProfileOverviewExtraInfoItem) {
        binding.apply {
            Glide.with(context)
                    .load(item.resId)
                    .centerInside()
                    .into(ivIcon)

            tvContent.text = item.content
        }
    }
}