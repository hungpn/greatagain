package com.hung.greatagain.ui.feature.profile.tab.overview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.hung.data.util.API_DATE_TIME_FORMAT_WITHOUT_HOUR
import com.hung.domain.model.user.FollowStatusType
import com.hung.domain.model.user.User
import com.hung.domain.usecase.user.CheckFollowingStatusUseCase
import com.hung.domain.usecase.user.GetContributeInputStream
import com.hung.domain.usecase.user.GetCurrentLoginIdUseCase
import com.hung.greatagain.R
import com.hung.greatagain.base.BaseViewModel
import com.hung.greatagain.base.model.DataState
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.*
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.ProfileOverviewFollowItem.FollowStatusButton.*
import com.hung.share.DateTimeUtils
import com.hung.share.EMPTY
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.io.InputStream
import javax.inject.Inject

class ProfileOverviewViewModel @Inject constructor(
        private val mGetFollowingStatusUseCase: CheckFollowingStatusUseCase,
        private val mGetContributeInputStream: GetContributeInputStream,
        private val mGetCurrentLoginIdUseCase: GetCurrentLoginIdUseCase
) : BaseViewModel() {

    var loginId: String = EMPTY

    private val currentLoginId: String
        get() = mGetCurrentLoginIdUseCase.execute()

    private val _profileOverviewDataStateItems =
            MutableLiveData<DataState<List<ProfileOverviewItem>>>()
    val profileOverviewDataStateItems: LiveData<DataState<List<ProfileOverviewItem>>>
        get() = _profileOverviewDataStateItems
    var profileOverviewItems: List<ProfileOverviewItem> = emptyList()

    private var overviewUserInfoWrapper: OverviewUserInfoWrapper? = null

    init {

    }

    fun handleGetUserInfoOnDone(user: User) {
        val followingStatusSingle = mGetFollowingStatusUseCase
                .execute(CheckFollowingStatusUseCase.Param(loginId))

        val contributeInputStreamSingle = mGetContributeInputStream
                .execute(GetContributeInputStream.Param(loginId))

        Single.zip(
                followingStatusSingle,
                contributeInputStreamSingle,
                BiFunction<Boolean, InputStream, OverviewUserInfoWrapper> { hasFollowing, inputStream ->
                    OverviewUserInfoWrapper(
                            isCurrentUser = currentLoginId == loginId,
                            user = user,
                            hasFollowing = hasFollowing,
                            contributeInputStream = inputStream
                    )
                })
                .flatMap {
                    overviewUserInfoWrapper = it
                    flatMapToProfileOverviewItem(it)
                }.doOnSuccess {
                    profileOverviewItems = it
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    _profileOverviewDataStateItems.value = DataState.Loading(true)
                }
                .doFinally {
                    _profileOverviewDataStateItems.value = DataState.Loading(false)
                }
                .subscribe({
                    _profileOverviewDataStateItems.value = DataState.Success(it)
                }, {
                    handleCommonError(it)
                })
                .addToDisposables()
    }

    private fun flatMapToProfileOverviewItem(overviewUserWrapper: OverviewUserInfoWrapper): Single<List<ProfileOverviewItem>> {
        val user = overviewUserWrapper.user
        return Single.fromCallable {
            // header
            val header = listOf(
                    ProfileOverviewHeaderInfoItem(
                            avatarUrl = user.avatarUrl,
                            fullName = user.name,
                            userName = user.loginId,
                            tvDescription = user.bio
                    )
            )

            // follow action
            val followAction = listOf(
                    ProfileOverviewFollowItem(
                            following = user.following,
                            followers = user.followers,
                            followStatusButton = when {
                                overviewUserWrapper.isCurrentUser -> HIDE
                                overviewUserWrapper.hasFollowing -> UN_FOLLOW
                                else -> FOLLOW
                            }
                    )
            )

            // extra info
            val company = user.company.let {
                if (it.isNotEmpty()) {
                    listOf(
                            ProfileOverviewExtraInfoItem(
                                    resId = R.drawable.ic_organization,
                                    content = it
                            ),
                            ProfileOverviewDividerItem()
                    )
                } else {
                    emptyList()
                }
            }

            val location = user.location.let {
                if (it.isNotEmpty()) {
                    listOf(
                            ProfileOverviewExtraInfoItem(
                                    resId = R.drawable.ic_location,
                                    content = it
                            ),
                            ProfileOverviewDividerItem()
                    )
                } else {
                    emptyList()
                }
            }

            val email = user.email.let {
                if (it.isNotEmpty()) {
                    listOf(
                            ProfileOverviewExtraInfoItem(
                                    resId = R.drawable.ic_email,
                                    content = it
                            ),
                            ProfileOverviewDividerItem()
                    )
                } else {
                    emptyList()
                }
            }

            val blog = user.blog.let {
                if (it.isNotEmpty()) {
                    listOf(
                            ProfileOverviewExtraInfoItem(
                                    resId = R.drawable.ic_link,
                                    content = it
                            ),
                            ProfileOverviewDividerItem()
                    )
                } else {
                    emptyList()
                }
            }

            val joinedTime = user.joinedTime.let {
                if (it != null) {
                    listOf(
                            ProfileOverviewExtraInfoItem(
                                    resId = R.drawable.ic_time,
                                    content = DateTimeUtils.convertDateToString(
                                            it,
                                            API_DATE_TIME_FORMAT_WITHOUT_HOUR
                                    )
                            ), ProfileOverviewDividerItem()
                    )
                } else {
                    emptyList()
                }
            }

            // contribute
            val contribute =
                    listOf(ProfileOverviewContributeInfoItem(overviewUserWrapper.contributeInputStream))

            val extraInfo = company +
                    location +
                    email +
                    blog +
                    joinedTime +
                    contribute

            // the result
            header + followAction + extraInfo
        }
    }

    fun changeTargetFollowStatusOnDone(targetFollowStatusType: FollowStatusType) {
        val a = profileOverviewItems.toMutableList()


        a.filterIsInstance(ProfileOverviewFollowItem::class.java)
                .firstOrNull()?.followStatusButton =
                if (targetFollowStatusType == FollowStatusType.FOLLOW) {
                    FOLLOW
                } else {
                    UN_FOLLOW
                }

        _profileOverviewDataStateItems.value = DataState.Success(a)
    }

    fun getFollowType() {

    }

    data class OverviewUserInfoWrapper(
            val isCurrentUser: Boolean,
            val hasFollowing: Boolean,
            val user: User,
            val contributeInputStream: InputStream
    )
}