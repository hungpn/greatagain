package com.hung.greatagain.ui.feature.profile

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import com.hung.domain.model.user.FollowType
import com.hung.greatagain.R
import com.hung.greatagain.base.ViewModelActivity
import com.hung.greatagain.databinding.ActivityProfileBinding
import com.hung.greatagain.ui.feature.main.adapter.FragmentModelWrapper
import com.hung.greatagain.ui.feature.main.adapter.SGFragmentPagerAdapter
import com.hung.greatagain.ui.feature.profile.tab.follow.ProfileFollowFragment
import com.hung.greatagain.ui.feature.profile.tab.overview.ProfileOverviewFragment
import com.hung.greatagain.util.BundleKeyConst

class ProfileActivity : ViewModelActivity<ProfileViewModel, ActivityProfileBinding>() {

    companion object {
        fun start(context: Context, loginId: String) {
            val intent = Intent(context, ProfileActivity::class.java).apply {
                putExtra(BundleKeyConst.EXTRA_1, loginId)
            }
            context.startActivity(intent)
        }
    }

    private lateinit var mFragmentPagerAdapter: SGFragmentPagerAdapter

    override val mLayoutId = R.layout.activity_profile

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val loginId = intent.getStringExtra(BundleKeyConst.EXTRA_1)
        mViewModel.loginId = loginId
        mViewModel.getUserInfo()
        initView()
        initViewModel()
    }

    private fun initView() {
        initViewPager(mViewModel.loginId)
    }

    private fun initViewModel() {
        mViewModel.apply {

        }
    }

    private fun initViewPager(loginId: String) {
        val baseFragmentList = listOf(
                FragmentModelWrapper(
                        fragment = ProfileOverviewFragment.newInstance(loginId = loginId),
                        title = "OVERVIEW"
                ),
                FragmentModelWrapper(
                        fragment = ProfileFollowFragment.newInstance(
                                loginId = loginId,
                                followType = FollowType.FOLLOWER
                        ),
                        title = "FOLLOWERS"
                ),
                FragmentModelWrapper(
                        fragment = ProfileFollowFragment.newInstance(
                                loginId = loginId,
                                followType = FollowType.FOLLOWING
                        ),
                        title = "FOLLOWING"
                )

                /*
                FragmentModelWrapper(fragment = ProfileOverviewFragment.newInstance(loginId = loginId), title = "REPOSITORIES"),
                FragmentModelWrapper(fragment = ProfileOverviewFragment.newInstance(loginId = loginId), title = "FOLLOWERS"),
                FragmentModelWrapper(fragment = ProfileOverviewFragment.newInstance(loginId = loginId), title = "FOLLOWING")*/
        )

        mFragmentPagerAdapter = SGFragmentPagerAdapter(supportFragmentManager, baseFragmentList)

        mViewBinding.apply {
            viewPager.apply {
                offscreenPageLimit = 4
                adapter = mFragmentPagerAdapter
            }
            ilTabs.tabs.apply {
                tabGravity = TabLayout.GRAVITY_FILL
                tabMode = TabLayout.MODE_SCROLLABLE
                setupWithViewPager(viewPager)
            }
        }
    }
}