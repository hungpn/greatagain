package com.hung.greatagain.ui.feature.feed.adapter.loadmore

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.hung.greatagain.R
import com.hung.greatagain.base.adapter.ItemComparator
import com.hung.greatagain.base.adapter.LoadMoreAdapter
import com.hung.greatagain.databinding.ItemNewFeedActionBinding
import com.hung.greatagain.ui.feature.feed.adapter.holder.*
import com.hung.greatagain.ui.feature.feed.adapter.loadmore.holder.BaseFeedViewHolder
import com.hung.greatagain.ui.feature.feed.adapter.loadmore.holder.FeedBindingDelegate
import com.hung.greatagain.ui.feature.feed.adapter.loadmore.item.*
import com.hung.greatagain.ui.feature.feed.adapter.loadmore.item.BaseFeedItem.Companion.TYPE_ITEM_FEEDS_CREATED
import com.hung.greatagain.ui.feature.feed.adapter.loadmore.item.BaseFeedItem.Companion.TYPE_ITEM_FEEDS_FORKED
import com.hung.greatagain.ui.feature.feed.adapter.loadmore.item.BaseFeedItem.Companion.TYPE_ITEM_FEEDS_PUBLIC
import com.hung.greatagain.ui.feature.feed.adapter.loadmore.item.BaseFeedItem.Companion.TYPE_ITEM_FEEDS_PUSHED
import com.hung.greatagain.ui.feature.feed.adapter.loadmore.item.BaseFeedItem.Companion.TYPE_ITEM_FEEDS_WATCH
import java.util.concurrent.Executor

class LoadMoreFeedAdapter(
        onLoadMore: () -> Unit = {},
        onRetry: () -> Unit = {},
        onItemClickListener: OnItemClickListener<BaseFeedItem>? = null,
        itemComparator: ItemComparator<BaseFeedItem> = object : ItemComparator<BaseFeedItem> {
            override fun areItemsTheSame(oldItem: BaseFeedItem, newItem: BaseFeedItem): Boolean {
                return oldItem.feed.id == newItem.feed.id
            }

            override fun areContentsTheSame(oldItem: BaseFeedItem, newItem: BaseFeedItem): Boolean {
                return oldItem.feed == newItem.feed
            }

        }
) : LoadMoreAdapter<BaseFeedItem>(
        onItemClickListener = onItemClickListener,
        mOnLoadMore = onLoadMore,
        onRetry = onRetry,
        comparator = itemComparator
) {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseHolder<*, BaseFeedItem>? {
        val binding = DataBindingUtil.inflate<ItemNewFeedActionBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_new_feed_action,
                parent,
                false
        )

        val delegate = when (viewType) {
            TYPE_ITEM_FEEDS_CREATED -> {
                FeedBindingDelegate<CreatedItem>()
            }
            TYPE_ITEM_FEEDS_FORKED -> {
                FeedBindingDelegate<ForkedItem>()
            }
            TYPE_ITEM_FEEDS_PUBLIC -> {
                FeedBindingDelegate<PublicItem>()
            }
            TYPE_ITEM_FEEDS_PUSHED -> {
                FeedBindingDelegate<PushedItem>()
            }
            TYPE_ITEM_FEEDS_WATCH -> {
                FeedBindingDelegate<StarredItem>()
            }
            else -> {
                FeedBindingDelegate<NoDefinedItem>()
            }
        }
        return BaseFeedViewHolder(delegate, binding)
    }

    override fun getItemTypeAtPosition(position: Int): Int {
        if (itemCount == 0 || position < 0 || position >= itemCount)
            return -1
        return itemList[position].viewType
    }
}