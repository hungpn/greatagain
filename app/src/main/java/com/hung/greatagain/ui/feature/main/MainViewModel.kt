package com.hung.greatagain.ui.feature.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.hung.domain.model.user.User
import com.hung.domain.usecase.GetCurrentUserUseCase
import com.hung.domain.usecase.user.GetCurrentLoginIdUseCase
import com.hung.greatagain.base.BaseViewModel
import com.hung.greatagain.base.model.DataState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainViewModel @Inject constructor(
        private val mGetCurrentUserUseCase: GetCurrentUserUseCase,
        private val mGetCurrentLoginIdUseCase: GetCurrentLoginIdUseCase
) : BaseViewModel() {

    val currentLoginId: String
        get() = mGetCurrentLoginIdUseCase.execute()

    private val _userStateLiveData = MutableLiveData<DataState<User>>()
    val userStateLiveData: LiveData<DataState<User>>
        get() = _userStateLiveData
    var user: User? = null

    fun handleSetUserInfo(user: User?) {
        if (user == null) {
            mGetCurrentUserUseCase.execute(
                    GetCurrentUserUseCase.Param(false)
            )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        this.user = it
                        _userStateLiveData.value = DataState.Success(it)
                    }, {
                        Log.d("hung123", "GetCurrentUserUseCase ex=$it")
                    }).addToDisposables()
        } else {
            this.user = user
            _userStateLiveData.value = DataState.Success(user)
        }
    }
}
