package com.hung.greatagain.ui.feature.profile

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.hung.domain.model.user.FollowStatusType
import com.hung.domain.model.user.User
import com.hung.domain.usecase.GetUserByLoginIdUseCase
import com.hung.domain.usecase.user.DoFollowUserUseCase
import com.hung.greatagain.base.BaseViewModel
import com.hung.share.EMPTY
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProfileViewModel @Inject constructor(
        private val mGetUserByLoginIdUseCase: GetUserByLoginIdUseCase,
        private val mDoFollowUserUseCase: DoFollowUserUseCase
) : BaseViewModel() {

    var loginId: String = EMPTY

    private val _userLiveData = MutableLiveData<User>()
    val userLiveData: LiveData<User>
        get() = _userLiveData

    fun getUserInfo() {
        mGetUserByLoginIdUseCase.execute(GetUserByLoginIdUseCase.Param(loginId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _userLiveData.value = it
                }, {
                    handleCommonError(it)
                })
                .addToDisposables()
    }

    private val _targetFollowStatusTypeLiveData = MutableLiveData<FollowStatusType>()
    val targetFollowStatusLiveData: LiveData<FollowStatusType>
        get() = _targetFollowStatusTypeLiveData

    fun doFollowChangeStatus(targetFollowStatusType: FollowStatusType) {
        mDoFollowUserUseCase.execute(DoFollowUserUseCase.Param(
                username = loginId,
                type = targetFollowStatusType
        ))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it) {
                        _targetFollowStatusTypeLiveData.value = targetFollowStatusType
                    }
                }, {
                    Log.e("hung123", "doFollowChangeStatus it=$it")
                })
                .addToDisposables()
    }
}