package com.hung.greatagain.ui.feature.issue

import androidx.lifecycle.ViewModel
import com.hung.greatagain.di.scope.PerFragment
import com.hung.greatagain.di.ViewModelKey
import com.hung.greatagain.di.scope.PerChildFragment
import com.hung.greatagain.ui.feature.issue.pager.IssueItemFragment
import com.hung.greatagain.ui.feature.issue.pager.IssueItemModule
import com.hung.greatagain.ui.feature.issue.pager.IssueItemViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class IssueModule {

    @Binds
    @IntoMap
    @ViewModelKey(IssueViewModel::class)
    abstract fun provideIssueViewModel(issueViewModel: IssueViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(IssueItemViewModel::class)
    abstract fun provideIssueItemViewModel(issueItemViewModel: IssueItemViewModel): ViewModel

    @PerFragment
    @ContributesAndroidInjector(modules = [IssueFragmentModule::class])
    internal abstract fun contributeIssueFragment(): IssueFragment
}

@Module
internal abstract class IssueFragmentModule {

    @PerChildFragment
    @ContributesAndroidInjector(modules = [IssueItemModule::class])
    internal abstract fun contributeIssueItemFragment(): IssueItemFragment
}