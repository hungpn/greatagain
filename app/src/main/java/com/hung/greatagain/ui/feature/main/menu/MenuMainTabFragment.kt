package com.hung.greatagain.ui.feature.main.menu

import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.google.android.material.navigation.NavigationView
import com.hung.greatagain.R
import com.hung.greatagain.base.ViewModelFragment
import com.hung.greatagain.databinding.FragmentMenuMainTabBinding

class MenuMainTabFragment
    : ViewModelFragment<MenuMainTabViewModel, FragmentMenuMainTabBinding>(),
        NavigationView.OnNavigationItemSelectedListener {

    companion object {

        const val TITLE = "Menu"

        fun newInstance() = MenuMainTabFragment().apply {

        }
    }

    override val mLayoutId = R.layout.fragment_menu_main_tab

    private lateinit var mMenuMainTabFragmentListener: MenuMainTabFragmentListener

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MenuMainTabFragmentListener) {
            mMenuMainTabFragmentListener = context
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewBinding.apply {
            mainNav.setNavigationItemSelectedListener(this@MenuMainTabFragment)
        }
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.item_profile -> {
                mMenuMainTabFragmentListener.onProfileClick()
            }
        }
        return true
    }
}

interface MenuMainTabFragmentListener {

    fun onProfileClick()
}