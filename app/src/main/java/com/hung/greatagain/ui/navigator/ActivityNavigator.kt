package com.hung.greatagain.ui.navigator

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager


interface ActivityNavigator {

    fun finish()

    fun startActivity(intent: Intent)

    fun startActivity(clazz: Class<out Activity>)

    fun startActivity(clazz: Class<out Activity>, bundle: Bundle)

    fun startActivityForResult(clazz: Class<out Activity>, requestCode: Int)

    fun replaceFragment(@IdRes idResource: Int, fragment: Fragment)

    fun replaceFragment(@IdRes idResource: Int, fragment: Fragment, bundle: Bundle)

    fun <T : Fragment?> findFragmentByTag(tag: String): T?

    fun <T : Fragment?> findFragmentById(@IdRes idResource: Int): T?

    fun getCurrentFragment(fragmentManager: FragmentManager): Fragment?
}
