package com.hung.greatagain.ui.feature.main.menu

import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.google.android.material.navigation.NavigationView
import com.hung.greatagain.R
import com.hung.greatagain.base.ViewModelFragment
import com.hung.greatagain.databinding.FragmentMenuAccountTabBinding

class MenuAccountTabFragment
    : ViewModelFragment<MenuMainTabViewModel, FragmentMenuAccountTabBinding>(),
        NavigationView.OnNavigationItemSelectedListener {

    companion object {

        const val TITLE = "Profile"

        fun newInstance() = MenuAccountTabFragment().apply {

        }
    }

    override val mLayoutId = R.layout.fragment_menu_account_tab

    private lateinit var mMenuAccountTabFragmentListener: MenuAccountTabFragmentListener

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MenuAccountTabFragmentListener) {
            mMenuAccountTabFragmentListener = context
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewBinding.apply {
            mainNav.setNavigationItemSelectedListener(this@MenuAccountTabFragment)
        }
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.item_logout -> {
                mMenuAccountTabFragmentListener.onLogoutClick()
            }
        }
        return true
    }
}

interface MenuAccountTabFragmentListener {

    fun onLogoutClick()
}