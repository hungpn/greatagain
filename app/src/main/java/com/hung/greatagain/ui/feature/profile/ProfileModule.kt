package com.hung.greatagain.ui.feature.profile

import androidx.lifecycle.ViewModel
import com.hung.greatagain.di.module.ActivityModule
import com.hung.greatagain.ui.navigator.ActivityNavigator
import com.hung.greatagain.ui.navigator.ActivityNavigatorImpl
import com.hung.greatagain.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Inject

@Module
class ProfileModule @Inject constructor(private val mActivity: ProfileActivity) {

}

@Module
abstract class ProfileViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    abstract fun bindProfileViewModel(profileViewModel: ProfileViewModel): ViewModel
}

@Module
class ProfileActivityModule : ActivityModule<ProfileActivity>() {

    @Provides
    fun provideNavigator(activity: ProfileActivity): ActivityNavigator = ActivityNavigatorImpl(activity)
}