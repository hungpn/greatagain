package com.hung.greatagain.ui.feature.main.menu

import androidx.lifecycle.ViewModel
import com.hung.greatagain.di.ViewModelKey
import com.hung.greatagain.di.scope.PerFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class MenuMainTabModule {

    @Binds
    @IntoMap
    @ViewModelKey(MenuMainTabViewModel::class)
    abstract fun provideMenuMainTabViewModel(menuMainTabViewModel: MenuMainTabViewModel): ViewModel

    @PerFragment
    @ContributesAndroidInjector
    internal abstract fun contributeMenuMainTabModule(): MenuMainTabFragment
}