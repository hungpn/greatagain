package com.hung.greatagain.ui.feature.login

import androidx.lifecycle.ViewModel
import com.hung.greatagain.di.module.ActivityModule
import com.hung.greatagain.ui.navigator.ActivityNavigator
import com.hung.greatagain.ui.navigator.ActivityNavigatorImpl
import com.hung.greatagain.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class LoginModule {

}

@Module
class LoginActivityModule : ActivityModule<LoginActivity>() {

    @Provides
    fun provideNavigator(activity: LoginActivity): ActivityNavigator = ActivityNavigatorImpl(activity)
}

@Module
internal abstract class LoginViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(loginViewModel: LoginViewModel): ViewModel
}