package com.hung.greatagain.ui.feature.splash

import androidx.lifecycle.ViewModel
import com.hung.greatagain.di.module.ActivityModule
import com.hung.greatagain.ui.navigator.ActivityNavigator
import com.hung.greatagain.ui.navigator.ActivityNavigatorImpl
import com.hung.greatagain.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Inject

@Module
class SplashModule @Inject constructor(private val mActivity: SplashActivity) {

}

@Module
internal abstract class SplashViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(splashViewModel: SplashViewModel): ViewModel
}

@Module
class SplashActivityModule : ActivityModule<SplashActivity>() {

    @Provides
    fun provideNavigator(activity: SplashActivity): ActivityNavigator = ActivityNavigatorImpl(activity)
}