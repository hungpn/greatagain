package com.hung.greatagain.ui.feature.pullrequest

import androidx.lifecycle.ViewModel
import com.hung.greatagain.di.scope.PerFragment
import com.hung.greatagain.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class PullRequestModule {

    @Binds
    @IntoMap
    @ViewModelKey(PullRequestViewModel::class)
    abstract fun providePullRequestViewModel(pullRequestViewModel: PullRequestViewModel): ViewModel

    @PerFragment
    @ContributesAndroidInjector
    internal abstract fun contributePullRequestFragment(): PullRequestFragment
}