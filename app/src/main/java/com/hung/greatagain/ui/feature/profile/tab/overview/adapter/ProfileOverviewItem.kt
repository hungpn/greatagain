package com.hung.greatagain.ui.feature.profile.tab.overview.adapter

import androidx.annotation.DrawableRes
import com.hung.greatagain.base.adapter.DiffObject
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.ProfileOverviewFollowItem.FollowStatusButton.*
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.ProfileOverviewItem.Companion.TYPE_CONTRIBUTE
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.ProfileOverviewItem.Companion.TYPE_DIVIDER
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.ProfileOverviewItem.Companion.TYPE_EXTRA_INFO
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.ProfileOverviewItem.Companion.TYPE_FOLLOW
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.ProfileOverviewItem.Companion.TYPE_HEADER_INFO
import java.io.InputStream

interface ProfileOverviewItem : DiffObject {

    companion object {
        const val TYPE_HEADER_INFO = 0
        const val TYPE_FOLLOW = 1
        const val TYPE_EXTRA_INFO = 2
        const val TYPE_CONTRIBUTE = 3
        const val TYPE_DIVIDER = 4
    }

    var type: Int
}

data class ProfileOverviewExtraInfoItem(
        @DrawableRes val resId: Int,
        val content: String
) : ProfileOverviewItem {

    override var type = TYPE_EXTRA_INFO

    override var id = content
}

data class ProfileOverviewHeaderInfoItem(
        val avatarUrl: String,
        val fullName: String,
        val userName: String,
        val tvDescription: String
) : ProfileOverviewItem {

    override var type = TYPE_HEADER_INFO

    override var id = userName
}

data class ProfileOverviewFollowItem(
        val following: Long,
        val followers: Long,
        var followStatusButton: FollowStatusButton = FOLLOW
) : ProfileOverviewItem {

    override var type = TYPE_FOLLOW

    override var id = "$following $followers $followStatusButton"

    enum class FollowStatusButton {
        HIDE,
        FOLLOW,
        UN_FOLLOW
    }
}

data class ProfileOverviewContributeInfoItem(
        val contributeInputStream: InputStream
) : ProfileOverviewItem {

    override var type = TYPE_CONTRIBUTE

    override var id = ProfileOverviewItem.javaClass.canonicalName!!
}

data class ProfileOverviewDividerItem(
        val ignored: Any = Any()
) : ProfileOverviewItem {

    override var type = TYPE_DIVIDER

    override var id = ProfileOverviewItem.javaClass.canonicalName!!
}