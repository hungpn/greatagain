package com.hung.greatagain.ui.feature.issue.pager.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.hung.domain.model.issue.Issue
import com.hung.greatagain.R
import com.hung.greatagain.base.adapter.ItemComparator
import com.hung.greatagain.base.adapter.LoadMoreAdapter
import com.hung.greatagain.databinding.ItemIssueBinding
import com.hung.greatagain.ui.feature.issue.pager.adapter.holder.IssueItemViewHolder

class IssueItemAdapter(
        onLoadMore: () -> Unit,
        onRetry: () -> Unit,
        onItemClickListener: OnItemClickListener<Issue>? = null,
        itemComparator: ItemComparator<Issue> = object : ItemComparator<Issue> {
            override fun areItemsTheSame(oldItem: Issue, newItem: Issue): Boolean {
                return oldItem.htmlUrl == newItem.htmlUrl
            }

            override fun areContentsTheSame(oldItem: Issue, newItem: Issue): Boolean {
                return oldItem == newItem
            }
        }
) : LoadMoreAdapter<Issue>(
        onItemClickListener = onItemClickListener,
        mOnLoadMore = onLoadMore,
        onRetry = onRetry,
        comparator = itemComparator
) {

    companion object {
        const val ISSUE_TYPE = 0
    }

    override fun getItemTypeAtPosition(position: Int): Int {
        if (itemCount == 0 || position < 0 || position >= itemCount)
            return -1
        return ISSUE_TYPE
    }

    override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseHolder<*, Issue>? {
        val binding = DataBindingUtil.inflate<ItemIssueBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_issue,
                parent,
                false
        )
        return IssueItemViewHolder(binding)
    }
}