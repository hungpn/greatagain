package com.hung.greatagain.ui.feature.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import com.hung.greatagain.R
import com.hung.greatagain.base.ViewModelActivity
import com.hung.greatagain.base.model.DataState.*
import com.hung.greatagain.databinding.ActivityLoginBinding
import com.hung.greatagain.ui.feature.login.message.MessageLoginBottomSheetFragment
import com.hung.greatagain.ui.feature.main.MainActivity
import com.hung.greatagain.ui.navigator.ActivityNavigator
import retrofit2.HttpException
import javax.inject.Inject


class LoginActivity : ViewModelActivity<LoginViewModel, ActivityLoginBinding>() {

    companion object {
        fun startLoginActivity(context: Context) {
            val intent = Intent(context, LoginActivity::class.java)
            context.startActivity(intent)
        }
    }

    @Inject
    lateinit var activityNavigator: ActivityNavigator

    override val mLayoutId = R.layout.activity_login

    private lateinit var mBottomSheetFragment: MessageLoginBottomSheetFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initViewModel()
    }

    private fun initView() {
        mViewBinding.apply {
            btnLogin.setOnClickListener {
                mViewModel.doBasicLogin(
                        edtUsername.text.toString(),
                        edtPassword.text.toString()
                )
            }

            ivCloseMsg.setOnClickListener {
                showIncorrectPassword = false
            }
        }

        mBottomSheetFragment = MessageLoginBottomSheetFragment()
        mBottomSheetFragment.show(supportFragmentManager, "MessageLoginBottomSheetFragment")
    }

    private fun initViewModel() {
        mViewModel.apply {
            authUserWrapper.observe(this@LoginActivity, Observer { loginState ->
                when (loginState) {
                    is Loading -> {
                        mViewBinding.apply {
                            if (loginState.isLoading) {
                                showIncorrectPassword = false
                                btnLogin.setText(R.string.signing_in_in_loading)
                                btnLogin.isEnabled = false
                                btnLogin.alpha = 0.5f
                            } else {
                                btnLogin.setText(R.string.sign_in)
                                btnLogin.isEnabled = true
                                btnLogin.alpha = 1f
                            }
                        }
                    }

                    is Success -> {
                        Toast.makeText(this@LoginActivity, "Success", Toast.LENGTH_SHORT).show()
                        MainActivity.startMainActivity(this@LoginActivity, loginState.data.user)
                        //startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                        finish()
                    }

                    is Failure -> {
                        val throwable = loginState.throwable
                        if (throwable is HttpException && throwable.code() == 401) {
                            mViewBinding.showIncorrectPassword = true
                        } else {
                            showErrorDialog(throwable.message)
                        }
                    }
                }
            })
        }
    }
}