package com.hung.greatagain.ui.feature.issue

import android.os.Bundle
import android.view.View
import com.google.android.material.tabs.TabLayout
import com.hung.greatagain.R
import com.hung.greatagain.base.ViewModelFragment
import com.hung.greatagain.databinding.FragmentIssueBinding
import com.hung.greatagain.ui.feature.issue.model.IssueType
import com.hung.greatagain.ui.feature.issue.pager.IssueItemFragment
import com.hung.greatagain.ui.feature.issue.pager.adapter.IssuePagerAdapter
import com.hung.greatagain.util.BundleKeyConst
import com.hung.share.EMPTY

class IssueFragment : ViewModelFragment<IssueViewModel, FragmentIssueBinding>() {

    companion object {

        fun newInstance(loginId: String) = IssueFragment().apply {
            arguments = Bundle().apply {
                putString(BundleKeyConst.EXTRA_1, loginId)
            }
        }
    }

    private lateinit var mPagerAdapter: IssuePagerAdapter

    override val mLayoutId = R.layout.fragment_issue

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        initView()
    }

    private fun initView() {
        mPagerAdapter = IssuePagerAdapter(childFragmentManager, initListPagerFragment())

        mViewBinding.apply {
            viewPager.apply {
                adapter = mPagerAdapter
                offscreenPageLimit = 1
            }

            tabs.apply {
                tabGravity = TabLayout.GRAVITY_CENTER
                tabMode = TabLayout.MODE_SCROLLABLE
                setupWithViewPager(viewPager)
            }
        }
    }

    private fun initViewModel() {
        mViewModel.apply {
            loginId = arguments?.getString(BundleKeyConst.EXTRA_1, EMPTY) ?: EMPTY
        }
    }

    private fun initListPagerFragment(): List<IssueItemFragment> {
        val list = ArrayList<IssueItemFragment>()
        list.add(IssueItemFragment.newInstance(IssueType.MENTIONED, mViewModel.loginId))
        list.add(IssueItemFragment.newInstance(IssueType.CREATED, mViewModel.loginId))
        list.add(IssueItemFragment.newInstance(IssueType.ASSIGNED, mViewModel.loginId))
        return list
    }
}