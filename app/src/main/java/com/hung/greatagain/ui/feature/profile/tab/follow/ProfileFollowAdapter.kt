package com.hung.greatagain.ui.feature.profile.tab.follow

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.hung.domain.model.user.User
import com.hung.greatagain.R
import com.hung.greatagain.base.adapter.ItemComparator
import com.hung.greatagain.base.adapter.LoadMoreAdapter
import com.hung.greatagain.databinding.ItemProfileFollowBinding

class ProfileFollowAdapter(
        onLoadMore: () -> Unit,
        onRetry: () -> Unit,
        onItemClickListener: OnItemClickListener<User>? = null,
        itemComparator: ItemComparator<User> = object : ItemComparator<User> {
            override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem == newItem
            }
        }
) : LoadMoreAdapter<User>(
        onItemClickListener = onItemClickListener,
        mOnLoadMore = onLoadMore,
        onRetry = onRetry,
        comparator = itemComparator
) {

    companion object {
        const val FOLLOW_TYPE = 0
    }

    override fun getItemTypeAtPosition(position: Int): Int {
        if (itemCount == 0 || position < 0 || position >= itemCount)
            return -1
        return FOLLOW_TYPE
    }

    override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseHolder<*, User>? {
        val binding = DataBindingUtil.inflate<ItemProfileFollowBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_profile_follow,
                parent,
                false
        )
        return ProfileFollowViewHolder(binding)
    }
}