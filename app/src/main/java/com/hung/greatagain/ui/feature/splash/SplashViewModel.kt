package com.hung.greatagain.ui.feature.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.hung.domain.exception.UserDoesNotLoginException
import com.hung.domain.usecase.CheckLoginUseCase
import com.hung.greatagain.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SplashViewModel @Inject constructor(
        private val mCheckLoginUseCase: CheckLoginUseCase
) : BaseViewModel() {

    private val _navigateToMainScreen = MutableLiveData<Boolean>()
    val navigateToMainScreen: LiveData<Boolean>
        get() = _navigateToMainScreen

    private val _navigateToLoginScreen = MutableLiveData<Boolean>()
    val navigateToLoginScreen: LiveData<Boolean>
        get() = _navigateToLoginScreen

    fun handleScreenNavigation() {
        mCheckLoginUseCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _navigateToMainScreen.postValue(true)
                }, {
                    when (it) {
                        is UserDoesNotLoginException -> {
                            _navigateToLoginScreen.postValue(true)
                        }
                        else -> {
                            postMessageToShowSimpleDialog()
                        }
                    }
                }).addToDisposables()
    }
}