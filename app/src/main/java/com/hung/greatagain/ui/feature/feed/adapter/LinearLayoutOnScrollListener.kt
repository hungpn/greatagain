package com.hung.greatagain.ui.feature.feed.adapter

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class LinearLayoutOnScrollListener(
        private var mVisibleThreshold: Int = 5,
        private val mOnLoadMore: () -> Unit = {}
) : RecyclerView.OnScrollListener() {

    private var mIsLoading = false

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (dy < 0) return

        val layoutManager = recyclerView.layoutManager
        val totalItemCount = layoutManager?.itemCount ?: 0

        if (mIsLoading || totalItemCount == 0) return

        val lastVisibleItemPosition = (layoutManager as? LinearLayoutManager)?.findLastVisibleItemPosition()
                ?: 0

        if (lastVisibleItemPosition + mVisibleThreshold > totalItemCount) {
            mIsLoading = true
            mOnLoadMore()
        }
    }

    fun setAsLoading() {
        mIsLoading = true
    }

    fun reset() {
        mIsLoading = false
    }
}