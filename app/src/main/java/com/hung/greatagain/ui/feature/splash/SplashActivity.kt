package com.hung.greatagain.ui.feature.splash

import android.os.Bundle
import androidx.lifecycle.Observer
import com.hung.greatagain.R
import com.hung.greatagain.base.ViewModelActivity
import com.hung.greatagain.databinding.ActivitySplashBinding
import com.hung.greatagain.ui.feature.login.LoginActivity
import com.hung.greatagain.ui.feature.main.MainActivity

class SplashActivity : ViewModelActivity<SplashViewModel, ActivitySplashBinding>() {

    override val mLayoutId = R.layout.activity_splash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        mViewModel.handleScreenNavigation()
    }

    private fun initViewModel() {
        mViewModel.navigateToLoginScreen.observe(this, Observer {
            LoginActivity.startLoginActivity(this)
            finish()
        })

        mViewModel.navigateToMainScreen.observe(this, Observer {
            MainActivity.startMainActivityClearTop(this)
            finish()
        })
    }
}