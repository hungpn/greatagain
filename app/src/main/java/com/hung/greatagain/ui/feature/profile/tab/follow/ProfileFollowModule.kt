package com.hung.greatagain.ui.feature.profile.tab.follow

import androidx.lifecycle.ViewModel
import com.hung.greatagain.di.ViewModelKey
import com.hung.greatagain.di.scope.PerFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class ProfileFollowModule {

    @Binds
    @IntoMap
    @ViewModelKey(ProfileFollowViewModel::class)
    abstract fun provideProfileFollowViewModel(profileFollowViewModel: ProfileFollowViewModel): ViewModel

    @PerFragment
    @ContributesAndroidInjector
    internal abstract fun contributeProfileFollowFragment(): ProfileFollowFragment
}