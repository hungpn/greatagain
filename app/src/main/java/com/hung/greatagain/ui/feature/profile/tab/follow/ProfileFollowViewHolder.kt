package com.hung.greatagain.ui.feature.profile.tab.follow

import com.hung.domain.model.user.User
import com.hung.greatagain.R
import com.hung.greatagain.base.adapter.BaseAdapter
import com.hung.greatagain.databinding.ItemProfileFollowBinding
import com.hung.greatagain.util.loadCircleCropImg

class ProfileFollowViewHolder(
        binding: ItemProfileFollowBinding
) : BaseAdapter.BaseHolder<ItemProfileFollowBinding, User>(binding) {

    override fun bindData(data: User) {
        mViewDataBinding.apply {
            fullName = data.loginId
            ivAvatar.loadCircleCropImg(data.avatarUrl, R.drawable.ic_github_icon)
        }
    }
}