package com.hung.greatagain.ui.feature.feed.adapter.holder

import com.hung.greatagain.databinding.ItemNewFeedActionBinding
import com.hung.greatagain.ui.feature.feed.adapter.item.CreatedItem

class CreatedViewHolder(
    binding: ItemNewFeedActionBinding
) : BaseFeedViewHolder<CreatedItem>(binding) {

    override fun onBinding(item: CreatedItem) {
        super.onBinding(item)

    }
}