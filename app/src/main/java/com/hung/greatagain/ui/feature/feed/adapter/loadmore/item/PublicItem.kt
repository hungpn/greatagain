package com.hung.greatagain.ui.feature.feed.adapter.loadmore.item

import com.hung.domain.model.feed.Feed
import com.hung.greatagain.R
import com.hung.greatagain.util.SpannableBuilder

class PublicItem(feed: Feed) : BaseFeedItem(feed) {

    override fun buildTitle(): SpannableBuilder {
        //TODO
        return SpannableBuilder.builder()
    }

    override fun buildDescription(): SpannableBuilder {
        //TODO
        return SpannableBuilder.builder()
    }

    override var timeIconRes = R.drawable.ic_github_icon;

    override var viewType = TYPE_ITEM_FEEDS_PUBLIC
}
