package com.hung.greatagain.ui.navigator

import android.app.Activity
import android.content.Intent

interface FragmentNavigator {

    fun startActivity(intent: Intent)

    fun startActivity(clazz: Class<out Activity>)
}
