package com.hung.greatagain.ui.feature.profile.tab.overview.adapter

import android.annotation.SuppressLint
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.hung.greatagain.base.adapter.DataBoundViewHolder
import com.hung.greatagain.base.adapter.ViewBindingListAdapter
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.ProfileOverviewItem.Companion.TYPE_CONTRIBUTE
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.ProfileOverviewItem.Companion.TYPE_DIVIDER
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.ProfileOverviewItem.Companion.TYPE_EXTRA_INFO
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.ProfileOverviewItem.Companion.TYPE_FOLLOW
import com.hung.greatagain.ui.feature.profile.tab.overview.adapter.ProfileOverviewItem.Companion.TYPE_HEADER_INFO

class ProfileOverviewAdapter(
        private val mOnFollowClick: (currentStatusButton: ProfileOverviewFollowItem.FollowStatusButton) -> Unit
) : ViewBindingListAdapter<ProfileOverviewItem, ViewDataBinding>(

        diffCallback = object : DiffUtil.ItemCallback<ProfileOverviewItem>() {

            override fun areItemsTheSame(
                    oldItem: ProfileOverviewItem,
                    newItem: ProfileOverviewItem
            ): Boolean {
                return oldItem.id == newItem.id
            }

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(
                    oldItem: ProfileOverviewItem,
                    newItem: ProfileOverviewItem
            ): Boolean {
                return oldItem == newItem
            }
        }
) {

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): DataBoundViewHolder<ProfileOverviewItem, ViewDataBinding> {
        return when (viewType) {
            TYPE_HEADER_INFO -> {
                ProfileOverviewHeaderInfoViewHolder(parent)
            }
            TYPE_FOLLOW -> {
                ProfileOverviewFollowViewHolder(parent, mOnFollowClick)
            }
            TYPE_EXTRA_INFO -> {
                ProfileOverviewExtraInfoViewHolder(parent)
            }
            TYPE_CONTRIBUTE -> {
                ProfileOverviewContributeViewHolder(parent)
            }
            TYPE_DIVIDER -> {
                ProfileOverviewDividerViewHolder(parent)
            }
            else -> {
                throw IllegalStateException("$viewType does not support - onCreateViewHolderFactory")
            }
        } as DataBoundViewHolder<ProfileOverviewItem, ViewDataBinding>
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).type
    }

    override fun onBindViewHolder(holder: DataBoundViewHolder<ProfileOverviewItem, ViewDataBinding>, position: Int) {
        super.onBindViewHolder(holder, position)
    }
}