package com.hung.greatagain.ui.feature.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.hung.domain.model.user.User
import com.hung.greatagain.R
import com.hung.greatagain.base.ViewModelActivity
import com.hung.greatagain.base.model.DataState.*
import com.hung.greatagain.databinding.ActivityMainBinding
import com.hung.greatagain.databinding.LayoutHeaderDrawerBinding
import com.hung.greatagain.ui.feature.feed.FeedFragment
import com.hung.greatagain.ui.feature.issue.IssueFragment
import com.hung.greatagain.ui.feature.main.adapter.FragmentModelWrapper
import com.hung.greatagain.ui.feature.main.adapter.SGFragmentPagerAdapter
import com.hung.greatagain.ui.feature.main.menu.MenuMainTabFragment
import com.hung.greatagain.ui.feature.main.menu.MenuMainTabFragmentListener
import com.hung.greatagain.ui.feature.main.menu.MenuAccountTabFragment
import com.hung.greatagain.ui.feature.main.menu.MenuAccountTabFragmentListener
import com.hung.greatagain.ui.feature.profile.ProfileActivity
import com.hung.greatagain.ui.feature.pullrequest.PullRequestFragment
import com.hung.greatagain.ui.navigator.ActivityNavigator
import com.hung.greatagain.ui.widget.BottomNavigationViewBehavior
import com.hung.greatagain.util.loadCircleCropImg
import javax.inject.Inject

class MainActivity : ViewModelActivity<MainViewModel, ActivityMainBinding>(),
        BottomNavigationView.OnNavigationItemSelectedListener,
        BottomNavigationView.OnNavigationItemReselectedListener,
        MenuMainTabFragmentListener,
        MenuAccountTabFragmentListener {

    companion object {
        const val KEY_USER_INFO = "KEY_USER_INFO"

        @JvmStatic
        fun startMainActivityClearTop(context: Context) {
            val intent = Intent(context, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            context.startActivity(intent)
        }

        fun startMainActivity(context: Context, user: User? = null) {
            val intent = Intent(context, MainActivity::class.java).apply {
                putExtra(KEY_USER_INFO, user)
            }
            context.startActivity(intent)
        }
    }

    private var mActionBar: ActionBar? = null

    private lateinit var mMainMenuPagerAdapter: SGFragmentPagerAdapter

    private lateinit var mHeaderMenuViewBinding: LayoutHeaderDrawerBinding

    override val mLayoutId = R.layout.activity_main

    @Inject
    lateinit var activityNavigator: ActivityNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initViewModel()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.appbar_search_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        //Log.d("hung123", "onNavigationItemSelected ${item.groupId}")
        val visibleFragment = activityNavigator.getCurrentFragment(supportFragmentManager)
        val containerId = mViewBinding.ilFragmentContent.container.id
        when (item.itemId) {
            R.id.bnv_feeds -> {
                activityNavigator.replaceFragment(
                        idResource = containerId,
                        fragment = FeedFragment.newInstance(mViewModel.currentLoginId)
                )
            }

            R.id.bnv_issues -> {
                activityNavigator.replaceFragment(
                        idResource = containerId,
                        fragment = IssueFragment.newInstance(mViewModel.currentLoginId)
                )
            }

            R.id.bnv_pull_request -> {
                activityNavigator.replaceFragment(
                        idResource = containerId,
                        fragment = PullRequestFragment.newInstance(mViewModel.currentLoginId)
                )
            }

        }
        return true
    }

    override fun onNavigationItemReselected(item: MenuItem) {
        Log.d("hung123", "onNavigationItemReselected ${item.groupId}")
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                mViewBinding.drawer.openDrawer(GravityCompat.START)
            }

            R.id.notifications -> {
                /*val intent = Intent(this, TestActivity::class.java)
                startActivity(intent)*/
            }

            R.id.search -> {

            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initView() {
        setupMainMenuNavigation()
        setupActionBar()
        setupBottomNavigation()
    }

    private fun initViewModel() {
        handleSetUserInfo()

        mViewModel.userStateLiveData.observe(this, Observer { userState ->
            when (userState) {
                is Loading -> {

                }
                is Success -> {
                    val user = userState.data
                    mHeaderMenuViewBinding.apply {
                        ivAvatar.loadCircleCropImg(user.avatarUrl, R.drawable.ic_github_icon)
                        tvFullName.text = user.name
                        tvLoginName.text = user.loginId
                        root.setOnClickListener {
                            onProfileClick()
                        }
                    }

                    mViewBinding.apply {

                    }
                }
                is Failure -> {

                }
            }
        })
    }

    private fun setupBottomNavigation() {
        mViewBinding.ilBottomNavigation.viewBottomNavigation.apply {
            setOnNavigationItemSelectedListener(this@MainActivity)
            setOnNavigationItemReselectedListener(this@MainActivity)
            selectedItemId = R.id.bnv_feeds

            val layoutParams = layoutParams as? CoordinatorLayout.LayoutParams
            layoutParams?.behavior = BottomNavigationViewBehavior()
        }
    }

    private fun setupActionBar() {
        setSupportActionBar(mViewBinding.ilAppBarMain.toolbar)
        mActionBar = supportActionBar
        mActionBar?.apply {
            setHomeAsUpIndicator(R.drawable.ic_menu)
            mActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        // Quick-Note: using android:parentActivityName".." to declare the parent activity,
        // and up button of child activity will back to parent in default
    }

    private fun handleSetUserInfo() {
        val user = intent.getSerializableExtra(KEY_USER_INFO) as? User
        mViewModel.handleSetUserInfo(user)
    }

    private fun setupMainMenuNavigation() {
        mViewBinding.apply {
            mHeaderMenuViewBinding = LayoutHeaderDrawerBinding.bind(ilMainMenu.navMainMenu.getHeaderView(0))

            mMainMenuPagerAdapter = SGFragmentPagerAdapter(
                    supportFragmentManager,
                    initMainMenuFragmentObjectWrapperList()
            )
            ilMainMenu.apply {
                vpMainMenu.apply {
                    adapter = mMainMenuPagerAdapter
                }
                tlMainMenu.setupWithViewPager(vpMainMenu, true)
            }
        }
    }

    private fun initMainMenuFragmentObjectWrapperList(): List<FragmentModelWrapper> {
        val array = ArrayList<FragmentModelWrapper>()
        val mainMenuFragmentModel =
                FragmentModelWrapper(MenuMainTabFragment.newInstance(), MenuMainTabFragment.TITLE)
        val profileMenuFragmentModel =
                FragmentModelWrapper(MenuAccountTabFragment.newInstance(), MenuAccountTabFragment.TITLE)
        array.add(profileMenuFragmentModel)
        array.add(mainMenuFragmentModel)
        return array
    }

    // Menu Main
    override fun onProfileClick() {
        mViewModel.user?.loginId?.let {
            ProfileActivity.start(this@MainActivity, it)
        }
    }

    // Menu Account
    override fun onLogoutClick() {
        mViewModel.user?.loginId?.let {
            ProfileActivity.start(this@MainActivity, it)
        }
    }
}