package com.hung.greatagain.ui.feature.feed.adapter.holder

import com.hung.greatagain.databinding.ItemNewFeedActionBinding
import com.hung.greatagain.ui.feature.feed.adapter.item.PublicItem

class PublicViewHolder(
    binding: ItemNewFeedActionBinding
) : BaseFeedViewHolder<PublicItem>(binding) {

    override fun onBinding(item: PublicItem) {
        super.onBinding(item)

    }
}