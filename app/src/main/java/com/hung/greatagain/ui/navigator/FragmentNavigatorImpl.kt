package com.hung.greatagain.ui.navigator


import android.app.Activity
import android.content.Context
import android.content.Intent

class FragmentNavigatorImpl(private val context: Context) : FragmentNavigator {

    override fun startActivity(intent: Intent) {
        context.startActivity(intent)
    }

    override fun startActivity(clazz: Class<out Activity>) {
        val intent = Intent(context, clazz)
        context.startActivity(intent)
    }
}
