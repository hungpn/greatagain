package com.hung.greatagain.ui.feature.profile.tab.follow

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hung.domain.model.user.FollowType
import com.hung.domain.model.user.User
import com.hung.greatagain.R
import com.hung.greatagain.base.ViewModelFragment
import com.hung.greatagain.base.adapter.BaseAdapter
import com.hung.greatagain.base.model.DataState.*
import com.hung.greatagain.base.model.LoadMoreListState
import com.hung.greatagain.databinding.FragmentProfileFollowBinding
import com.hung.greatagain.ui.feature.profile.ProfileActivity
import com.hung.greatagain.ui.feature.profile.ProfileViewModel
import com.hung.greatagain.util.BundleKeyConst
import com.hung.share.EMPTY
import com.hung.share.ext.negative

class ProfileFollowFragment :
        ViewModelFragment<ProfileFollowViewModel, FragmentProfileFollowBinding>() {

    companion object {
        fun newInstance(loginId: String, followType: FollowType) = ProfileFollowFragment().apply {
            arguments = Bundle().apply {
                putString(BundleKeyConst.EXTRA_1, loginId)
                putSerializable(BundleKeyConst.EXTRA_2, followType)
            }
        }
    }

    override val mLayoutId = R.layout.fragment_profile_follow

    private lateinit var mAdapter: ProfileFollowAdapter

    private lateinit var mSharedProfileViewModel: ProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mSharedProfileViewModel = activity?.run {
            ViewModelProviders.of(this).get(ProfileViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initViewModel()
    }

    private fun initView() {
        mAdapter = ProfileFollowAdapter(
                onLoadMore = {
                    mViewModel.getFollowViewModel()
                },
                onRetry = {
                    mViewModel.getFollowViewModel()
                },
                onItemClickListener = object : BaseAdapter.OnItemClickListener<User> {
                    override fun onItemClick(v: View, position: Int, data: User) {
                        ProfileActivity.start(requireContext(), data.loginId)
                    }
                }
        )

        mAdapter.setLoadMoreCondition {
            mViewModel.isEnd.negative()
        }
        mViewBinding.apply {
            rvFollow.setLoadMoreAdapter(mAdapter)
            rvFollow.layoutManager =
                    LinearLayoutManager(
                            requireContext(),
                            RecyclerView.VERTICAL,
                            false
                    )

            srlFollow.setOnRefreshListener {
                refreshFollowList()
            }
        }
    }

    private fun initViewModel() {
        mViewModel.apply {
            loginId = arguments?.getString(BundleKeyConst.EXTRA_1, EMPTY) ?: EMPTY
            followType = arguments?.getSerializable(BundleKeyConst.EXTRA_2) as FollowType
            getFollowViewModel(true)
            followLiveData.observe(
                    this@ProfileFollowFragment.viewLifecycleOwner,
                    Observer { usersState ->
                        when (usersState) {
                            is Loading -> {
                                if (usersState.isRefresh) {
                                    mViewBinding.srlFollow.isRefreshing = usersState.isLoading
                                }
                                mAdapter.setLoading(usersState.isLoading)
                            }
                            is Success -> {
                                usersState.apply {
                                    when {
                                        data.isNotEmpty() && !isComplete -> mAdapter.update(data)
                                        isComplete -> mAdapter.setLoadMoreState(LoadMoreListState.COMPLETE)
                                    }
                                }
                            }
                            is Failure -> {
                                if (mViewBinding.srlFollow.isRefreshing) {
                                    mViewBinding.srlFollow.isRefreshing = false
                                }
                                mAdapter.setLoadMoreState(LoadMoreListState.ERROR)
                            }
                        }
                    })
        }

        mSharedProfileViewModel.targetFollowStatusLiveData.observe(this@ProfileFollowFragment,
                Observer {
                    if (mViewModel.followType == FollowType.FOLLOWER) {
                        refreshFollowList()
                    }
                })
    }

    private fun refreshFollowList() {
        mAdapter.reset()
        mViewModel.getFollowViewModel(true)
    }
}