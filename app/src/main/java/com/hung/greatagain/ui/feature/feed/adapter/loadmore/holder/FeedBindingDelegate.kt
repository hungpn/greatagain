package com.hung.greatagain.ui.feature.feed.adapter.loadmore.holder

import android.view.View
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.hung.greatagain.databinding.ItemNewFeedActionBinding
import com.hung.greatagain.ui.feature.feed.adapter.loadmore.item.BaseFeedItem

class FeedBindingDelegate<T : BaseFeedItem> {

    fun onBinding(mBinding : ViewDataBinding, item: T){
        if (mBinding is ItemNewFeedActionBinding) {
            mBinding.apply {
                    Glide.with(mBinding.root.context)
                        .asDrawable()
                        .load(item.actorAvatar)
                        .into(imvAvatar)

                    tvTitle.text = item.buildTitle()
                    tvDescription.apply {
                        val a = item.buildDescription()
                        if (a.toString().isEmpty()) {
                            visibility = View.GONE
                        } else {
                            visibility = View.VISIBLE
                            text = item.buildDescription()
                        }
                    }

                    time = item.buildTime.toString()
                    timeIconRes = item.timeIconRes
            }
        }
    }

}