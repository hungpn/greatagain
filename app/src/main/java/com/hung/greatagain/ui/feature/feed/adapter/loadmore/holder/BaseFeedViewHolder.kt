package com.hung.greatagain.ui.feature.feed.adapter.loadmore.holder

import androidx.databinding.ViewDataBinding
import com.hung.greatagain.base.adapter.BaseAdapter
import com.hung.greatagain.ui.feature.feed.adapter.loadmore.item.*

class BaseFeedViewHolder(
    private val mFeedBindingDelegate: FeedBindingDelegate<*>,
    mBinding: ViewDataBinding
) : BaseAdapter.BaseHolder<ViewDataBinding, BaseFeedItem>(mBinding) {

    @Suppress("UNCHECKED_CAST")
    override fun bindData(data: BaseFeedItem) {
        when(data) {
            is CreatedItem -> {
                (mFeedBindingDelegate as FeedBindingDelegate<CreatedItem>).onBinding(mViewDataBinding, data)
            }
            is ForkedItem -> {
                (mFeedBindingDelegate as FeedBindingDelegate<ForkedItem>).onBinding(mViewDataBinding, data)
            }
            is PublicItem -> {
                (mFeedBindingDelegate as FeedBindingDelegate<PublicItem>).onBinding(mViewDataBinding, data)
            }
            is PushedItem -> {
                (mFeedBindingDelegate as FeedBindingDelegate<PushedItem>).onBinding(mViewDataBinding, data)
            }
            is StarredItem -> {
                (mFeedBindingDelegate as FeedBindingDelegate<StarredItem>).onBinding(mViewDataBinding, data)
            }
            else -> {
            }
        }
    }
}