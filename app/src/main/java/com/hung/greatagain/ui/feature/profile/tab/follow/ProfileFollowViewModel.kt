package com.hung.greatagain.ui.feature.profile.tab.follow

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.hung.domain.model.user.FollowType
import com.hung.domain.model.user.User
import com.hung.domain.usecase.GetFollowUserByTypeUserCase
import com.hung.greatagain.base.BaseViewModel
import com.hung.greatagain.base.model.DataState
import com.hung.share.EMPTY
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProfileFollowViewModel @Inject constructor(
        private val mGetFollowUserByTypeUserCase: GetFollowUserByTypeUserCase
) : BaseViewModel() {

    companion object {
        const val STARTING_PAGE = 1
    }

    var followType: FollowType = FollowType.FOLLOWER

    var loginId: String = EMPTY

    private val _followLiveData = MutableLiveData<DataState<List<User>>>()
    val followLiveData: LiveData<DataState<List<User>>>
        get() = _followLiveData

    private var mFollowUser = listOf<User>()
    private var mCurrentPage = STARTING_PAGE
    var isEnd = false

    fun getFollowViewModel(isRefresh: Boolean = false) {
        if (isRefresh) {
            mCurrentPage = STARTING_PAGE
            mFollowUser = emptyList()
            isEnd = false
        }
        mGetFollowUserByTypeUserCase.execute(GetFollowUserByTypeUserCase.Param(
                loginId, followType, mCurrentPage))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    _followLiveData.value = DataState.Loading(true, isRefresh)
                }
                .doFinally {
                    _followLiveData.value = DataState.Loading(false, isRefresh)
                }
                .subscribe({
                    if (it.isNotEmpty()) {
                        mCurrentPage++
                        mFollowUser = mFollowUser + it
                        _followLiveData.value = DataState.Success(mFollowUser)
                    } else {
                        _followLiveData.value = DataState.Success(mFollowUser, true)
                    }
                }, {
                    Log.d("hung123", "error=$it")
                })
                .addToDisposables()
    }
}