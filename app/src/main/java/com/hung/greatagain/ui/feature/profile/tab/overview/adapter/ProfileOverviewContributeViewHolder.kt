package com.hung.greatagain.ui.feature.profile.tab.overview.adapter

import android.graphics.drawable.PictureDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.caverock.androidsvg.SVG
import com.caverock.androidsvg.SVGParseException
import com.hung.greatagain.R
import com.hung.greatagain.base.adapter.DataBoundViewHolder
import com.hung.greatagain.databinding.ItemProfileContributeInfoBinding

class ProfileOverviewContributeViewHolder(
        parent: ViewGroup
) : DataBoundViewHolder<ProfileOverviewContributeInfoItem, ItemProfileContributeInfoBinding>(
        DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_profile_contribute_info,
                parent,
                false
        )
) {

    override fun onBind(item: ProfileOverviewContributeInfoItem) {
        binding.apply {
            //TODO: DkVu
            val inputStream = item.contributeInputStream
            try {
                val svg = SVG.getFromInputStream(inputStream)
                ivContributions.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
                val picture = PictureDrawable(svg.renderToPicture())
                Glide.with(context)
                        .asDrawable()
                        .load(picture)
                    .centerInside()
                        .into(ivContributions)
            } catch (e: SVGParseException) {
                Log.e("hung123", "getFromInputStream - e: ${e.message}")
            }
        }
    }
}