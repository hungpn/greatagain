package com.hung.greatagain.ui.feature.feed.adapter.holder

import com.hung.greatagain.databinding.ItemNewFeedActionBinding
import com.hung.greatagain.ui.feature.feed.adapter.item.PushedItem

class PushedViewHolder(
    binding: ItemNewFeedActionBinding
) : BaseFeedViewHolder<PushedItem>(binding) {

    override fun onBinding(item: PushedItem) {
        super.onBinding(item)

    }
}