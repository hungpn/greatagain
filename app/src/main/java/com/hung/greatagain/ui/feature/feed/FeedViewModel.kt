package com.hung.greatagain.ui.feature.feed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.hung.domain.model.feed.Feed
import com.hung.domain.model.feed.FeedType
import com.hung.domain.usecase.GetFeedsByUserNameUseCase
import com.hung.greatagain.base.BaseViewModel
import com.hung.greatagain.base.model.DataState
import com.hung.greatagain.ui.feature.feed.adapter.loadmore.item.*
import com.hung.share.EMPTY
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class FeedViewModel @Inject constructor(
        private val mGetFeedUseCase: GetFeedsByUserNameUseCase
) : BaseViewModel() {

    companion object {
        const val STARTING_PAGE = 1
    }

    var loginId: String = EMPTY

    private val _feedsState = MutableLiveData<DataState<List<BaseFeedItem>>>()
    val feedsState: LiveData<DataState<List<BaseFeedItem>>>
        get() = _feedsState

    private var mFeeds = listOf<BaseFeedItem>()
    private var mCurrentPage = STARTING_PAGE
    var isEnd = false

    fun getFeeds(isRefresh: Boolean = false) {
        if (isRefresh) {
            mCurrentPage = STARTING_PAGE
            mFeeds = emptyList()
            isEnd = false
        }

        mGetFeedUseCase.execute(
                GetFeedsByUserNameUseCase.Param(
                        // hard code for testing
                        userName = loginId, page = mCurrentPage
                )
        )
                .map {
                    convertFeedToFeedItem(it)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    _feedsState.value = DataState.Loading(true, isRefresh)
                }
                .doFinally {
                    _feedsState.value = DataState.Loading(false, isRefresh)
                }
                .subscribe({
                    if (it.isNotEmpty()) {
                        mCurrentPage++
                        mFeeds = mFeeds + it
                        _feedsState.value = DataState.Success(mFeeds)
                    } else {
                        _feedsState.value = DataState.Success(mFeeds, true)
                    }
                }, {
                    handleCommonError(it) {}
                    _feedsState.value = DataState.Failure(it)
                }).addToDisposables()
    }

    private fun convertFeedToFeedItem(feeds: List<Feed>): List<BaseFeedItem> {
        return feeds.map {
            return@map when (it.type) {
                FeedType.PUSH_EVENT -> PushedItem(it)
                FeedType.FORK_EVENT -> ForkedItem(it)
                FeedType.WATCH_EVENT -> StarredItem(it)
                FeedType.CREATE_EVENT -> CreatedItem(it)
                FeedType.PUBLIC_EVENT -> PushedItem(it)
                else -> NoDefinedItem(it) //TODO; need to support the orders FeedType
            }
        }.filterNot {
            it is NoDefinedItem
        }
    }
}
