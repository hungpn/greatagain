package com.hung.greatagain.ui.feature.login.message

import android.os.Bundle
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.hung.greatagain.R
import kotlinx.android.synthetic.main.fragment_bottom_sheet_login.*

class MessageLoginBottomSheetFragment : BottomSheetDialogFragment() {

    companion object {
        val TAG: String = MessageLoginBottomSheetFragment::class.java.canonicalName!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bottom_sheet_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isCancelable = false
        tvOk.setOnClickListener {
            dismiss()
        }

        Linkify.addLinks(tvMessage, Linkify.WEB_URLS)
    }
}