package com.hung.greatagain.ui.feature.issue.model

enum class IssueType(val displayName: String) {
    CREATED("CREATED"),
    ASSIGNED("ASSIGNED"),
    MENTIONED("MENTIONED"),
    PARTICIPATED("PARTICIPATED"),
    UNKNOW("UNKNOW")
}
