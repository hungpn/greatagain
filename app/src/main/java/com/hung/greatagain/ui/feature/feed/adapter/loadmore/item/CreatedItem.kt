package com.hung.greatagain.ui.feature.feed.adapter.loadmore.item


import com.hung.domain.model.feed.Feed
import com.hung.greatagain.R
import com.hung.greatagain.util.SpannableBuilder

class CreatedItem(feed: Feed) : BaseFeedItem(feed) {

    override var viewType = TYPE_ITEM_FEEDS_CREATED

    override fun buildTitle(): SpannableBuilder {
        val spannableBuilder = SpannableBuilder.builder()
        spannableBuilder
                .append(actorName)
                .bold(" created")
                .append(" repository")
                .bold(" at ")
                .append(repoNamePushTo) //TODO: need to be reviewed

        return spannableBuilder
    }

    override fun buildDescription(): SpannableBuilder {
        //TODO
        return SpannableBuilder.builder()
    }

    override var timeIconRes = R.drawable.ic_document;
}
