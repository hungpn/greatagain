package com.hung.greatagain.ui.feature.feed.adapter.holder

import android.view.View
import com.hung.greatagain.databinding.ItemNewFeedActionBinding
import com.hung.greatagain.ui.feature.feed.adapter.item.NoDefinedItem

class NoDefinedViewHolder(
        binding: ItemNewFeedActionBinding
) : BaseFeedViewHolder<NoDefinedItem>(binding) {

    override fun onBinding(item: NoDefinedItem) {
        super.onBinding(item)
        mBinding.root.visibility = View.GONE
    }
}