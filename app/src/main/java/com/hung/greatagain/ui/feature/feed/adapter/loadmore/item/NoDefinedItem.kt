package com.hung.greatagain.ui.feature.feed.adapter.loadmore.item


import com.hung.domain.model.feed.Feed
import com.hung.greatagain.util.SpannableBuilder

class NoDefinedItem(feed: Feed = Feed()) : BaseFeedItem(feed) {

    override var viewType = TYPE_ITEM_FEEDS_NO_DEFINED

    override fun buildTitle(): SpannableBuilder {
        return SpannableBuilder.builder()
    }

    override fun buildDescription(): SpannableBuilder {
        return SpannableBuilder.builder()
    }

    override var timeIconRes = 0
}
