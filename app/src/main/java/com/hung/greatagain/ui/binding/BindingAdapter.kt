package com.hung.greatagain.ui.binding

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.RawRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.databinding.BindingAdapter
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.hung.greatagain.R
import de.hdodenhof.circleimageview.CircleImageView


@BindingAdapter("visibility")
fun View.setVisibility(value: Boolean) {
    visibility = if (value) {
        View.VISIBLE
    } else {
        View.GONE
    }
}

@BindingAdapter("textVisibility")
fun TextView.setVisibilityAndText(text: String?) {
    if (text.isNullOrEmpty()) {
        visibility = View.GONE
    } else {
        this.text = text
        visibility = View.VISIBLE
    }
}

@BindingAdapter("src")
fun setImageResource(imageView: ImageView, resource: Int) {
    imageView.setImageResource(resource)
}

@BindingAdapter("loadingButtonState")
fun loadingButtonState(view: Button, isLoading: Boolean) {
    if (isLoading) {
        view.alpha = 0.5f
        view.setText(R.string.signing_in_in_loading)
        view.isEnabled = false
    } else {
        view.alpha = 1.0f
        view.setText(R.string.sign_in)
        view.isEnabled = true
    }
}

@BindingAdapter("constraintShowAnimated")
fun constraintShowAnimated(view: ConstraintLayout, isShow: Boolean) {
    val constraintLayout = (view.parent as ConstraintLayout)
    val set = ConstraintSet()
    val transition = AutoTransition()
    transition.duration = 100L
    TransitionManager.beginDelayedTransition(constraintLayout, transition)
    set.clone(constraintLayout)
    if (isShow) {
        set.setVisibility(view.id, ConstraintSet.VISIBLE)
        set.applyTo(constraintLayout)
    } else {
        set.setVisibility(view.id, ConstraintSet.GONE)
        set.applyTo(constraintLayout)
    }
}

@BindingAdapter("imageUrlCircleCrop")
fun ImageView.bindCircleCropImage(url: String?) {
    Glide.with(context)
            .load(url)
            .apply(RequestOptions.circleCropTransform())
            .into(this)
}

@BindingAdapter("imageUrl")
fun ImageView.bindImage(url: String?) {
    Glide.with(context)
            .load(url)
            .into(this)
}

@BindingAdapter("imageUrl")
fun ImageView.bindImage(@RawRes @DrawableRes resourceId: Int?) {
    Glide.with(context)
            .load(resourceId)
            .into(this)
}