package com.hung.greatagain.ui.feature.pullrequest

import android.os.Bundle
import android.view.View
import com.hung.greatagain.R
import com.hung.greatagain.base.ViewModelFragment
import com.hung.greatagain.databinding.FragmentPullRequestBinding
import com.hung.greatagain.util.BundleKeyConst

class PullRequestFragment : ViewModelFragment<PullRequestViewModel, FragmentPullRequestBinding>() {

    companion object {

        fun newInstance(loginId: String)=  PullRequestFragment().apply {
            arguments = Bundle().apply {
                putString(BundleKeyConst.EXTRA_1, loginId)
            }
        }
    }

    override val mLayoutId = R.layout.fragment_pull_request

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel.toString()
    }
}