package com.hung.greatagain.ui.feature.main.adapter

import com.hung.greatagain.base.BaseFragment

data class FragmentModelWrapper(
        var fragment: BaseFragment,
        var title: String)