package com.hung.greatagain.ui.feature.main.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class SGFragmentPagerAdapter(
        fm: FragmentManager,
        var fragmentModelWrappers: List<FragmentModelWrapper>
) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return fragmentModelWrappers[position].fragment
    }

    override fun getCount(): Int {
        return fragmentModelWrappers.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return fragmentModelWrappers[position].title
    }
}