package com.hung.greatagain

import android.os.StrictMode
import com.facebook.stetho.Stetho
import com.hung.greatagain.di.component.DaggerApplicationComponent
import com.squareup.leakcanary.LeakCanary
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump

class App : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        setUpStetho()
        setupLeakCanary()
        setupCalligraphyConfig()
    }

    private fun setUpStetho() {
        Stetho.initializeWithDefaults(this)
    }

    private fun setupLeakCanary() {
        //enabledStrictMode()
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return
        }
        LeakCanary.install(this)
    }

    private fun setupCalligraphyConfig() {
        ViewPump.init(ViewPump.builder()
                .addInterceptor(CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/roboto_regular.ttf")
                                .setFontAttrId(R.attr.fontFamily)
                                .build()))
                .build())
    }

    private fun enabledStrictMode() {
        StrictMode.setThreadPolicy(
                StrictMode.ThreadPolicy.Builder() //
                        .detectAll() //
                        .penaltyLog() //
                        .penaltyDeath() //
                        .build()
        )
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerApplicationComponent.builder().myApp(this).build()
    }
}