package com.hung.greatagain.base.dialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.RelativeLayout
import androidx.databinding.ObservableField
import androidx.fragment.app.FragmentManager
import com.hung.greatagain.R
import com.hung.greatagain.databinding.DialogLoadingBinding
import com.hung.greatagain.util.BundleKeyConst
import com.hung.share.EMPTY
import pl.droidsonroids.gif.GifDrawable
import java.io.IOException

class LoadingDialog : BaseDialogFragment<DialogLoadingBinding>() {

    companion object {
        fun newInstance(message: String) =
                LoadingDialog().apply {
                    arguments = Bundle().apply {
                        putString(BundleKeyConst.EXTRA_1, message)
                    }
                }
    }

    private var mMessage = ObservableField<String>("")

    override fun getLayoutId(): Int {
        return R.layout.dialog_loading
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewBinding.apply {
            val gifFromResource: GifDrawable
            try {
                gifFromResource = GifDrawable(mViewBinding.root.resources, R.raw.loading1)
                ivLoading.setImageDrawable(gifFromResource)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            message = mMessage
        }

        mMessage.set(arguments?.getString(BundleKeyConst.EXTRA_1) ?: EMPTY)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // the content
        val root = RelativeLayout(activity)
        root.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        )

        // creating the fullscreen dialog
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        if (dialog.window != null) {
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.window!!.setLayout(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }
        dialog.setCanceledOnTouchOutside(false)

        return dialog
    }

    override fun dismiss() {
        if (isAdded) {
            super.dismiss()
        }
    }

    override fun show(fragmentManager: FragmentManager?, tag: String) {
        if (fragmentManager == null) {
            return
        }

        val transaction = fragmentManager.beginTransaction()
        val prevFragment = fragmentManager.findFragmentByTag(tag)
        if (prevFragment != null) {
            transaction.remove(prevFragment)
        }
        transaction.addToBackStack(null)
        show(transaction, tag)
    }
}