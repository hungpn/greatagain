package com.hung.greatagain.base.model

sealed class DataState<out T> {

    data class Loading<out T>(val isLoading: Boolean = true, val isRefresh : Boolean = false) : DataState<T>()

    data class Success<out T>(val data: T, val isComplete: Boolean = false) : DataState<T>()

    data class Failure<out T>(val throwable: Throwable) : DataState<T>()
}

sealed class PagingDataState<out T> {

    data class Loading<out T>(val isLoading: Boolean = true, val isRefresh : Boolean = false) : PagingDataState<T>()

    data class Success<out T>(val data: T, val isComplete: Boolean = false) : PagingDataState<T>()

    data class Failure<out T>(val throwable: Throwable) : PagingDataState<T>()
}