package com.hung.greatagain.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.hung.greatagain.di.ViewModelProviderFactory
import java.lang.reflect.ParameterizedType
import javax.inject.Inject

abstract class ViewModelActivity<VM : BaseViewModel, VB : ViewDataBinding> : BaseActivity() {

    @Inject
    lateinit var mViewModelProviderFactory: ViewModelProviderFactory

    protected lateinit var mViewModel: VM

    protected lateinit var mViewBinding: VB

    @get:LayoutRes
    protected abstract val mLayoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewBinding = DataBindingUtil.setContentView(this, mLayoutId)
        initViewModel()
    }

    private fun initViewModel() {
        val viewModelClass = (javaClass.genericSuperclass as ParameterizedType)
                .actualTypeArguments[0] as Class<VM>

        mViewModel = ViewModelProviders.of(this, mViewModelProviderFactory).get(viewModelClass)

        mViewModel.apply {
            // observer network state
            mNetworkStateLiveData.observe(this@ViewModelActivity, Observer {
                observerNetworkState(it)
            })

            // observer show simple dialog
            showSimpleDialogLiveData.observe(
                    this@ViewModelActivity, Observer { pair ->
                showSimpleDialog(
                        message = pair.first,
                        title = pair.second
                )
            })
        }
    }
}