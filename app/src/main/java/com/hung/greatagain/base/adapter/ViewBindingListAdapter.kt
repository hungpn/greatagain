package com.hung.greatagain.base.adapter

import androidx.annotation.CallSuper
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import java.util.concurrent.Executor
import java.util.concurrent.Executors

abstract class ViewBindingListAdapter<T, V : ViewDataBinding>(
        executor: Executor = Executors.newSingleThreadExecutor(),
        diffCallback: DiffUtil.ItemCallback<T>
) : ListAdapter<T, DataBoundViewHolder<T, V>>(
        AsyncDifferConfig.Builder<T>(diffCallback)
                .setBackgroundThreadExecutor(executor)
                .build()
) {

    @CallSuper
    override fun onBindViewHolder(holder: DataBoundViewHolder<T, V>, position: Int) {
        holder.binding.executePendingBindings()
        holder.onBind(getItem(position))
    }
}
