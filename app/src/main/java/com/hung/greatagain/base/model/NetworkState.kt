package com.hung.greatagain.base.model

sealed class NetworkState {

    object IsChecking : NetworkState()

    data class NoConnection(val msg: String) : NetworkState()

    object Connected : NetworkState()
}