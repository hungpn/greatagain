package com.hung.greatagain.base.adapter

import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import androidx.recyclerview.widget.*
import androidx.recyclerview.widget.DiffUtil.DiffResult.NO_POSITION
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference
import java.util.ArrayList
import java.util.concurrent.Executor
import java.util.concurrent.Executors

abstract class BaseAdapter<VH : BaseAdapter.BaseHolder<*, T>, T>(
    private var mOnItemClickListener: OnItemClickListener<T>? = null,
    private var mComparator: ItemComparator<T>? = null
) : RecyclerView.Adapter<VH>() {

    protected val itemList = ArrayList<T>()

    private var mCalculateDiffDisposable: Disposable? = null

    private var mAdapterDataObserver: RecyclerView.AdapterDataObserver? = null

    @Volatile
    var mViewWeakReference: WeakReference<RecyclerView>? = null

    protected abstract fun getViewHolder(parent: ViewGroup, viewType: Int): VH?

    private val listUpdateCallBack = object : ListUpdateCallback{

        var firstInsert = false

        override fun onChanged(position: Int, count: Int, payload: Any?) {
            notifyItemRangeChanged(position, count, payload)
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {
            notifyItemMoved(fromPosition, toPosition)
        }

        override fun onInserted(position: Int, count: Int) {
            if(position == 0)
                firstInsert = true
            notifyItemRangeInserted(position, count)
        }

        override fun onRemoved(position: Int, count: Int) {
            notifyItemRangeRemoved(position, count)
        }
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        mViewWeakReference = WeakReference(recyclerView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val viewHolder = getViewHolder(parent, viewType)
        if (viewHolder != null && mOnItemClickListener != null) {
            viewHolder.mViewDataBinding.root.setOnClickListener { view ->
                val pos = viewHolder.adapterPosition
                if (pos != NO_POSITION) {
                    mOnItemClickListener!!.onItemClick(view, pos, itemList[pos])
                }
            }
        }
        return viewHolder!!
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bindData(itemList[position])
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    fun registerDataObserver(adapterDataObserver: RecyclerView.AdapterDataObserver) {
        if (mAdapterDataObserver != null) {
            unregisterAdapterDataObserver(mAdapterDataObserver!!)
        }
        mAdapterDataObserver = adapterDataObserver
        registerAdapterDataObserver(mAdapterDataObserver!!)
    }

    private fun unRegisterDataObserver() {
        if (mAdapterDataObserver != null) {
            unregisterAdapterDataObserver(mAdapterDataObserver!!)
        }
        mAdapterDataObserver = null
    }

    open fun update(items: List<T>) {
        if (!itemList.isNullOrEmpty() && mComparator != null) {
            updateDiffItemsOnly(items)
        } else {
            updateAllItems(items)
        }
    }

    private fun updateAllItems(items: List<T>) {
        updateItemsInModel(items)
        notifyDataSetChanged()
    }

    private fun updateDiffItemsOnly(items: List<T>) {
        if (mCalculateDiffDisposable != null && !mCalculateDiffDisposable!!.isDisposed)
            mCalculateDiffDisposable!!.dispose()
        mCalculateDiffDisposable = Single.fromCallable { calculateDiff(items) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { updateItemsInModel(items) }
            .subscribe { result -> updateAdapterWithDiffResult(result) }
    }

    private fun calculateDiff(newItems: List<T>): DiffUtil.DiffResult {
        return DiffUtil.calculateDiff(DiffUtilCallback(itemList, newItems, mComparator!!))
    }

    private fun updateItemsInModel(items: List<T>) {
        itemList.clear()
        itemList.addAll(items)
    }

    private fun updateAdapterWithDiffResult(result: DiffUtil.DiffResult) {
        result.dispatchUpdatesTo(listUpdateCallBack)
        mViewWeakReference?.get()?.layoutManager?.let {
            when(it){
                is LinearLayoutManager -> {
                    if(it.findFirstCompletelyVisibleItemPosition() == 0)
                        mViewWeakReference?.get()?.scrollToPosition(0)
                }
                else -> {}
            }
        }
    }

    open fun release() {
        mOnItemClickListener = null
        unRegisterDataObserver()
    }

    open class BaseHolder<out V : ViewDataBinding, in T>(val mViewDataBinding: V) :
        RecyclerView.ViewHolder(mViewDataBinding.root) {

        open fun bindData(data: T) {}
    }

    interface OnItemClickListener<in T> {
        fun onItemClick(v: View, position: Int, data: T)
    }
}