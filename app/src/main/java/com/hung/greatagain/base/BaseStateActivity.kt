package com.hung.greatagain.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProviders
import com.hung.greatagain.di.ViewModelProviderFactory
import com.hung.greatagain.util.withState
import java.lang.reflect.ParameterizedType
import javax.inject.Inject

abstract class BaseStateActivity<State, Intent, VM : BaseStateViewModel<State, Intent>, VB : ViewDataBinding> : BaseActivity() {

    @Inject
    internal lateinit var mViewModelFactory: ViewModelProviderFactory

    protected lateinit var mViewModel: VM

    protected lateinit var mViewBinding: VB

    @get:LayoutRes
    protected abstract val mLayoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewBinding = DataBindingUtil.setContentView(this, mLayoutId)

        // getting view model class (3rd position is ViewModel type)
        val viewModelClass = (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[2] as Class<VM>
        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(viewModelClass)
        lifecycle.addObserver(mViewModel)
        //withState { onState(this) }
    }

    override fun onDestroy() {
        super.onDestroy()
        lifecycle.removeObserver(mViewModel)
    }

    protected fun withState(state: State.() -> Unit) {
        withState(mViewModel, state)
    }

    //protected abstract fun onState(state: State)

    companion object {
        const val NO_LAYOUT = 0
    }
}