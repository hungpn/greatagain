package com.hung.greatagain.base.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.databinding.ViewDataBinding
import com.hung.greatagain.base.model.LoadMoreListState
import com.hung.greatagain.base.model.LoadMoreListState.*
import com.hung.greatagain.databinding.ItemNetworkStateBinding

abstract class LoadMoreAdapter<T>(
        var mOnLoadMore: () -> Unit = {},
        onRetry: (() -> Unit)? = null,
        onItemClickListener: OnItemClickListener<T>? = null,
        comparator: ItemComparator<T>? = null
) : BaseAdapter<BaseAdapter.BaseHolder<ViewDataBinding, T>, T>(
        onItemClickListener, comparator
) {

    companion object {
        private const val BOTTOM_ITEM_TYPE = 100
    }

    private var loadMoreState = INITIAL

    private var isLoading = false

    private var _loadMoreCondition: LoadMoreAdapter<T>.() -> Boolean = { false }

    private var onRetryWrapper: () -> Unit = {}

//    private val mLoadMoreDataObserver = object : RecyclerView.AdapterDataObserver() {
//        override fun onChanged() {
//            Log.d("LoadMoreAdapter", "onChanged")
//            notifyDataSetChanged()
//        }
//
//        override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
//            Log.d("LoadMoreAdapter", "onItemRangeRemoved")
//        }
//
//        override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
//            Log.d("LoadMoreAdapter", "onItemRangeMoved")
//        }
//
//        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
//            Log.d("LoadMoreAdapter", "onItemRangeInserted")
//        }
//
//        override fun onItemRangeChanged(positionStart: Int, itemCount: Int) {
//            Log.d("LoadMoreAdapter", "onItemRangeChanged")
//        }
//
//        override fun onItemRangeChanged(positionStart: Int, itemCount: Int, payload: Any?) {
//            Log.d("LoadMoreAdapter", "onChanged")
//        }
//    }

    init {
//        registerDataObserver(mLoadMoreDataObserver)
        onRetry?.let {
            onRetryWrapper = {
                if (super.getItemCount() > 0)
                    setLoadMoreState(HASMORE)
                else reset()
                onRetry.invoke()
            }
        }
    }

    override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseHolder<*, T>? {
        return null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseHolder<*, T> {
        return when (viewType) {
            BOTTOM_ITEM_TYPE -> onCreateNetWorkStateViewHolder(parent)
            else -> super.onCreateViewHolder(parent, viewType)
        }
    }

    override fun onBindViewHolder(holder: BaseHolder<*, T>, position: Int) {
        if (holder is NetworkViewHolder) {
            holder.setLoadMoreState(loadMoreState)
        } else
            super.onBindViewHolder(holder, position)
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (isBottomAdded()) 1 else 0
    }

    override fun getItemViewType(position: Int): Int {
        return if (isBottomAdded() && position == bottomItemPosition()) BOTTOM_ITEM_TYPE
        else getItemTypeAtPosition(position)
    }

    override fun update(items: List<T>) {
        processListViewState()
        super.update(items)
    }

    abstract fun getItemTypeAtPosition(position: Int): Int

    private fun bottomItemPosition(): Int {
        return itemCount - 1
    }

    fun setLoadMoreCondition(inFunc: LoadMoreAdapter<T>.() -> Boolean) {
        _loadMoreCondition = inFunc
    }

//    fun setCompleteCondition(inFunc: LoadMoreAdapter<T>.() -> Boolean){
//        completeCondition = inFunc
//    }

    fun processListViewState() {
        if (isLoading)
            isLoading = false
        val loadMoreState =
                if (_loadMoreCondition.invoke(this)) HASMORE else COMPLETE
        setLoadMoreState(loadMoreState)
    }

    fun isReachedEnd(): Boolean {
        return loadMoreState == COMPLETE
    }

    fun setLoading(loading: Boolean) {
        isLoading = loading
    }

    fun blockingRequest(): Boolean {
        return isLoading || loadMoreState == COMPLETE || loadMoreState == ERROR
    }

    private fun isBottomAdded(): Boolean {
        return loadMoreState == HASMORE || loadMoreState == ERROR
    }

    fun isLoadMoreView(position: Int): Boolean {
        return getItemViewType(position) == BOTTOM_ITEM_TYPE
    }

    fun reset() {
        setLoadMoreState(INITIAL)
//        submitList(emptyList())
    }

    fun setLoadMoreState(state: LoadMoreListState) {
        if (loadMoreState == state) {
            return
        }
        when (state) {
            INITIAL -> {
                when (loadMoreState) {
                    HASMORE, ERROR -> notifyItemChanged(bottomItemPosition())
                    else -> {
                    }
                }
                loadMoreState = state
            }
            HASMORE -> {
                loadMoreState = state
                notifyItemChanged(bottomItemPosition())
            }
            ERROR -> {
                if (super.getItemCount() == 0) {
                    loadMoreState = INITIAL
                } else {
                    loadMoreState = state
                    notifyItemChanged(bottomItemPosition())
                }
            }
            COMPLETE -> {
                notifyItemRemoved(bottomItemPosition())
                loadMoreState = state
            }
        }
    }

    protected fun onCreateNetWorkStateViewHolder(parent: ViewGroup): BaseHolder<ViewDataBinding, T> {
        return DefaultNetworkViewHolder(
                ItemNetworkStateBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                ), onRetryWrapper
        )
    }

    class DefaultNetworkViewHolder<T>(
            viewDataBinding: ItemNetworkStateBinding,
            private var mOnRetry: () -> Unit = {}
    ) : NetworkViewHolder<ItemNetworkStateBinding, T>(viewDataBinding) {

        init {
            mViewDataBinding.btnRetry.setOnClickListener {
                setLoadMoreState(HASMORE)
                mOnRetry.invoke()
            }
        }

        override fun setLoadMoreState(state: LoadMoreListState) {
            super.setLoadMoreState(state)
            mViewDataBinding.state = state
        }
    }

    open class NetworkViewHolder<out V : ViewDataBinding, in T>(
            viewDataBinding: V
    ) : BaseAdapter.BaseHolder<V, T>(viewDataBinding) {

        protected var mState: LoadMoreListState = INITIAL

        @CallSuper
        open fun setLoadMoreState(state: LoadMoreListState) {
            if (mState != state)
                mState = state
        }
    }
}