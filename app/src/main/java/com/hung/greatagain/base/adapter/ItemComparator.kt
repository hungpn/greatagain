package com.hung.greatagain.base.adapter

interface ItemComparator<in T> {

    fun areItemsTheSame(oldItem: T, newItem: T): Boolean

    fun areContentsTheSame(oldItem: T, newItem: T): Boolean
}