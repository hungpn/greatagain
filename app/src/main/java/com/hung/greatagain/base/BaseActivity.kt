package com.hung.greatagain.base

import android.content.Context
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.hung.greatagain.base.model.NetworkState
import com.hung.share.EMPTY
import dagger.android.support.DaggerAppCompatActivity
import io.github.inflationx.viewpump.ViewPumpContextWrapper


abstract class BaseActivity : DaggerAppCompatActivity() {

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    fun showSimpleDialog(message: String, title: String? = "") {
        AlertDialog.Builder(this).create().apply {
            setTitle(title)
            setMessage(message)
            setButton(
                    AlertDialog.BUTTON_NEUTRAL, "OK"
            ) { dialog, _ ->
                dialog.dismiss()
            }
            show()
        }
    }

    fun showErrorDialog(message: String?) {
        Toast.makeText(this, message ?: EMPTY, Toast.LENGTH_SHORT).show()
    }

    fun observerNetworkState(networkState: NetworkState?) {
        when (networkState) {
            is NetworkState.NoConnection -> {
                showErrorDialog(networkState.msg)
            }

            is NetworkState.IsChecking -> {
                //TODO:
            }

            is NetworkState.Connected -> {
                //TODO:
            }
        }
    }
}