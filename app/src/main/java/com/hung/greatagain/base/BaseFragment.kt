package com.hung.greatagain.base

import android.widget.Toast
import com.hung.share.EMPTY
import dagger.android.support.DaggerFragment

abstract class BaseFragment : DaggerFragment() {

    fun showErrorDialog(message: String?) {
        Toast.makeText(requireContext(), message ?: EMPTY, Toast.LENGTH_SHORT).show()
    }
}