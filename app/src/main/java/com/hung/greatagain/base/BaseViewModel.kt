package com.hung.greatagain.base

import androidx.lifecycle.*
import com.hung.data.exception.NoConnectionException
import com.hung.greatagain.base.model.NetworkState
import com.hung.greatagain.base.model.NetworkState.IsChecking
import com.hung.greatagain.base.model.NetworkState.NoConnection
import com.hung.greatagain.util.default
import com.hung.share.EMPTY
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo

abstract class BaseViewModel : ViewModel(), LifecycleObserver {

    val mNetworkStateLiveData = MutableLiveData<NetworkState>()
            .default(IsChecking)

    protected val showSimpleDialogMutableLiveData = MutableLiveData<Pair<String, String?>>()
    val showSimpleDialogLiveData: LiveData<Pair<String, String?>>
        get() = showSimpleDialogMutableLiveData

    private val mDisposables: CompositeDisposable = CompositeDisposable()

    fun Disposable.addToDisposables() {
        this.addTo(mDisposables)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        mDisposables.dispose()
    }

    fun postMessageToShowSimpleDialog(message: String? = null, title: String? = null) {
        if (message?.isNotEmpty() == true) {
            showSimpleDialogMutableLiveData.postValue(Pair(message, title))
        } else {
            showSimpleDialogMutableLiveData.postValue(Pair("Something was wrong!", title))
        }
    }

    protected fun handleCommonError(throwable: Throwable, block: () -> Unit = {}) {
        when (throwable) {
            is NoConnectionException -> {
                mNetworkStateLiveData.postValue(NoConnection(throwable.message ?: EMPTY))
            }
            else -> {
                block()
            }
        }
    }
}