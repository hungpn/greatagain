package com.hung.greatagain.base.adapter

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.hung.greatagain.R

class LoadMoreRecyclerView : RecyclerView {

    companion object {
        private const val NUMBER_TO_LOAD_MORE_DEFAULT = 1
    }

    private var mVisibleThreshold = 1

    private var mLoadMoreAdapter: LoadMoreAdapter<*>? = null

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.LoadMoreRecyclerView, defStyle, 0)

        mVisibleThreshold = a.getInt(R.styleable.LoadMoreRecyclerView_number_to_load_more, NUMBER_TO_LOAD_MORE_DEFAULT)

        a.recycle()
    }

    private val mEndScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if (dy < 0 || mLoadMoreAdapter!!.blockingRequest())
                return

            val layoutManager = layoutManager!!

            initLayoutManager(layoutManager)

            val totalItemCount = layoutManager.itemCount

//            if (totalItemCount == 0) return

            val lastVisibleItemPosition: Int

            lastVisibleItemPosition = when (layoutManager) {
                is GridLayoutManager -> layoutManager.findLastVisibleItemPosition()
                is StaggeredGridLayoutManager -> {
                    val into = IntArray(layoutManager.spanCount)
                    layoutManager.findLastVisibleItemPositions(into)
                    findMax(into)
                }
                else -> (layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
            }

            if (!mLoadMoreAdapter!!.blockingRequest() && totalItemCount <= lastVisibleItemPosition + mVisibleThreshold) {
                mLoadMoreAdapter?.mOnLoadMore?.invoke()
            }
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        addOnScrollListener(mEndScrollListener)
    }

    override fun onDetachedFromWindow() {
//        mOnLoadMoreListener = null
        removeOnScrollListener(mEndScrollListener)
        super.onDetachedFromWindow()
    }

    override fun setLayoutManager(layout: LayoutManager?) {
        super.setLayoutManager(layout)
        if (layout is GridLayoutManager) {
            layout.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (mLoadMoreAdapter != null && mLoadMoreAdapter!!.isLoadMoreView(position)) {
                        layout.spanCount
                    } else 1
                }
            }
        }
    }

    fun setLoadMoreAdapter(adapter: LoadMoreAdapter<*>?) {
        mLoadMoreAdapter = adapter
        super.setAdapter(adapter)
    }

    private fun initLayoutManager(layoutManager: RecyclerView.LayoutManager) {
        if (layoutManager is GridLayoutManager) {
            mVisibleThreshold *= layoutManager.spanCount
        } else if (layoutManager is StaggeredGridLayoutManager) {
            mVisibleThreshold *= layoutManager.spanCount
        }
    }

    private fun findMax(lastPositions: IntArray): Int {
        return lastPositions.max() ?: 0
    }

}