package com.hung.greatagain.base.model

//@IntDef(INITIAL, HASMORE, COMPLETE, ERROR)
//@Retention(AnnotationRetention.RUNTIME)
//annotation class LoadMoreListState {
//    companion object {
//        const val INITIAL = 1
//        const val HASMORE = 2
//        const val COMPLETE = 3
//        const val ERROR = 4
//    }
//}

enum class LoadMoreListState {INITIAL, HASMORE, COMPLETE, ERROR}