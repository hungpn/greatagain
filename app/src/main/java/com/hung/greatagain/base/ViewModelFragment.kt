package com.hung.greatagain.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.hung.greatagain.di.ViewModelProviderFactory
import java.lang.reflect.ParameterizedType
import javax.inject.Inject

abstract class ViewModelFragment<VM : BaseViewModel, VB : ViewDataBinding> : BaseFragment() {

    @Inject
    lateinit var mViewModelProviderFactory: ViewModelProviderFactory

    protected lateinit var mViewModel: VM

    protected lateinit var mViewBinding: VB

    @get:LayoutRes
    protected abstract val mLayoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mViewBinding = DataBindingUtil.inflate(inflater, mLayoutId, container, false)
        return mViewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerViewModel()
    }

    private fun initViewModel() {
        val viewModelClass = (javaClass.genericSuperclass as ParameterizedType)
                .actualTypeArguments[0] as Class<VM>

        mViewModel = ViewModelProviders.of(this, mViewModelProviderFactory).get(viewModelClass)
    }

    /**
     * note: this@ViewModelFragment.viewLifecycleOwner always call before onCreateView and onDestroy
     */
    private fun observerViewModel() {
        mViewModel.apply {
            mNetworkStateLiveData.observe(this@ViewModelFragment.viewLifecycleOwner, Observer {
                (requireActivity() as? BaseActivity)?.observerNetworkState(networkState = it)
            })

            showSimpleDialogLiveData.observe(this@ViewModelFragment.viewLifecycleOwner, Observer {
                (requireActivity() as? BaseActivity)?.showSimpleDialog(
                        message = it.first,
                        title = it.second
                )
            })
        }
    }
}