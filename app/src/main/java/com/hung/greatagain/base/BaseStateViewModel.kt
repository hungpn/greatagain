package com.hung.greatagain.base

import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hung.greatagain.util.onNextIfNew
import com.hung.greatagain.util.setValueIfNew
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject

abstract class BaseStateViewModel<S, I> : ViewModel(), LifecycleObserver {

    private val mCompositeDisposable = CompositeDisposable()

    // intent
    private val mIntentSubject : BehaviorSubject<I>

    // view state
    val state: MutableLiveData<S> = MutableLiveData(initState)

    init {
        mIntentSubject = BehaviorSubject.createDefault<I>(initIntent)
        bindStateSubject()
    }

    /**
     * Provide initial state to be used in [setState] in which the new state will be produced from current state,
     * therefore current state should not be null
     */
    protected abstract fun initState(): S

    private val initState: S
        get() = initState()

    /**
     * Get current state, [initState] will be returned if current state is null
     */
    val currentState: S
        get() = state.value ?: initState()

    abstract fun getIntentClass(): Class<I>

    private val initIntent : I
        get() = getIntentClass().newInstance()

    /**
     * set new view state and post it through [state] live data
     *
     * @param stateFunc provide new state by applying current state
     */
    @MainThread
    protected fun setState(stateFunc: S.() -> S) {
        state.setValueIfNew(stateFunc.invoke(state.value ?: initState()))
    }

    private fun bindStateSubject() {
        addDisposable {
            transform(mIntentSubject) {
                return@transform mapIntentToState(this)
            }.forEach { nextState ->
                run {
                    if (currentState == nextState) return@run
                    setState { nextState }
                }
            }
        }
    }

    private fun transform(intents: Observable<I>, next: I.() -> Observable<S>): Observable<S> {
        return intents.switchMap {
            return@switchMap next.invoke(it)
        }
    }

    fun disPatch(intentFunc : I.() -> I) {
        mIntentSubject.onNextIfNew(intentFunc.invoke(mIntentSubject.value!!))
    }


    abstract fun mapIntentToState(intent: I): Observable<S>

    fun addDisposable(disposable: () -> Disposable) {
        mCompositeDisposable.add(disposable.invoke())
    }

    override fun onCleared() {
        super.onCleared()
        mCompositeDisposable.clear()
    }
}