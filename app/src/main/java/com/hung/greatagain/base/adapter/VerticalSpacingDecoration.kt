package com.hung.greatagain.base.adapter

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.hung.greatagain.ui.feature.feed.adapter.item.BaseFeedItem.Companion.TYPE_ITEM_FEEDS_NO_DEFINED

class VerticalSpacingDecoration(
        private val verticalSpaceHeight: Int,
        private val hasSpaceLast: Boolean = true
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
            outRect: Rect, view: View, parent: RecyclerView,
            state: RecyclerView.State
    ) {
        val layoutManager = parent.layoutManager
        val viewType = layoutManager?.getItemViewType(view)

        if (viewType == TYPE_ITEM_FEEDS_NO_DEFINED) {
            return
        }

        if (hasSpaceLast) {
            outRect.bottom = verticalSpaceHeight
            if (parent.getChildAdapterPosition(view) == parent.adapter!!.itemCount - 1) {
                outRect.bottom = verticalSpaceHeight + 50
            }
        } else {
            if (parent.getChildAdapterPosition(view) != parent.adapter!!.itemCount - 1) {
                outRect.bottom = verticalSpaceHeight
            }
        }
    }
}
