package com.hung.greatagain.base.adapter

import android.content.Context
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class DataBoundViewHolder<T, out V : ViewDataBinding> constructor(
        val binding: V
) : RecyclerView.ViewHolder(binding.root) {

    val context: Context
        get() = binding.root.context

    abstract fun onBind(item: T)
}
