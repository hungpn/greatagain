package com.hung.greatagain.di.module

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.hung.data.util.API_DATE_TIME_FORMAT
import com.hung.data.di.qualifier.ApplicationContext
import com.hung.data.model.feed.FeedTypeDeserializer
import com.hung.data.model.feed.FeedTypeResponse
import com.hung.domain.executor.UseCaseExecutor
import com.hung.domain.executor.UseCaseExecutorImpl
import com.hung.greatagain.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule {

    @Singleton
    @Provides
    @ApplicationContext
    fun provideContext(app: App): Context = app.applicationContext

    /*@Provides
    @Singleton
    fun getSharePreferences(@ApplicationContext context: Context): SharePreferenceManager {
        return SharePreferenceManager(context)
    }*/

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder()
                //.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .setDateFormat(API_DATE_TIME_FORMAT)
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .registerTypeAdapter(FeedTypeResponse::class.java, FeedTypeDeserializer()) // feeds parser
                .create()
    }

  /*  @Singleton
    @Provides
    fun provideSecurePreferences(@ApplicationContext context: Context): SharedPreferences {
        return context.getSharedPreferences("hihihehe.xml", Context.MODE_PRIVATE)
    }*/

    @Singleton
    @Provides
    fun provideUseCaseExecutor(useCaseExecutorImpl: UseCaseExecutorImpl): UseCaseExecutor = useCaseExecutorImpl
}