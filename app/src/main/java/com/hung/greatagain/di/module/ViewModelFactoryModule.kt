package com.hung.greatagain.di.module

import androidx.lifecycle.ViewModelProvider
import com.hung.greatagain.di.ViewModelProviderFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {

    @Binds
    internal abstract fun bindViewModelFactory(viewModelProviderFactory: ViewModelProviderFactory)
            : ViewModelProvider.Factory
}