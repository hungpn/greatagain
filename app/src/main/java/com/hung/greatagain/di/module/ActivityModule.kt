package com.hung.greatagain.di.module

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.hung.greatagain.di.qualifier.ActivityContext
import dagger.Module
import dagger.Provides

@Module
abstract class ActivityModule<T : AppCompatActivity> {

    @Provides
    @ActivityContext
    fun provideContext(activity: T) = activity as Context

    @Provides
    fun provideActivity(activity: T) = activity as AppCompatActivity
}