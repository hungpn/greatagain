package com.hung.greatagain.di.module

import com.hung.greatagain.di.scope.PerActivity
import com.hung.greatagain.ui.feature.feed.FeedModule
import com.hung.greatagain.ui.feature.issue.IssueModule
import com.hung.greatagain.ui.feature.login.*
import com.hung.greatagain.ui.feature.main.MainActivity
import com.hung.greatagain.ui.feature.main.MainActivityModule
import com.hung.greatagain.ui.feature.main.MainModule
import com.hung.greatagain.ui.feature.main.MainViewModelModule
import com.hung.greatagain.ui.feature.main.menu.MenuMainTabModule
import com.hung.greatagain.ui.feature.main.menu.MenuAccountTabModule
import com.hung.greatagain.ui.feature.profile.ProfileActivity
import com.hung.greatagain.ui.feature.profile.ProfileActivityModule
import com.hung.greatagain.ui.feature.profile.ProfileModule
import com.hung.greatagain.ui.feature.profile.ProfileViewModelModule
import com.hung.greatagain.ui.feature.profile.tab.follow.ProfileFollowModule
import com.hung.greatagain.ui.feature.profile.tab.overview.ProfileOverviewModule
import com.hung.greatagain.ui.feature.pullrequest.PullRequestModule
import com.hung.greatagain.ui.feature.splash.SplashActivity
import com.hung.greatagain.ui.feature.splash.SplashActivityModule
import com.hung.greatagain.ui.feature.splash.SplashModule
import com.hung.greatagain.ui.feature.splash.SplashViewModelModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {

    @PerActivity
    @ContributesAndroidInjector(
            modules = [
                LoginModule::class,
                LoginActivityModule::class,
                LoginViewModelModule::class
            ]
    )
    abstract fun contributeLoginActivity(): LoginActivity

    @PerActivity
    @ContributesAndroidInjector(
            modules = [
                LoginStateModule::class,
                LoginStateActivityModule::class,
                LoginStateViewModelModule::class
            ]
    )
    abstract fun contributeLoginStateActivity(): LoginStateActivity

    @PerActivity
    @ContributesAndroidInjector(
            modules = [
                MainModule::class,
                MainActivityModule::class,
                MainViewModelModule::class,
                FeedModule::class,
                IssueModule::class,
                MenuMainTabModule::class,
                MenuAccountTabModule::class,
                PullRequestModule::class
            ]
    )
    abstract fun contributeMainActivity(): MainActivity

    @PerActivity
    @ContributesAndroidInjector(
            modules = [
                ProfileModule::class,
                ProfileViewModelModule::class,
                ProfileActivityModule::class,
                ProfileOverviewModule::class,
                ProfileFollowModule::class
            ]
    )
    abstract fun contributeProfileActivity(): ProfileActivity

    @PerActivity
    @ContributesAndroidInjector(
            modules = [
                SplashModule::class,
                SplashViewModelModule::class,
                SplashActivityModule::class
            ]
    )
    abstract fun contributeSplashActivity(): SplashActivity
}