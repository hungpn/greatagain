package com.hung.greatagain.di.component

import com.hung.data.di.module.DatabaseModule
import com.hung.data.di.module.NetworkModule
import com.hung.data.di.module.RepositoryModule
import com.hung.greatagain.App
import com.hung.greatagain.di.module.ActivityBuildersModule
import com.hung.greatagain.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
        modules = [
            AndroidSupportInjectionModule::class,
            ApplicationModule::class,
            ActivityBuildersModule::class,
            RepositoryModule::class,
            NetworkModule::class,
            DatabaseModule::class
        ]
)
interface ApplicationComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {

        /*@BindsInstance
        fun application(application: Application): Builder*/

        @BindsInstance
        fun myApp(app: App) : Builder

        fun build(): ApplicationComponent
    }
}