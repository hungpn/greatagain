package com.hung.greatagain.util

import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions


fun ImageView.loadImg(res: String?, @DrawableRes errorImageResId: Int = 0) {
    val requestOptions = RequestOptions()
            .error(errorImageResId)

    loadImg(res, requestOptions)
}

fun ImageView.loadCircleCropImg(res: String?, @DrawableRes errorImageResId: Int = 0) {
    val requestOptions = RequestOptions()
            .error(errorImageResId)
            .circleCrop()

    loadImg(res, requestOptions)
}

fun ImageView.loadImg(res: String?, requestOptions: RequestOptions) {
    Glide.with(context)
            .setDefaultRequestOptions(requestOptions)
            .load(res)
            .into(this)
}