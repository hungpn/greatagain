package com.hung.greatagain.util

object BundleKeyConst {

    const val ID = "id"
    const val EXTRA_1 = "extra_1"
    const val EXTRA_2 = "extra_2"
    const val EXTRA_3 = "extra_3"
    const val EXTRA_4 = "extra_4"

    const val EXTRA_REPO_ID = "EXTRA_REPO_ID"
    const val EXTRA_LOGIN = "EXTRA_LOGIN"
    const val EXTRA_HTML_LINK = "EXTRA_HTML_LINK"
    const val EXTRA_URL = "EXTRA_URL"
    const val EXTRA_DEFAULT_BRANCH = "EXTRA_DEFAULT_BRANCH"
}