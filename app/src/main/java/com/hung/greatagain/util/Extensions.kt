package com.hung.greatagain.util

import androidx.lifecycle.LifecycleOwner
import com.hung.greatagain.base.BaseStateViewModel
import com.hung.greatagain.base.model.Event
import io.reactivex.subjects.BehaviorSubject

/**
 * Observes state of a [BaseStateViewModel] in a [LifecycleOwner] only if the emitted state is not null
 */
fun <S, I, VM : BaseStateViewModel<S, I>> LifecycleOwner.withState(vm: VM, state: S.() -> Unit) {
    vm.state.observeNonNull(this, state)
}

/**
 * Get an [Event] value only if the event is NOT NULL and its content hasn't been handled yet
 */
fun <T> event(event: Event<T>?, onEventUnhandledContent: T.() -> Unit) {
    event?.get(onEventUnhandledContent)
}

fun <T> BehaviorSubject<T>.onNextIfNew(newValue: T) {
    if (this.value != newValue) {
        onNext(newValue)
    }
}

fun <T> Class<T>.createInstance(): T {
    return this.newInstance()
}