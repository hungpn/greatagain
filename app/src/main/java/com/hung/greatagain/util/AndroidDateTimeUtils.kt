package com.hung.greatagain.util

import android.text.format.DateUtils
import java.util.*

object AndroidDateTimeUtils {

    fun getTimeAgo(date: Date?): CharSequence? {
        if (date != null) {
            val now = System.currentTimeMillis()
            return DateUtils.getRelativeTimeSpanString(date.time, now, DateUtils.SECOND_IN_MILLIS)
        }
        return null
    }
}
