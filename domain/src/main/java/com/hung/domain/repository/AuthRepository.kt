package com.hung.domain.repository

import com.hung.domain.model.login.Auth
import io.reactivex.Single

interface AuthRepository {

    fun doBasicLogin(userName : String, password: String) : Single<Auth>
}