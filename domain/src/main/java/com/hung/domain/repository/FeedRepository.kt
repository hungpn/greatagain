package com.hung.domain.repository

import com.hung.domain.model.feed.Feed
import io.reactivex.Single

interface FeedRepository {

    fun getFeedsByUserName(userName: String, page: Int): Single<List<Feed>>
}