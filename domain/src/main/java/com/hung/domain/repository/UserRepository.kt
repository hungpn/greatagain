package com.hung.domain.repository

import com.hung.domain.model.user.User
import io.reactivex.Completable
import io.reactivex.Single
import java.io.InputStream

interface UserRepository {

    fun getUserByLoginId(loginId: String, getFromLocalFirst: Boolean = false): Single<User>

    fun getUserById(id: Long): Single<User>

    fun saveUser(user: User): Completable

    fun saveToken(token: String?)

    fun getToken(): String

    fun saveUserLoginId(loginId: String?)

    fun getUserLoginId(): String

    /*---profile---*/

    fun getFollowers(loginId: String, page: Int): Single<List<User>>

    fun getFollowing(loginId: String, page: Int): Single<List<User>>

    fun getFollowingStatus(loginId: String) : Single<Boolean>

    fun getContributes(loginId: String) : Single<InputStream>

    fun followUser(username: String): Single<Boolean>

    fun unFollowUser(username: String): Single<Boolean>
}