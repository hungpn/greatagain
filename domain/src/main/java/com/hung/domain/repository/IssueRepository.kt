package com.hung.domain.repository

import com.hung.domain.model.issue.Issues
import io.reactivex.Single

interface IssueRepository {

    fun getCreatedIssues(q: String, page: Int): Single<Issues>
}