package com.hung.domain.usecase

import com.hung.domain.model.user.User
import com.hung.domain.repository.UserRepository
import io.reactivex.Single
import javax.inject.Inject

class GetUserByIdUseCase @Inject constructor(
    private val userRepository: UserRepository
) : UseCase<GetUserByIdUseCase.Param, Single<User>>() {

    override fun execute(param: Param?): Single<User> {
        if (param == null) {
            return Single.error(Throwable("param is null"))
        }

        return userRepository.getUserById(param.id)
    }

    override fun onCleared() {

    }

    data class Param(val id: Long)
}