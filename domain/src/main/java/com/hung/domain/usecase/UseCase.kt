package com.hung.domain.usecase

abstract class UseCase<in Param, out T> where T : Any {

    abstract fun execute(param: Param? = null): T

    abstract fun onCleared()
}