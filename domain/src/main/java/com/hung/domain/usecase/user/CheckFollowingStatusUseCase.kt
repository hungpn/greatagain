package com.hung.domain.usecase.user

import com.hung.domain.repository.UserRepository
import com.hung.domain.usecase.UseCase
import io.reactivex.Single
import java.lang.Exception
import javax.inject.Inject

class CheckFollowingStatusUseCase @Inject constructor(
        private val mUserRepository: UserRepository
) : UseCase<CheckFollowingStatusUseCase.Param, Single<Boolean>>() {

    override fun execute(param: Param?): Single<Boolean> {
        if (param == null) {
            return Single.error(Exception("param is null"))
        }
        return mUserRepository.getFollowingStatus(param.userName)
    }

    override fun onCleared() {

    }

    class Param(
            val userName: String
    )
}