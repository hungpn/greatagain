package com.hung.domain.usecase

import com.hung.domain.model.feed.Feed
import com.hung.domain.repository.FeedRepository
import io.reactivex.Single
import javax.inject.Inject

class GetFeedsByUserNameUseCase @Inject constructor(
        private val mFeedRepository: FeedRepository
) : UseCase<GetFeedsByUserNameUseCase.Param, Single<List<Feed>>>() {

    override fun execute(param: Param?): Single<List<Feed>> {
        if (param == null) {
            return Single.error(Throwable("param is null"))
        }

        return mFeedRepository.getFeedsByUserName(param.userName, param.page)
    }

    override fun onCleared() {

    }

    data class Param(
            val userName: String,
            val page: Int
    )
}