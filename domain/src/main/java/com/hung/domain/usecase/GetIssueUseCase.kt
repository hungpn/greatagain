package com.hung.domain.usecase

import com.hung.domain.model.issue.Issues
import com.hung.domain.repository.IssueRepository
import io.reactivex.Single
import javax.inject.Inject

class GetIssueUseCase @Inject constructor(
        private val mIssueRepository: IssueRepository
) : UseCase<GetIssueUseCase.Param, Single<Issues>>() {

    override fun execute(param: Param?): Single<Issues> {
        if (param == null) {
            return Single.error(Throwable("param is null"))
        }

        return mIssueRepository.getCreatedIssues(
                param.q,
                param.page
        )
    }

    override fun onCleared() {

    }

    data class Param(
            val q: String,
            val page: Int
    )
}