package com.hung.domain.usecase.user

import com.hung.domain.model.user.FollowStatusType
import com.hung.domain.model.user.FollowStatusType.FOLLOW
import com.hung.domain.repository.UserRepository
import com.hung.domain.usecase.UseCase
import io.reactivex.Single
import javax.inject.Inject

class DoFollowUserUseCase @Inject constructor(
        private val mUserRepository: UserRepository
) : UseCase<DoFollowUserUseCase.Param, Single<Boolean>>() {

    override fun execute(param: Param?): Single<Boolean> {
        if (param == null) {
            return Single.error(Exception("param is null"))
        }

        return if (param.type == FOLLOW) {
            mUserRepository.followUser(param.username)
        } else {
            mUserRepository.unFollowUser(param.username)
        }
    }

    override fun onCleared() {

    }

    data class Param(
            val username: String,
            val type: FollowStatusType = FOLLOW
    )
}