package com.hung.domain.usecase

import com.hung.domain.model.user.User
import com.hung.domain.repository.UserRepository
import io.reactivex.Single
import javax.inject.Inject

class GetCurrentUserUseCase @Inject constructor(
        private val mUserRepository: UserRepository
) : UseCase<GetCurrentUserUseCase.Param, Single<User>>() {

    override fun execute(param: Param?): Single<User> {
        if (param == null) {
            return Single.error(Throwable("param is null"))
        }

        return mUserRepository.getUserByLoginId(
                loginId = mUserRepository.getUserLoginId(),
                getFromLocalFirst = param.getFromLocalFirst
        )
    }

    override fun onCleared() {

    }

    data class Param(
            val getFromLocalFirst: Boolean = false
    )
}