package com.hung.domain.usecase

import com.hung.domain.model.login.Auth
import com.hung.domain.repository.AuthRepository
import com.hung.domain.repository.UserRepository
import io.reactivex.Single
import javax.inject.Inject

class DoBasicLoginUseCase @Inject constructor(
        private val mUserRepository: UserRepository,
        private val mAuthRepository: AuthRepository
) : UseCase<DoBasicLoginUseCase.Param, Single<Auth>>() {

    override fun execute(param: Param?): Single<Auth> {
        if (param == null) {
            return Single.error(Throwable("param is null"))
        }

        return mAuthRepository.doBasicLogin(param.userName, param.password)
                .doOnSuccess {
                    mUserRepository.saveToken(it.token)
                    mUserRepository.saveUserLoginId(param.userName)
                }
    }

    override fun onCleared() {

    }

    data class Param(
            val userName: String,
            val password: String
    )
}