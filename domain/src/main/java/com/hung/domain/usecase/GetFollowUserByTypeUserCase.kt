package com.hung.domain.usecase

import com.hung.domain.model.user.FollowType
import com.hung.domain.model.user.User
import com.hung.domain.repository.UserRepository
import io.reactivex.Single
import javax.inject.Inject

class GetFollowUserByTypeUserCase @Inject constructor(
        private val mUserRepository: UserRepository
) : UseCase<GetFollowUserByTypeUserCase.Param, Single<List<User>>>() {

    override fun execute(param: Param?): Single<List<User>> {
        if (param == null) {
            return Single.error(Throwable("param is null"))
        }

        return if (param.type == FollowType.FOLLOWER) {
            mUserRepository.getFollowers(param.loginId, param.page)
        } else {
            mUserRepository.getFollowing(param.loginId, param.page)
        }
    }

    override fun onCleared() {

    }

    data class Param(
            val loginId: String,
            val type: FollowType,
            val page: Int
    )
}