package com.hung.domain.usecase.user

import com.hung.domain.repository.UserRepository
import com.hung.domain.usecase.UseCase
import io.reactivex.Single
import java.io.InputStream
import javax.inject.Inject

class GetContributeInputStream @Inject constructor(
        private val mUserRepository: UserRepository
) : UseCase<GetContributeInputStream.Param, Single<InputStream>>() {

    override fun execute(param: Param?): Single<InputStream> {
        if (param == null) {
            return Single.error(Exception("param is null"))
        }

        return mUserRepository.getContributes(param.loginId)
    }

    override fun onCleared() {

    }

    data class Param(
            val loginId: String
    )
}