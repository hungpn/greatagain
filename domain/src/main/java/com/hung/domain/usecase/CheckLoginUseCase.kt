package com.hung.domain.usecase

import com.hung.domain.exception.UserDoesNotLoginException
import com.hung.domain.repository.UserRepository
import io.reactivex.Completable
import javax.inject.Inject

class CheckLoginUseCase @Inject constructor(
        private val mUserRepository: UserRepository
) : UseCase<CheckLoginUseCase.Param, Completable>() {

    override fun execute(param: Param?): Completable {

        val loginId = mUserRepository.getToken()
        return if (loginId.isEmpty()) {
            Completable.error(UserDoesNotLoginException())
        } else {
            Completable.complete()
        }
    }

    override fun onCleared() {

    }

    class Param
}