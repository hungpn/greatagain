package com.hung.domain.usecase

import com.hung.domain.model.user.User
import com.hung.domain.repository.UserRepository
import io.reactivex.Single
import javax.inject.Inject

class GetUserByLoginIdUseCase @Inject constructor(
        private val userRepository: UserRepository
) : UseCase<GetUserByLoginIdUseCase.Param, Single<User>>() {

    override fun execute(param: Param?): Single<User> {
        if (param == null) {
            return Single.error(Throwable("param is null"))
        }

        return userRepository.getUserByLoginId(
                loginId = param.loginId,
                getFromLocalFirst = param.getFromLocalFirst
        )
    }

    override fun onCleared() {

    }

    data class Param(
            val loginId: String,
            val getFromLocalFirst: Boolean = false
    )
}