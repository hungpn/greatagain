package com.hung.domain.usecase.user

import com.hung.domain.repository.UserRepository
import com.hung.domain.usecase.UseCase
import javax.inject.Inject

class GetCurrentLoginIdUseCase @Inject constructor(
        private val mUserRepository: UserRepository
) : UseCase<GetCurrentLoginIdUseCase.Param, String>() {
    override fun execute(param: Param?): String {
        return mUserRepository.getUserLoginId()
    }

    override fun onCleared() {

    }

    data class Param(
            val ignore: Any = Any()
    )
}