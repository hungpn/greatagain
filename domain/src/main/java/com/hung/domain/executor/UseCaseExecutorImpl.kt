package com.hung.domain.executor

import java.util.concurrent.*
import java.util.concurrent.atomic.AtomicInteger
import javax.inject.Inject
import kotlin.math.max
import kotlin.math.min

class UseCaseExecutorImpl @Inject constructor() : UseCaseExecutor {

    companion object {
        private const val THREAD_NAME = "USECASE_THREAD"
        private val CPU_COUNT = Runtime.getRuntime().availableProcessors()
        // We want at least 2 threads and at most 4 threads in the core pool,
        // preferring to have 1 less than the CPU count to avoid saturating
        // the CPU with background work
        private val CORE_POOL_SIZE = max(2, min(CPU_COUNT - 1, 4))
        private val MAXIMUM_POOL_SIZE = CPU_COUNT * 2 + 1
        private const val KEEP_ALIVE_SECONDS = 30L
    }

    private val mQueue: BlockingQueue<Runnable>
    private val mThreadFactory: UseCaseThreadFactory
    private val mExecutor: Executor

    init {
        mQueue = LinkedBlockingQueue()
        mThreadFactory = UseCaseThreadFactory()
        mExecutor = ThreadPoolExecutor(
                CORE_POOL_SIZE, MAXIMUM_POOL_SIZE,
                KEEP_ALIVE_SECONDS, TimeUnit.SECONDS, mQueue, mThreadFactory
        )
    }


    override fun execute(command: Runnable?) {
        if (command == null) {
            throw IllegalArgumentException("Runnable to execute cannot be null")
        }
        mExecutor.execute(command)
    }

    private class UseCaseThreadFactory : ThreadFactory {

        val mCount = AtomicInteger(1)

        override fun newThread(r: Runnable): Thread {
            return Thread(r, THREAD_NAME + "#" + mCount.getAndIncrement())
        }
    }
}