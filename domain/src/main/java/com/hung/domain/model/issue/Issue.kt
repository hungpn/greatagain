package com.hung.domain.model.issue

import java.util.*

data class Issue(
        val title: String? = null,
        val comments: Int = 0,
        val htmlUrl: String? = null,
        val createdAt: Date? = null,
        val updateAt: Date? = null,
        val body: String? = null,
        val labels: List<Label>? = null
)