package com.hung.domain.model.issue

data class Label(
        var id: Int = 0,
        var url: String? = null,
        var name: String? = null,
        var color: String? = null
)