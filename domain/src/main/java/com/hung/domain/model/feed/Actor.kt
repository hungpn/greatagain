package com.hung.domain.model.feed

data class Actor(
        val id: Long = 0,
        val avatarUrl: String? = null,
        val displayLogin: String? = null
)