package com.hung.domain.model.issue

data class Issues(
        val totalCount: Int = 0,
        val incompleteResults: Boolean = false,
        val items: List<Issue> = listOf()
)