package com.hung.domain.model.user

enum class FollowStatusType {
    FOLLOW,
    UN_FOLLOW
}