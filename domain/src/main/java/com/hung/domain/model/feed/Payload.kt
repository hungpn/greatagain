package com.hung.domain.model.feed

data class Payload(
        val pushId: Long = 0,
        val ref: String? = null,
        val commits: List<Commit>? = null
)