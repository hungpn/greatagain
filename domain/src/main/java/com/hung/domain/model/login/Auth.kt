package com.hung.domain.model.login

data class Auth(
    val id: Long,
    val url: String,
    val token: String
)