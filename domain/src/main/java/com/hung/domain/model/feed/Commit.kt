package com.hung.domain.model.feed

data class Commit(
        val sha: String? = null,
        val message: String? = null
)