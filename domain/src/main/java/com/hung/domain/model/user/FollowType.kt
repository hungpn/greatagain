package com.hung.domain.model.user

enum class FollowType {
    FOLLOWING,
    FOLLOWER
}