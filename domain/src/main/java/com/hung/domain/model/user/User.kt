package com.hung.domain.model.user

import java.io.Serializable
import java.util.*

data class User(
        val id: Long = 0,
        val name: String = "",
        val loginId: String = "",
        val avatarUrl: String = "",
        val htmlUrl: String = "",
        val followers: Long = 0,
        val following: Long = 0,
        val company: String = "",
        val location: String = "",
        val email: String = "",
        val blog: String = "",
        val bio: String = "",
        val joinedTime: Date? = null
) : Serializable