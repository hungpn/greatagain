package com.hung.domain.model.feed

data class Repo(
        val id: Long = 0,
        val name: String? = null
)