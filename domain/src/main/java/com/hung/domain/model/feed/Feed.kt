package com.hung.domain.model.feed

import java.util.*

data class Feed(
        val id: String? = null,
        val type: FeedType? = FeedType.NO_DEFINE,
        val isPublic: Boolean? = false,
        val repo: Repo? = null,
        val actor: Actor? = null,
        val payload: Payload? = null,
        val createAt: Date? = null
)