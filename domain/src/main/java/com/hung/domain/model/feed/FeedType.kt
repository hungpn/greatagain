package com.hung.domain.model.feed

enum class FeedType constructor(var key: String) {

    PUSH_EVENT("PushEvent"),
    FORK_EVENT("ForkEvent"),
    PUBLIC_EVENT("PublicEvent"),
    CREATE_EVENT("CreateEvent"),
    WATCH_EVENT("WatchEvent"),
    NO_DEFINE("NoDefine")
}