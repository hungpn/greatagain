package com.hung.domain.usecase

import com.hung.domain.model.login.Auth
import com.hung.domain.repository.AuthRepository
import com.hung.domain.repository.UserRepository
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

class DoBasicLoginUseCaseTest : BaseUseCaseTest<DoBasicLoginUseCase>() {

    @Mock
    private lateinit var mUserRepository: UserRepository

    @Mock
    private lateinit var mAuthRepository: AuthRepository

    override fun createUseCase(): DoBasicLoginUseCase {
        return DoBasicLoginUseCase(mUserRepository, mAuthRepository)
    }

    @Test
    fun `checkUser param null`() {
        val testObserver = mUseCase.execute(null).test()

        // test case
        testObserver
            .assertError {
                it.message == "param is null"
            }
    }

    @Test
    fun `checkUser param not null, login success`() {
        val auth = Auth(0, "url", "token")
        whenever(mAuthRepository.doBasicLogin(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(Single.just(Auth(0, "url", "token")))

        val testObserver =
            mUseCase.execute(DoBasicLoginUseCase.Param(Mockito.anyString(), Mockito.anyString()))
                .test()

        // test case
        testObserver.assertComplete()
        testObserver.assertValue {
            it == auth
        }
        verify(mUserRepository, times(1)).saveToken(auth.token)
    }

    @Test
    fun `checkUser param not null, login fail`() {
        whenever(mAuthRepository.doBasicLogin(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(Single.error(Throwable("loginId fail")))

        val testObserver =
            mUseCase.execute(DoBasicLoginUseCase.Param(Mockito.anyString(), Mockito.anyString()))
                .test()

        // test case
        testObserver
            .assertError {
                it.message == "loginId fail"
            }
    }
}