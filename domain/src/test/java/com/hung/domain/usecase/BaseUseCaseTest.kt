package com.hung.domain.usecase

import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
abstract class BaseUseCaseTest<U : Any> {

    protected lateinit var mUseCase : U

    @Before
    fun setup() {
        mUseCase = createUseCase()
    }

    abstract fun createUseCase() : U
}